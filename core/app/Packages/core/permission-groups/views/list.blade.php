
@extends('layouts.back_master') @section('title','Add Menu')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">

	
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Permission Group 
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
	<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Group List</li>
	</ol>	
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Add Group</h3>
			<div class="box-tools pull-right">
				<a href="{{url('permission/groups/add')}}" class="btn btn-success btn-sm" style="    margin-top: 2px;">Add</a>
			</div>
		</div>
		<div class="box-body">
			<table class="table table-bordered" style="width:100%">
				<thead style="background:#ddd">
				<tr>
					<th width="5%">#</th>
					<th width="25%">Group Name</th>
					<th width="70%">Permissons</th>
				</tr>
				</thead>
				<tbody>
				<tr>
                    <?php $i=1 ?>
					@foreach($data as $item)
						<tr>
							<td>{{$i}}</td>
							<td>
								@if($item->name!=null)
									{{$item->name}}
								@endif								
							</td>
							<td>
								@if($item->permissions!=null)
									@foreach(json_decode($item->permissions) as $key=>$item)
										<span class="badge perm">{{$key}}</span>
									@endforeach
								@else
									-
								@endif										
							</td>
							<?php $i++ ?>
						</tr>
					@endforeach
				</tr>
				</tbody>
			</table>
			<?php echo $data->appends(Input::except('page'))->render()?>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</section><!-- /.content -->

@stop

@section('js')
<script src="{{asset('assets/sammy_new/vendor/jquery-validation/dist/jquery.validate.min.js')}}"></script>
<script type="text/javascript">
	var id = 0;
	var table = '';
	$(document).ready(function(){
		$('.item-delete').click(function(e){
			e.preventDefault();
			id = $(this).data('id');
			sweetAlertConfirm('Delete Asset', 'Are you sure?',2, deleteFunc);
		});
	});

	/**
	 * Delete the menu
	 * Call to the ajax request menu/delete.
	 */
	function deleteFunc(){
		ajaxRequest( '{{url('asset/delete')}}' , { 'id' : id  }, 'post', handleData);
	}

	/**
	 * Delete the menu return function
	 * Return to this function after sending ajax request to the menu/delete
	 */
	function handleData(data){
		if(data.status=='success'){
			sweetAlert('Delete Success','Record Deleted Successfully!',0);
			table.ajax.reload();
		}else if(data.status=='invalid_id'){
			sweetAlert('Delete Error','Record Id doesn\'t exists.',3);
		}else{
			sweetAlert('Error Occured','Please try again!',3);
		}
	}

</script>
@stop

