@extends('layouts.back_master') @section('title','User List')
@section('css')
<style type="text/css">
	.switch.switch-sm{
		width: 30px;
    	height: 16px;
	}

	.switch.switch-sm span i::before{
		width: 16px;
    	height: 16px;
	}

	.switch :checked + span {
	    border-color: #398C2F;
	    -webkit-box-shadow: #398C2F 0px 0px 0px 21px inset;
	    -moz-box-shadow: #2ecc71 0px 0px 0px 21px inset;
	    box-shadow: #398C2F 0px 0px 0px 21px inset;
	    -webkit-transition: border 300ms, box-shadow 300ms, background-color 1.2s;
	    -moz-transition: border 300ms, box-shadow 300ms, background-color 1.2s;
	    -o-transition: border 300ms, box-shadow 300ms, background-color 1.2s;
	    transition: border 300ms, box-shadow 300ms, background-color 1.2s;
	    background-color: #398C2F;
	}

</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	User 
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">User List</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">User List</h3>
			<div class="box-tools pull-right">
				<a href="{{url('user/add')}}" class="btn bg-purple btn-sm pull-right" style="    margin-top: 2px;">Add User</a>
			</div>
		</div>
		<div class="box-body">
			<div class="table-wrapper">
				<table class="table table-bordered table-hover datatable">
	              	<thead>	
		                <tr>
		                  	<th rowspan="2" class="text-center" width="4%">#</th>
		                  	<th rowspan="2" class="text-center" >Country</th>
		                  	<th rowspan="2" class="text-center" >Full Name</th>
		                  	<th rowspan="2" class="text-center" >E-mail</th>
		                  	<th rowspan="2" class="text-center" >Username</th>
		                  	<th rowspan="2" class="text-center" >Supervisor</th>
		                  	<th rowspan="2" class="text-center" width="2%">Status</th>
		                  	<th colspan="2" class="text-center" width="4%">Action</th>
		                </tr>
		                <tr>
		                	<th style="display: none;" width="2%"></th>
		                	<th style="display: none;" width="2%"></th>
		                </tr>
	              	</thead>
	              	<tbody>
	              		<?php $i ?>
	              		@if(count($data) > 0)
	              			@foreach($data as $key => $value)
	              				@if(count($value->customer) == 0)
			              			<tr>
			              				<td class="text-center">{{ (($data->currentPage()-1)*$data->perPage())+($key+1) }}</td>
			              				<td>
			              					@if($value['country'])
			              						{{$value['country']->name}}
			              					@endif
			              				</td>
			              				<td>{{ $value->first_name }} {{ $value->last_name }}</td>
			              				<td>{{ ($value->email != "") ? $value->email : '-' }}</td>
			              				<td>{{ $value->username }}</td>
			              				<td>
			              					{{ ($value->supervisor_id != "") ? Sentinel::findById($value->supervisor_id)->first_name.' '.Sentinel::findById($value->supervisor_id)->last_name : '-' }}
			              				</td>
			              				<td class="text-center">
			              					@if($value->status == 1)
			              					<label class="switch switch-sm" data-toggle="tooltip" data-placement="top" title="Deactivate"><input class="user-activate" type="checkbox" checked value="{{$value->id}}"><span style="position:inherit;"><i class="handle" style="position:inherit;"></i></span></label>
			              					@else
			              					<label class="switch switch-sm" data-toggle="tooltip" data-placement="top" title="Activate"><input class="user-activate" type="checkbox" value="{{$value->id}}"><span style="position:inherit;"><i class="handle" style="position:inherit;"></i></span></label>
			              					@endif
			              				</td>
			              				<td class="text-center">
			              					@if($user->hasAnyAccess(['user.edit','admin']))
			              					{!!'<a href="#" class="blue" onclick="window.location.href=\''.url('user/edit/'.$value->id).'\'" data-toggle="tooltip" data-placement="top" title="Edit User"><i class="fa fa-pencil"></i></a>'!!}
			              					@else
			              					{!!'<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>'!!}
			              					@endif
			              				</td>
			              				<td class="text-center">
			              					@if($user->hasAnyAccess(['user.delete','admin']))
			              					{!!'<a href="#" class="red user-delete" data-id="'.$value->id.'" data-toggle="tooltip" data-placement="top" title="Delete User"><i class="fa fa-trash-o"></i></a>'!!}
			              					@else
			              					{!!'<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>'!!}
			              					@endif
			              				</td>
			              			</tr>
			              		@endif
	              			@endforeach
	              		@else
	              		<tr>
	              			<td colspan="10" align="center"> - No Data Found -</td>
	              		</tr>
	              		@endif
	              	</tbody>
	            </table>
	            <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $data->appends(Input::except('page'))->render()?>
                    </div>
                </div>
            </div>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</section><!-- /.content -->

@stop
@section('js')

<!-- datatables -->
<script type="text/javascript">
	var id = 0;
	var table = '';
	$(document).ready(function(){
		$("[data-toggle=tooltip]").tooltip();

		$('.user-activate').change(function(){
			if($(this).prop('checked')==true){
				ajaxRequest( '{{url('user/status')}}' , { 'id' : $(this).val() , 'status' : 1 }, 'post', successFunc);
			}else{
				ajaxRequest( '{{url('user/status')}}' , { 'id' : $(this).val() , 'status' : 0 }, 'post', successFunc);
			}
		});



		$('.user-delete').click(function(e){
			e.preventDefault();
			id = $(this).data('id');
			$.confirm({
			    title: 'Delete User!',
			    content: 'Are you sure?',
			    buttons: {
			        confirm: function () {
			        	deleteFunc()
			            //$.alert('Confirmed!');
			        },
			        cancel: function () {
			            $.alert('Canceled!');
			        }
			    }
			});
			sweetAlertConfirm('Delete User', 'Are you sure?',2, deleteFunc);
		});
	});

	/**
	 * Delete the menu
	 * Call to the ajax request menu/delete.
	 */
	function deleteFunc(){
		ajaxRequest( '{{url('user/delete')}}' , { 'id' : id  }, 'post', handleData);
	}

	/**
	 * Delete the menu return function
	 * Return to this function after sending ajax request to the menu/delete
	 */
	function handleData(data){
		if(data.status=='success'){
			//sweetAlert('Delete Success','Record Deleted Successfully!',0);
			$.alert('Record Deleted Successfully!');
			window.location.reload();
			//table.ajax.reload();
		}else if(data.status=='invalid_id'){
			//sweetAlert('Delete Error','User Id doesn\'t exists.',3);
			$.alert('User Id doesn\'t exists.');
		}else{
			//sweetAlert('Error Occured','Please try again!',3);
			$.alert('Please try again!');
		}
	}

	function successFunc(data){
		//table.ajax.reload();
	}
</script>
@stop
