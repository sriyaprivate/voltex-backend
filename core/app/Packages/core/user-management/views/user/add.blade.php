@extends('layouts.back_master') @section('title','Add Menu')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<style type="text/css">

	
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	User 
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
	<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('user/list')}}}">User List</a></li>
		<li class="active">Add User</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Default box -->
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Add User</h3>
		</div>
		<div class="box-body">
			<form role="form" class="form-validation" method="post" novalidate>
          			{!!Form::token()!!}
          			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          				<div class="form-group @if($errors->has('first_name')) has-error @endif">
	          				<label class="control-label required">First Name <span class="require">*</span></label>
	            			<input type="text" class="form-control input-sm" name="first_name" placeholder="First Name" value="{{Input::old('first_name')}}">
	            			@if($errors->has('first_name'))
	            				<label id="label-error" class="help-block" for="label">{{$errors->first('first_name')}}</label>
	            			@endif
	            		</div>	
          			</div>
          			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          				<div class="form-group @if($errors->has('last_name')) has-error @endif">
	          				<label class="control-label required">Last Name <span class="require">*</span></label>
	            			<input type="text" class="form-control input-sm" name="last_name" placeholder="Last Name" value="{{Input::old('last_name')}}">
	            			@if($errors->has('last_name'))
	            				<label id="label-error" class="help-block" for="label">{{$errors->first('last_name')}}</label>
	            			@endif
            			</div>
          			</div>
          			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          				<div class="form-group">
	          				<label class=" control-label">Supervisor</label>
	            			@if($errors->has('supervisor'))
	            				{!! Form::select('supervisor',$users, Input::old('supervisor'),['class'=>'chosen error input-sm','style'=>'width:100%;','required','data-placeholder'=>'Set After']) !!}
	            				<label id="supervisor-error" class="help-block" for="supervisor">{{$errors->first('supervisor')}}</label>
	            			@else
	            				{!! Form::select('supervisor',$users, Input::old('supervisor'),['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Set After']) !!}
	            			@endif
	            		</div>
          			</div>
          			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          				<div class="form-group">
	          				<label class=" control-label">Country</label>
	            			@if($errors->has('country'))
	            				{!! Form::select('country',$countries, Input::old('country'),['class'=>'chosen error input-sm','style'=>'width:100%;','required','data-placeholder'=>'Select country']) !!}
	            				<label id="country-error" class="help-block" for="country">{{$errors->first('country')}}</label>
	            			@else
	            				{!! Form::select('country',$countries, Input::old('country'),['class'=>'chosen','style'=>'width:100%;','required','data-placeholder'=>'Select country']) !!}
	            			@endif
	            		</div>
          			</div>
          			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                <div class="form-group @if($errors->has('roles[]')) has-error @endif">
		            		<label class="control-label">Role</label>
	            			@if($errors->has('roles[]'))
	            				{!! Form::select('roles[]',$roles,Input::old('roles[]'),['class'=>'chosen error input-sm', 'multiple','id'=>'roles','style'=>'width:100%;','required']) !!}
	            				<label id="label-error" class="help-block" for="label">The Role field is reqired.</label>
	            			@else
	            				{!! Form::select('roles[]',$roles, Input::old('roles[]'),['class' => 'chosen', 'multiple','id'=>'roles','style'=>'width:100%;','required']) !!}
	            			@endif
		                </div>
          			</div>
	          		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		                <div class="form-group  @if($errors->has('email')) has-error @endif">
		            		<label class="control-label required">E-mail <span class="require">*</span></label>
	            			<input type="email" class="form-control input-sm" name="email" placeholder="Email" autocomplete="off" value="{{Input::old('email')}}">
	            			@if($errors->has('email'))
	            				<label id="label-error" class="help-block" for="label">{{$errors->first('email')}}</label>
	            			@endif          		
		                </div>
	                </div>
	               	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">	
		                <div class="form-group @if($errors->has('user_name')) has-error @endif">
		            		<label class="control-label required">User Name <span class="require">*</span></label>
	            			<input type="text" class="form-control input-sm" name="user_name" placeholder="User Name" value="{{Input::old('user_name')}}">
	            			@if($errors->has('user_name'))
	            				<label id="label-error" class="help-block" for="label">{{$errors->first('user_name')}}</label>
	            			@endif
		            	</div>
	                </div>
	                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		                <div class="form-group @if($errors->has('password')) has-error @endif">
		            		<label class="control-label required">Password <span class="require">*</span></label>
	            			<input type="password" class="form-control input-sm" name="password" autocomplete="off" placeholder="Password" value="{{Input::old('password')}}">
	            			@if($errors->has('password'))
	            				<label id="label-error" class="help-block" for="label">{{$errors->first('password')}}</label>
	            			@endif
		           		</div>
		           	</div>
		           	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
		                <div class="form-group  @if($errors->has('confirmed')) has-error @endif">
		            		<label class="control-label required">Confirm Password <span class="require">*</span></label>
	            			<input type="password" class="form-control input-sm" name="password_confirmation" placeholder="Confirm Password" value="{{Input::old('confirmed')}}">
	            			@if($errors->has('confirmed'))
	            				<label id="label-error" class="help-block" for="label">{{$errors->first('confirmed')}}</label>
	            			@endif
		            	</div>
		            </div>	
	                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		                <div class="pull-right">
		                	<button type="submit" class="btn bg-purple btn-sm pull-right"><i class="fa fa-floppy-o"></i> Save</button>
		                </div>
	                </div>
            	</form>
		</div><!-- /.box-body -->
	</div><!-- /.box -->
</section><!-- /.content -->

@stop
@section('js')
@stop
