<?php
namespace Core\UserManage\Http\Requests;

use App\Http\Requests\Request;
use Input;

class UserRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){    
		if(Input::get('password') != NULL){
			$rules = [
				'first_name' => 'required',
				'last_name' => 'required',
				'email' => 'required|email|unique:users,email',
				'user_name' => 'required|min:5',
				'password' => 'required|confirmed|min:6'
				// 'roles[]' => 'required|array'
			];
		} else{
			$rules = [
				'first_name' => 'required',
				'last_name' => 'required',
				'email' => 'required|email|unique:users,email,'.Request::segment(3),
				'user_name' => 'required|min:6'
			];
		}
		
		return $rules;
	}

}
