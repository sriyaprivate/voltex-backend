<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductDelivery extends Model
{
    use SoftDeletes;
    /**
	 * The database Tag table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_delivery';

	/**
     * The id attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    //get delivery type
    public function deliveryType(){
    	return $this->belongsTo('App\Models\DeliveryType','delivery_type_id','id');
    }
}
