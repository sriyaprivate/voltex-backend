<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderTransaction extends Model
{
	use SoftDeletes;
    /**
	 * The database Tag table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'order_transaction';

	/**
     * The id attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
