<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    /**
	 * The database Tag table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'status';

	/**
     * The id attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
