<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    /**
	 * The database Tag table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_category';

	/**
     * The id attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function category(){
        return $this->belongsTo('App\Modules\Category\Models\Category','category_id','id');
    }

    public function categoryFilter()
    {
        return $this->hasMany('App\Modules\Category\Models\CategoryFilter','category_id','category_id');
    }
}
