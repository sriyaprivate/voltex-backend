<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Warrenty extends Model
{
    /**
	 * The database Tag table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'warrenty';

	/**
     * The id attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
