<?php
namespace App\Classes;

/**
*
* Dynamic Menu Generation
*
* @author Yasith Samarawickrama <yazith11@gmail.com>
* @version 1.0.0
* @copyright Copyright (c) 2015, Yasith Samarawickrama
*
*/

use Core\MenuManage\Models\Menu;
use Core\Permissions\Models\Permission;
use Sentinel;
class NestMenu{
  
  /**
   * Generate Dynamic Menu Function
   *
   * @param  Array   $menu          Arranged menu array
   */
  static function generate($menu){
    
    $html = "";
    $i    = 1;

    if(!empty($menu)){
      $html .= '<ol class="dd-list">';
      foreach ($menu as $key => $element) {
        
          if(count($element->children) == 0){            
           
              $html .= '<li class="dd-item" 
                          data-id="'.$element->id.'" 
                          data-label="'.$element->label.'"
                          data-ordering="'.$i.'">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">
                              '.$element->label.'
                               <a href="'.url("menu/edit/").'/'.$element->id.'" class="btn-link pull-right">edit</a>
                               <span class="btn-link pull-right" style="margin-left:5px;margin-right:5px"> | </span>
                               <a href="#" class="btn-link pull-right menu-delete" data-id="'.$element->id.'" >delete</a>
                            </div> 
                        </li>';
          }else{
            
            $html .= '<li class="dd-item"
                          data-id="'.$element->id.'" 
                          data-label="'.$element->label.'"
                          data-ordering="'.$i.'">';
            $html .= '<div class="dd-handle dd3-handle"></div>';            
            $html .= '<div class="dd3-content">
                        '.$element->label.'
                        <a href="'.url("menu/edit/").'/'.$element->id.'" class="btn-link pull-right">edit</a>
                       <span class="btn-link pull-right" style="margin-left:5px;margin-right:5px"> | </span>
                        <a href="#" class="btn-link pull-right menu-delete" data-id="'.$element->id.'" >delete</a>
                      </div>';            

              
            $html .= NestMenu::generate($element->children);

            $html .= "</li>";
          }
          $i++;
      }
      $html .= "</ol>";
    }
  	
  	return $html;
  }
}
