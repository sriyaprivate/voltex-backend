<?php
namespace App\Classes\PayCorp\Utils;

interface IJsonHelper {

    public function fromJson($json);

    public function toJson($instance);
    
}
