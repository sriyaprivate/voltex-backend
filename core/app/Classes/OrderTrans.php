<?php
namespace App\Classes;

/**
*
* Dynamic Menu Generation
*
* @author Yasith Samarawickrama <yazith11@gmail.com>
* @version 1.0.0
* @copyright Copyright (c) 2015, Yasith Samarawickrama
*
*/

use App\Models\OrderTransaction;
use Sentinel;

class OrderTrans{
  
    /*** Order Transaction Functions ***/    
    static function store($order_id, $status, $reference){
        
        OrderTransaction::where('order_id',$order_id)->delete();

        $res = OrderTransaction::create([
            'order_id'      =>  $order_id,
            'status'        =>  $status,
            'reference'     =>  $reference,
            'action_by'     =>  Sentinel::getUser()->id
        ]);

        if($res){
            return true;
        }else{
            return false;
        }

    }
}
