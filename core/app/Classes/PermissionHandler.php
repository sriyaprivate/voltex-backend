<?php
namespace App\Classes;

/**
*
* Handel all the perm 
*
* @author Tharindu Lakshan <info.tharindumac@gmail.com>
* @version 1.0.0
* @copyright Copyright (c) 2015, Tharindu Lakshan
*
*/

use Core\MenuManage\Models\Menu;
use Core\Permissions\Models\Permission;
use Sentinel;
class PermissionHandler{
  
  

  public static function getAllPermissions()
  {
    $per = Permission::all();
    $arr = array();
    foreach ($per as $key => $value) {
        $keys = explode('.', $value->name);
        $aa = [];
        foreach ($keys as $key) {
            $aa[$key]=$key;
        }
        array_push($arr,$aa);
    }
    return $arr;
  }


// {
//           "id": "W",
//           "text": "World",
//           "state": { "opened": true },
//           "children": [{"text": "Asia"}, 
//                        {"text": "Africa"}, 
//                        {"text": "Europe",
//                         "state": { "opened": false },
//                         "children": [ "France","Germany","UK" ]
//           }]
//         }
}
