<?php
/**
 * PRICEBOOK MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-31
 */

namespace App\Modules\Pricebook\PricebookRepository;
use App\Models\Channel;
use App\Models\PricebookChannel;
use App\Modules\Pricebook\Models\Pricebook;
use App\Modules\Pricebook\Models\ProductPricebook;
use App\Modules\Product\Models\Product;
use App\Modules\Product\Requests\PricebookRequest;
use Excel;
use DB;

class PricebookRepository{
    public function __construct(){
        
    }

    //get all channel
    public function getAllChannel($type){
    	try {
    		if($type == "get")
    		{
    			$channels = Channel::all();

	    		if(count($channels) > 0){
	    			return $channels;
	    		}else{
	    			return [];
	    		}
    		}
    		elseif($type == "list")
    		{
    			$channels = Channel::lists('name', 'id')->prepend('-- channel --', '');

	    		if(count($channels) > 0){
	    			return $channels;
	    		}else{
	    			return [];
	    		}
    		}
    		else
    		{
    			throw new \Exception('type parameter was not matched.');
    		}
    	} catch (\Exception $e) {
    		throw new \Exception('message: '.$e->getMessage());
    	}
    }

    //get product by product code
    public function getProductByCode($product_code){
    	try {
    		if(!empty($product_code)){
    			$hasProduct = Product::where('code', $product_code)->first();
    			if(count($hasProduct) > 0){
    				return $hasProduct;
    			}else{
    				return [];
    			}
    		}else{
    			throw new \Exception(' Product Code cannot be empty!.');
    		}
    	} catch (\Exception $e) {
    		throw new \Exception($e->getMessage());
    	}
    }

    //check is exists channel by id
    public function isChannelExist($pricebook_channel_id = null, $channel_id){
    	try {
    		if(!empty($channel_id)){
    			$channel = PricebookChannel::where('channel_id', $channel_id)
    				->where('status', 1);

    			if($pricebook_channel_id !== null){
    				$channel = $channel->where('id', '!=', $pricebook_channel_id);
    			}

    			$channel = $channel->first();

    			if(count($channel) > 0){
    				return $channel;
    			}else{
    				return [];
    			}
    		}else{
    			throw new \Exception('Channel not found for this ID :'.$channel_id);	
    		}
    	} catch (\Exception $e) {
    		throw new \Exception($e->getMessage());
    	}
    }

    //save pricebook
    public function savePricebook($request){
    	try {
    		DB::beginTransaction();
			
			$pricebook_name   = $request->input('pricebook_name');
			$channel_id       = $request->input('channel_id');
			$description      = $request->input('description');
			$status           = $request->input('status');
			$excel_file       = $request->file('excel_file');
			$productPricebook = null;
			$success_count    = 0;            
			$error_count      = 0; 
			$msg              = [];           

			//params: pricebook_channel_id, channel_id
			$isExist          = $this->isChannelExist(null, $channel_id);
			
			if($isExist){
				$status = "";
			}else{
				$status = "on";
			}

            $pricebook              = new Pricebook();
			$pricebook->name        = $pricebook_name;
			$pricebook->description = $description;
			$pricebook->status   = $status? '1':'0';
			$pricebook->created_at  = date('Y-m-d h:i:s');
			$pricebook->save();

			if(count($pricebook) > 0){
				$pricebookChannel               = new PricebookChannel();
				$pricebookChannel->pricebook_id = $pricebook->id;
				$pricebookChannel->channel_id   = $channel_id;
				$pricebookChannel->status       = $status? '1':'0';
				$pricebookChannel->created_at   = date('Y-m-d');
				$pricebookChannel->save();

				if(count($pricebookChannel) > 0){
					if(count($excel_file) > 0){
		            	Excel::load($excel_file, function($reader) 
		            		use($pricebook, $pricebookChannel, $status, &$success_count, &$error_count, &$msg){

		            		$reader->each(function($row) 
		            			use($pricebook, $pricebookChannel, $status, &$success_count, &$error_count, &$msg){

		            			if(isset($row->code)){
		            				$product = $this->getProductByCode($row->code);
			            			if(count($product) > 0){
										$productPricebook                       = new ProductPricebook();
										$productPricebook->product_id           = $product->id;
										$productPricebook->pricebook_channel_id = $pricebookChannel->id;
										$productPricebook->mrp                  = $row->price;
										$productPricebook->status               = $status? '1':'0';
										$productPricebook->created_at           = date('Y-m-d h:i:s');
										$productPricebook->updated_at           = date('Y-m-d h:i:s');
										$productPricebook->save();

										if(count($productPricebook) > 0){
											$success_count                      = ++$success_count;
											$msg['info'][$product->code]['status'] = 'success';
											$msg['info'][$product->code]['msg']    = 'Uploaded.';
										}else{
											$error_count                         = ++$error_count;
											$msg['info'][$product->code]['status'] = 'error';
											$msg['info'][$product->code]['msg']    = 'Data cannot saved in product_pricebook table.';
										}
			            			}else{
										$error_count                     = ++$error_count;
										$msg['info'][$row->code]['status'] = 'error';
										$msg['info'][$row->code]['msg']    = 'product not found for this code "'.$row->code.'".';
			            			}
		            			}else{
		            				throw new \Exception("Invalid Excel file, columns doesn't matched");
		            			}
		            		});

		            		DB::commit();
						});

						$msg['success_count'] = $success_count;
						$msg['error_count']   = $error_count;

		            	return json_decode(json_encode($msg));
		            }
				}else{
					DB::rollback();
					return [];
				}
			}else{
				DB::rollback();
				return [];
			}

    	} catch (\Exception $e) {
    		DB::rollback();
    		throw new \Exception('message: '.$e->getMessage());
    	}
    }

    //get pricebook by filters
    public function getPricebookByFilter($request, $perPage){
    	try {
			$keyword    = trim($request->input('search'));
			$status     = $request->input('status');
			$pricebooks = PricebookChannel::select(
					'pricebook_channel.id as pricebook_channel_id', 
					'pricebook_channel.status', 
					'c.id as channel_id',
					'p.id as pricebook_id',
					'p.name', 
					'p.description', 
					'p.created_at',
					'c.name as channel')
				->join('pricebook as p', 'pricebook_channel.pricebook_id', '=', 'p.id')
				->join('channel as c', 'pricebook_channel.channel_id', '=', 'c.id');
				
			if(isset($keyword) && !empty($keyword)){
				$pricebooks = $pricebooks->where(function($query) use($keyword){
					$query->where('p.name', 'LIKE', '%'.$keyword.'%')
						->orWhere('p.description', 'LIKE', '%'.$keyword);
				});	
			}

			if(isset($status)){
				if($status == '1' || $status == '0'){
					$pricebooks = $pricebooks->where('pricebook_channel.status', $status);
				}
			}

			if(!empty($perPage)){
				$pricebooks = $pricebooks->paginate($perPage);
			}else{
				$pricebooks = $pricebooks->first();
			}

			return $pricebooks;

    	} catch (Exception $e) {
    		throw new \Exception('message: '.$e->getMessage());
    	}
    }

    //get product by pricebook_channel_id
    public function getProductByPricebookChannelId($id, $search = null){
    	try {
    		$search = trim($search);

    		$products = PricebookChannel::select(
    							'p.id as product_id', 
    							'p.code', 
    							'p.display_name as product_name', 
    							'p.description', 
    							'p.weight', 
    							'p.width', 
    							'p.status as product_status', 
    							'p.created_at as product_added_date',
    							'c.id as channel_id',
    							'c.name as channel',
    							'pr.id as pricebook_id',
    							'pr.name as pricebook_name',
    							'pp.mrp as price',
    							'pp.status as price_status',
    							DB::raw('DATE_FORMAT(pp.created_at, "%Y-%m-%d") as product_pricebook_date')
    						)
    						->where('pricebook_channel.id', $id)
    						->join('product_pricebook as pp', 'pricebook_channel.id', '=', 'pp.pricebook_channel_id')
    						->join('product as p', 'pp.product_id', '=', 'p.id')
    						->join('pricebook as pr', 'pricebook_channel.pricebook_id', '=', 'pr.id')
    						->join('channel as c', 'pricebook_channel.channel_id', '=', 'c.id')
    						->where('pp.deleted_at', null);

				    		if(!empty($search)){
				    			$products = $products->where(function($query) use($search){
				    				$query->where('p.code', 'LIKE', '%'.$search.'%')
				    					->orWhere('p.display_name', 'LIKE', '%'.$search.'%')
				    					->orWhere('p.description', 'LIKE', '%'.$search.'%');
				    			});
				    		}

    						$products = $products->get();

    		if(count($products) > 0){
    			return $products;
    		}else{
    			return [];
    		}
    	} catch (\Exception $e) {
    		throw new \Exception($e->getMessage());
    	}
    }

    //get pricebook by id
    public function getPricebookById($id){
    	try {
			$pricebook = PricebookChannel::select(
					'pricebook_channel.id as pricebook_channel_id', 
					'c.id as channel_id',
					'p.id as pricebook_id',
					'pricebook_channel.status', 
					'p.name', 
					'p.description', 
					'p.created_at',
					'c.name as channel')
				->join('pricebook as p', 'pricebook_channel.pricebook_id', '=', 'p.id')
				->join('channel as c', 'pricebook_channel.channel_id', '=', 'c.id')
				->where('pricebook_channel.deleted_at', null);

			if(isset($id) && !empty($id)){
				$pricebook = $pricebook->where('pricebook_channel.id', $id)->first();
			}

			if(count($pricebook) > 0){
				return $pricebook;
			}else{
				throw new \Exception('Pricebook not found!.');
			}

    	} catch (Exception $e) {
    		throw new \Exception('message: '.$e->getMessage());
    	}
    }

    //delete pricebook
    public function deletePricebook($pricebook_id, $pricebook_channel_id){
    	try {
    		DB::beginTransaction();

            if(!empty($pricebook_id) && !empty($pricebook_channel_id)){

				$pricebook = Pricebook::where('id', $pricebook_id)->delete();

                if($pricebook){

                	$pricebook_channel = PricebookChannel::where('id', $pricebook_channel_id)->delete();
                	if($pricebook_channel){
						$product_pricebook = ProductPricebook::where('pricebook_channel_id', $pricebook_channel_id)->delete();

						DB::commit();
                		return $product_pricebook;           		
                	}else{

                		DB::rollback();
                		throw new \Exception("pricebook_channel couldn't be deleted.");
                	}                    
                }else{

                	DB::rollback();
                    throw new \Exception("Pricebook couldn't be deleted.");
                }
            }else{

            	DB::rollback();
                throw new \Exception('Invalid ID.');
            }
        } catch (\Exception $e) {
            throw new \Exception('message: '.$e->getMessage());
        }
    }

    //update pricebook
    public function updatePricebook($id, $request){
    	try {
    		DB::beginTransaction();

			$pricebook_id       = trim($request->input('pricebook_id'));
			$pricebook_name     = trim($request->input('pricebook_name'));
			$old_pricebook_name = trim($request->input('old_pricebook_name'));
			$channel_id         = trim($request->input('channel_id'));
			$old_channel_id     = trim($request->input('old_channel_id'));
			$description        = trim($request->input('description'));
			$old_description    = trim($request->input('old_description'));
			$status             = trim($request->input('status'));
			$old_status         = trim($request->input('old_status'));
			$selected_product   = $request->input('selected_product');
			$new_price          = $request->input('new_price');
			$old_price          = $request->input('old_price');
			$excel_file         = $request->file('excel_file');
			$pricebookChannel   = null;
			$success_count      = 0;
			$error_count        = 0;
			$msg                = [];
			
			//prams : pricebook_channel_id , channel_id
			$isExist            = $this->isChannelExist($id, $channel_id);
			
			if($status !== '' && $old_status !== '' && $status != $old_status){
				if(count($isExist) == 0){
					$status = $status;
				}elseif(count($isExist) >= 1){
					$msg['error'] = 'Something went wrong!, Only one pricebok can be available at a time!.';
					return json_decode(json_encode($msg));
				}
			}else{
				$status = '';
			}

			if(!empty($id)){
				if($pricebook_name !== $old_pricebook_name || $description !== $old_description || $status !== $old_status){

					$pricebook              = Pricebook::find($pricebook_id);
					$pricebook->name        = $pricebook_name;
					$pricebook->description = $description;
					$pricebook->status      = $status? '1': '0';
					$pricebook->updated_at  = date('Y-m-d h:i:s');
					$pricebook->update();
				}

				if($channel_id !== $old_channel_id || $status !== $old_status){
					$pricebookChannel               = PricebookChannel::find($id);
					$pricebookChannel->channel_id   = $channel_id;
					$pricebookChannel->pricebook_id = $pricebook_id;
					$pricebookChannel->status       = $status? '1': '0';
					$pricebookChannel->updated_at   = date('Y-m-d h:i:s');
					$pricebookChannel->update();
				}

				if(count($selected_product) > 0){
					foreach($selected_product as $key => $product_code){
						if($new_price[$key] !== $old_price[$key] || $status !== $old_status){
							$updated = ProductPricebook::join('product as p', 'product_pricebook.product_id', '=', 'p.id')
								->where('p.code', $product_code)
								->where('product_pricebook.pricebook_channel_id', $id)
								->update([
									'product_pricebook.mrp'        => $new_price[$key],
									'product_pricebook.updated_at' => date('Y-m-d h:i:s')
								]);

							$updated = ProductPricebook::where('product_pricebook.pricebook_channel_id', $id)
								->update([
									'product_pricebook.status'     => $status? '1':'0',
									'product_pricebook.updated_at' => date('Y-m-d h:i:s')
								]);
						}
					}
				}else{
					if($status !== $old_status){
						$updated = ProductPricebook::where('product_pricebook.pricebook_channel_id', $id)
							->update([
								'product_pricebook.status'     => $status? '1': '0',
								'product_pricebook.updated_at' => date('Y-m-d h:i:s')
							]);
					}
				}

				if(count($excel_file) > 0){
	            	Excel::load($excel_file, function($reader) 
	            		use($id, $status, &$success_count, &$error_count, &$msg, $pricebookChannel){

	            		$reader->each(function($row) 
	            			use($id, $status, &$success_count, &$error_count, &$msg, $pricebookChannel){

	            			if(isset($row->code)){
	            				$product = $this->getProductByCode($row->code);
		            			if(count($product) > 0){
									
									$deleted = ProductPricebook::where('product_id', $product->id)->delete();
									if($deleted){
										$productPricebook = new ProductPricebook();
										if(isset($pricebookChannel)){
											$productPricebook->pricebook_channel_id = $pricebookChannel->id;
										}
										$productPricebook->product_id           = $product->id;
										$productPricebook->mrp                  = $row->price;
										$productPricebook->status               = $status? '1':'0';
										$productPricebook->created_at           = date('Y-m-d h:i:s');
										$productPricebook->save();

										if(count($productPricebook) > 0){
											$success_count                      = ++$success_count;
											$msg['info'][$product->code]['status'] = 'success';
											$msg['info'][$product->code]['msg']    = 'Uploaded.';
											$msg['excel_upload']                   = true;
										}else{
											$error_count                         = ++$error_count;
											$msg['info'][$product->code]['status'] = 'error';
											$msg['info'][$product->code]['msg']    = 'Data cannot saved in product_pricebook table.';
										}
									}else{
										$productPricebook = new ProductPricebook();
										if(isset($pricebookChannel)){
											$productPricebook->pricebook_channel_id = $pricebookChannel->id;
										}
										$productPricebook->product_id           = $product->id;
										$productPricebook->mrp                  = $row->price;
										$productPricebook->status               = $status? '1':'0';
										$productPricebook->created_at           = date('Y-m-d h:i:s');
										$productPricebook->save();

										if(count($productPricebook) > 0){
											$success_count                      = ++$success_count;
											$msg['info'][$product->code]['status'] = 'success';
											$msg['info'][$product->code]['msg']    = 'Uploaded.';
											$msg['excel_upload']                   = true;
										}else{
											$error_count                         = ++$error_count;
											$msg['info'][$product->code]['status'] = 'error';
											$msg['info'][$product->code]['msg']    = 'Data cannot saved in product_pricebook table.';
										}
									}
		            			}else{
									$error_count                     = ++$error_count;
									$msg['info'][$row->code]['status'] = 'error';
									$msg['info'][$row->code]['msg']    = 'product not found for this code "'.$row->code.'".';
		            			}
	            			}else{
	            				throw new \Exception("Invalid Excel file, columns doesn't matched");
	            			}
	            		});

	            		DB::commit();
					});

					$msg['success_count'] = $success_count;
					$msg['error_count']   = $error_count;
					$msg['excel_upload']  = false;
	            }else{
	            	$msg['is_updated'] = true;
	            }				

				DB::commit();
				if(count($pricebookChannel) > 0){
					return json_decode(json_encode($msg));
				}else{
					DB::rollback();
					return [];
				}
			}else{
				DB::rollback();
				throw new \Exception('Invalid Pricebook ID!.');	
			}
    	} catch (\Exception $e) {
    		throw new \Exception('message:'.$e->getMessage());
    	}
    }


    /** 
     * This function is used to get all pricebook
     * @param 
     * @return pricebook object
     * @author Lahiru Madhusankha Perera
     */

    public function get_pricebook(){
		$pricebook = Pricebook::select('id','name')->where('status', 1)->whereNull('deleted_at')->get();
		if(!$pricebook){
			throw new \Exception("Error Occured while getting pricebook list", 1);
		}else{
			if(count($pricebook) > 0){
				return $pricebook;
			}else{
				return [];
			}
		} 
    }	

    /**
     * This function is used to get all channels
     * @param No
     * @return chanel object
     * @author Lahiru Madhusankha Perera
     */

    public function get_channel(){
    	$channels = Channel::where('status',1)->lists('name', 'id');
    	if(!$channels){
    		throw new \Exception("Error Occured while getting channel list", 1);
    	}else{
    		if(count($channels) > 0){
    			return $channels;
    		}else{
    			return [];
    		}
    	}
	    		
    }



}
