<?php
/**
 * PRICEBOOK MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-19
 */

namespace App\Modules\Pricebook\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductPricebook extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_pricebook';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];    

    public $timestamps = false;
}
