<?php
/**
 * PRICEBOOK MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-24
 */

namespace App\Modules\Pricebook\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Pricebook\Models\Pricebook;
use App\Modules\Pricebook\PricebookRepository\PricebookRepository;
use App\Modules\Pricebook\PricebookLogic\PricebookLogic;
use App\Classes\Functions;
use App\Modules\Pricebook\Requests\PricebookRequest;
use App\Modules\Pricebook\Requests\PricebookRequestOnUpdate;
use App\Models\Channel;
use Illuminate\Http\Request;
use Session;

class PricebookController extends Controller
{
    //variable declaration
    private $pricebookLogic;
    private $pricebookRepo;
    private $common;

    //assigned dependancy injection
    public function __construct(PricebookLogic $pricebookLogic, PricebookRepository $pricebookRepo, Functions $common){
        $this->pricebookRepo  = $pricebookRepo;
        $this->pricebookLogic = $pricebookLogic;
        $this->common         = $common;
    }

    /**
     * Display a listing of the pricebooks.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        try {
            $perPage    = 25;
            //params: request, page count for pagination
            $pricebooks = $this->pricebookRepo->getPricebookByFilter($request, $perPage);
            return view('Pricebook::pricebook.index', compact('pricebooks'));       
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'pricebook.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * create new pricebook
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        try {
            //params: get for get all data and list for name and id for selectbox
            $channels = $this->pricebookRepo->getAllChannel('list');

            return view('Pricebook::pricebook.create', compact('channels'));
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'pricebook.create', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Store a newly created pricebook in db.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PricebookRequest $request)
    {
        try {
            $product = $this->pricebookRepo->savePricebook($request);
            
            if(count($product) > 0){
                return redirect()->route('pricebook.create')->with(['product' => $product]);  
            }else{
                throw new \Exception("Something went wrong, pricebook couldn't be saved.");
            }
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'pricebook.create',
                'error',
                'Error!.',
                $e->getMessage()
            );
        }
    }

    /**
     * Display the specified pricebook.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id, Request $request)
    {   
        try {
            //params: id
            $pricebook = $this->pricebookRepo->getPricebookById($id);
            $products   = $this->pricebookRepo->getProductByPricebookChannelId($pricebook->pricebook_channel_id, $request->search);

            return view('Pricebook::pricebook.show', compact('pricebook', 'products'));
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'pricebook.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Show the form for editing the specified pricebook.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id, Request $request)
    {
        try {
            //params: id
            $pricebook = $this->pricebookRepo->getPricebookById($id);
            $channels  = $this->pricebookRepo->getAllChannel('list');
            $products  = $this->pricebookRepo->getProductByPricebookChannelId($pricebook->pricebook_channel_id, $request->search);
            $page      = 'edit';
            
            return view('Pricebook::pricebook.edit', compact('pricebook', 'channels', 'page', 'products'));
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'pricebook.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * Update the specified pricebook in db.
     *
     * @param  int  $id,
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, PricebookRequestOnUpdate $request)
    {
        try {
            $products = $this->pricebookRepo->updatePricebook($id, $request);

            $perPage = 25;

            if(count($products) > 0){
                if(isset($products->is_updated) && $products->is_updated == true){
                    if(isset($products->error)){
                        return $this->common->redirectWithAlert(
                            'pricebook.index', 
                            'error', 
                            'Error!.', 
                            $products->error
                        );        
                    }else{
                        return $this->common->redirectWithAlert(
                            'pricebook.index', 
                            'success', 
                            'Done!.', 
                            'Pricebook has been successfully updated!.'
                        );        
                    }                    
                }elseif(isset($products->error)){
                    return $this->common->redirectWithAlert(
                        'pricebook.index', 
                        'error', 
                        'Error!.', 
                        $products->error
                    );        
                }

                $pricebooks = $this->pricebookRepo->getPricebookByFilter($request, $perPage);
                return redirect()->route('pricebook.index')->with([
                    'products'   => $products,
                    'pricebooks' => $pricebooks
                ]);
            }else{
                throw new \Exception("message: pricebook couldn't be updated.");
            }
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'pricebook.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    /**
     * Remove the specified pricebook from db.
     *
     * @param  int  $pricebook_id, $pricebook_channel_id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($pricebook_id, $pricebook_channel_id)
    {
        try {
            $pricebook = $this->pricebookRepo->deletePricebook($pricebook_id, $pricebook_channel_id);

            if(count($pricebook) > 0){
                return $this->common->redirectWithAlert(
                    'pricebook.index', 
                    'success', 
                    'Done!.', 
                    'Pricebook has been successfully deleted!.'
                );
            }else{
                throw new \Exception("Pricebook couldn't be deleted.");
            }
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'pricebook.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }
}
