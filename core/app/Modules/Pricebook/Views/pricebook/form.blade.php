<br>
<div class="form-group row {{ $errors->has('pricebook_name') ? 'has-error' : ''}}">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right required" title="name">Pricebook name</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        @if(isset($pricebook->pricebook_id))
            <input type="hidden" name="pricebook_id" value="{{ $pricebook->pricebook_id }}">
        @endif
        <input type="text" name="pricebook_name" id="pricebook_name" class="form-control" placeholder="Enter pricebook name..." value="{{ isset($pricebook->name)? $pricebook->name : old('pricebook_name') }}">
        @if($errors->has('pricebook_name'))
            <span class="help-block">
                {{ $errors->first('pricebook_name') }}
            </span>
        @endif
    </div>  
</div>
<div class="form-group row {{ $errors->has('channel') ? 'has-error' : ''}}">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right required" title="channel">Channel</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        {!! Form::select('channel_id', $channels, isset($pricebook->channel_id)? $pricebook->channel_id:old('channel_id'), [
                'class' => 'form-control chosen',
                'id'    => 'channel_id'
            ]) 
        !!}
        @if($errors->has('channel_id'))
            <span class="help-block">
                {{ $errors->first('channel_id') }}
            </span>
        @endif
    </div>  
</div>
<div class="form-group row">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right" title="Description">Description</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        <textarea name="description" rows="5" class="form-control" placeholder="Enter description.">{{ isset($pricebook->description)?$pricebook->description:old('description') }}</textarea>
    </div>
</div>
@if(isset($page) && $page == 'edit')
<div class="form-group row">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right" title="Description">Status</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        <label class="switch">
            <input type="checkbox" name="status" {{ $pricebook->status == 1? 'checked':'' }}>
          <span class="slider"></span>
        </label>
    </div>
</div>
@else
<div class="row">
    <div class="col-lg-12 col-lg-offset-1">
        <div class="form-group row">
            <div class="col-lg-2 col-md-2 col-ms-2 col-xs-2 col-lg-offset-8">
                <a href="{{ asset('assets/files/example.xls') }}" class="btn btn-default btn-block"><span class="fa fa-download"></span> Example Excel</a>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-lg-offset-1">
        <div class="form-group row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <label class="control-label pull-right" title="Description">Status</label>
            </div>
            <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
                <label class="switch">
                    <input type="checkbox" name="status" checked>
                  <span class="slider"></span>
                </label>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <label class="control-label pull-right" title="Description">Excel File</label>
            </div>
            <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
                <span class="btn bg-purple btn-file btn-block">
                    Browse <input type="file" name="excel_file" id="excel_file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" >
                </span>
                @if($errors->has('excel_file'))
                    <p class="padding-top-10 text-red">
                      {{ $errors->first('excel_file') }}  
                    </p>
                @endif
            </div>
        </div>
    </div>
</div>
@endif
<div class="form-group row">
    <div class="col-lg-2 col-md-2 col-ms-2 col-xs-3 col-lg-offset-9 col-md-offset-9  col-ms-offset-9  col-xs-offset-8">
        <button type="submit" name="btnSubmit" id="btnSubmit" class="btn bg-purple btn-block" disabled><span class="fa fa-save"></span> {{ isset($submitButtonText) ? $submitButtonText : 'Create' }}</button>
    </div>      
</div>
@section('common_js')
<script>
    $(document).ready(function() {
      $('#pricebook_form').on('change keyup', 'input, select, textarea', function(evt){

        if($('#pricebook_name').val().trim().length > 0 && $('#channel_id').val() != '' && $("#excel_file").get(0).files.length > 0){
            $('#btnSubmit').attr('disabled', false);
        }else{
            $('#btnSubmit').attr('disabled', true);
        }
      });
    });
</script>
@stop