@extends('layouts.back_master') @section('title','Add Menu')
@section('css')
<style type="text/css">
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
</style>
<script>
    function diasbledSubmitButton(form){
        form.btnSubmit.disabled = true;
        form.btnSubmit.text = 'Waiting...';
        return true;
    }
</script>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Pricebook 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li>Pricebook</li>
        <li class="active">Add</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Pricebook</h3>
            <div class="box-title pull-right">
                <button class="btn btn-warning btn-xs" onclick="goBack()"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
            </div>
        </div>
        <div class="box-body">
            {!! Form::open([
                    'route'  => 'pricebook.store', 
                    'class'  => 'form-horizontal', 
                    'id'     => 'pricebook_form', 
                    'method' => 'post',
                    'onsubmit' => 'return diasbledSubmitButton(this)',
                    'files'  => true
                ]) 
            !!}
                @include ('Pricebook::pricebook.form')
            {!! Form::close() !!}
            
            @if(Session::get('product') !== null)
                <div class="row">
                    <div class="col-lg-9 col-lg-offset-2">
                        <br>
                        <br>
                        <br>
                        <table class="table table-striped table-bordered">
                            <tr class="color-dark-blue">
                                <th>Product code</th>
                                <th>Message</th>
                                <th>Upload status</th>
                            </tr>
                            @foreach(Session::get('product')->info as $key => $info)
                                <tr>
                                    <td>{{ $key }}</td>
                                    <td>{{ $info->msg }}</td>
                                    <td>{!! $info->status == 'success'? '<span class="fa fa-check text-green"></span> success': '<span class="fa fa-times text-red"></span> error'!!}</td>
                                </tr>
                            @endforeach
                        </table>
                        <div class="pull-right">
                            <table class="padding-10">
                                <tr>
                                    <td><strong><p class="text-green">Success</p></strong></td>
                                    <td class="padding-5"><p>{{ Session::get('product')->success_count !== null? Session::get('product')->success_count :'-' }}</p></td>
                                </tr>
                                <tr>
                                    <td><strong><p class="text-red">Error</p></strong></td>
                                    <td class="padding-5"><p>{{ Session::get('product')->error_count !== null? Session::get('product')->error_count :'-' }}</p></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>  
</section>
@stop
@section('js')
@yield('common_js')
<script type="text/javascript">
$(document).ready(function() {
  
});
</script>
@stop




































