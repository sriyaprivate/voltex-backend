<?php
/**
 * CATEGORY MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-09
 */
 
namespace App\Modules\Pricebook\Requests;

use App\Http\Requests\Request;

class PricebookRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$id = $this->id;
		$rules = [
			'pricebook_name' => 'required|unique:pricebook,name,'.$id.',,deleted_at,NULL',
			'channel_id'     => 'required',
			'excel_file'     => 'required|mimes:xls,xlsx',
		];
		
		return $rules;
	}

}
