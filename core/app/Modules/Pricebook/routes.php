<?php
/**
 * PRICEBOOK MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-19
 */
 
Route::group(['middleware' => ['auth']], function(){
    Route::group(['prefix' => 'pricebook', 'namespace' => 'App\Modules\Pricebook\Controllers'],function(){

    	/*===================================
					 Pricebook GET request
    	===================================*/
		Route::get('create', [
			'uses' => 'PricebookController@create',
			'as'   => 'pricebook.create'
        ]);

        Route::get('/', [
			'uses' => 'PricebookController@index',
			'as'   => 'pricebook.index'
        ]);

        Route::get('delete/pricebook_id/{pricebook_id}/pricebook_channel_id/{pricebook_channel_id}', [
			'uses' => 'PricebookController@destroy',
			'as'   => 'pricebook.destroy'
        ]);

        Route::get('show/{id}', [
            'uses' => 'PricebookController@show',
            'as'   => 'pricebook.show'
        ]);

        Route::get('edit/{id}', [
            'uses' => 'PricebookController@edit',
            'as'   => 'pricebook.edit'
        ]);


        /*===================================
					POST request
    	===================================*/
    	Route::post('store', [
    		'uses' => 'PricebookController@store',
    		'as'   => 'pricebook.store'
    	]);

        Route::post('update/{id}', [
            'uses' => 'PricebookController@update',
            'as'   => 'pricebook.update'
        ]);
	});  	
}); 

