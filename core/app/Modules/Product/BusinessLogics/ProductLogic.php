<?php namespace App\Modules\Product\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use DB;
use Exception;
use Excel;

use App\Models\ProductCategory;
use App\Models\CategoryType;
use App\Models\Country;

use App\Modules\Product\Models\Product;
use App\Modules\Product\Models\Image;
use App\Modules\Product\Models\Datasheet;
use App\Modules\Category\Models\Category;
use App\Modules\Pricebook\Models\ProductPricebook;
use App\Modules\Category\CategoryRepository\CategoryRepository;

class ProductLogic{

	protected $catRepo;

	public function __construct(CategoryRepository $category_repository){
	        $this->catRepo  = $category_repository;
	    }

	/** 
	 * This function is used to get all tags
	 * @param String $tag
	 * @return array
	 */

	public function searchTag($tag){
		return $tag = Tag::where('name','LIKE','%'.$tag.'%')
					      ->lists('name','id');
	}

	/** 
	 * This function is used to get all delivery types
	 * @param 
	 * @return array
	 */

	public function getDeliveryType(){
		$delivery_type = DB::table('delivery_type')
						    ->select('id','name')
							->where('status',1)
							->whereNull('deleted_at')
							->get();
		if(count($delivery_type) > 0){
			return $delivery_type;
		}else{
			return [];
		}
	}


	/** 
	 * Get tax types
	 * @param no
	 * @return object 
	 */
	public function getTaxType(){
		$taxType = TaxType::where('status',1)
							->whereNull('deleted_at')
							->lists('name','id');
		if(!$taxType){
			throw new Exception("Error occured while getting tax types", 1);
		}
		return $taxType;
	}
	
	/** 
	 * This function is used to add product master data
	 * @param String $product_array
	 * @return $last_insert_id
	 */

	public function addProduct($form_data,$img_arr){
		DB::beginTransaction();
		/** ===================  product master data ==================== */
		empty($form_data['display_name']) ? $display_name = $form_data['item_name'] : $display_name = $form_data['display_name'];
		$product = Product::create([
			'country_id'	   		=> $form_data['country'],
			'code'	   				=> trim($form_data['item_code']),
			'name'	   				=> trim($form_data['item_name']),
			'display_name'			=> trim($display_name),
			'description'			=> $form_data['description'],
			'selling_price'			=> $form_data['selling_price'],
			'tax_type_id'   		=> $form_data['tax_type']
		]);
		if(!$product){
			DB::rollback();
			throw new Exception("Error occured while adding product master data");
		}

		/** ================== product category ======================== */
		$product_category = [];
		
		if(!empty($form_data['sub_category'])){
			array_push($product_category,[
				'product_id'  => $product->id,
				'category_id' => $form_data['sub_category'],
				'created_at'  => date('Y-m-d H:i:s'),
				'updated_at'  => date('Y-m-d H:i:s')
			]);
		}
		$pro_cat = ProductCategory::insert($product_category);

		if(!$pro_cat){
			DB::rollback();
			throw new Exception("Error occured while adding product category");
		}
		
		/** ================ product images ================================= */
		if(count($img_arr) > 0){
			$images_array = [];
			foreach ($img_arr as $key => $image_path) {
				array_push($images_array,[
					'product_id' => $product->id,
					'image' 	 => $image_path,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			$image = Image::insert($images_array);
	        if(!$image){
	            DB::rollback();
	            throw new Exception("Error occured while adding images");
	        }
		}
		
		DB::commit();
	}
	
	/** 
	 * This function is used to edit product master data
	 * @param Integer $productId Array $form_data Array $img_arr
	 * @return 
	 */

	public function updateProduct($productId,$form_data,$img_arr){
		DB::beginTransaction();
		/** ===================  update product master data ==================== */
		empty($form_data['display_name']) ? $display_name = $form_data['item_name'] : $display_name = $form_data['display_name'];
		$product = Product::where('id',$productId)
					->whereNull('deleted_at')
					->update([
						'name'	   				=> trim($form_data['item_name']),
						'display_name'			=> trim($display_name),
						'description'			=> $form_data['description'],
						'selling_price'			=> $form_data['selling_price'],
						'tax_type_id'   		=> $form_data['tax_type']
					]);
		if(count($product) == 0){
			DB::rollback();
			throw new Exception("Error occured while updating product master data");
		}



		/** ================ product images ================================= */
		if(!empty($form_data['delete_images'])){
			$imgArr = explode(',',$form_data['delete_images']);
			if(count($imgArr) > 0){
				$removeImage = Image::whereIn('id',$imgArr)->delete();
				if(count($removeImage) > 0){
					$getImage = Image::whereIn('id',$imgArr)->get();
					if(count($getImage) > 0){
						foreach ($getImage as $key => $value) {
							$imageFile = UPLOAD_PATH.'uploads/images/product/'.$getImage[$key]->image;
							if(file_exists($imageFile)){
								unlink($imageFile);
							}
						}
					}
				}
			}
		}
		if(count($img_arr) > 0){
			$images_array = [];
			foreach ($img_arr as $key => $image_path) {
				array_push($images_array,[
					'product_id' => $productId,
					'image' 	 => $image_path,
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			$image = Image::insert($images_array);
	        if(!$image){
	            DB::rollback();
	            throw new Exception("Error occured while adding images");
	        }
		}
		
		DB::commit();
	}


	//get all category type
	public function getAllCategoryByType($type){
		try {
			$category = Category::where('category_type_id',$type)->lists('name', 'id');
			
			if(count($category) > 0){
				return $category;
			}else{
				return [];
			}
		} catch (\Exception $e) {
			throw new \Exception('message: '.$e->getMessage());
		}
	}		

	/**
     * This function is used to search product
     * @param 
     * @return 
     */

	public function searchProduct($search,$type,$category_id,$status){

		$tmp = 	Product::with('product_category.category.type')->whereNull('deleted_at');
		
		if(!empty($search)){
			$tmp = 	$tmp->where('name','LIKE','%'.trim($search).'%')
					   ->orWhere('display_name','LIKE','%'.trim($search).'%')
					   ->orWhere('code','LIKE','%'.trim($search).'%')
					   ->whereNull('deleted_at');
		}

		if(!empty($category_id)){

			// $_cat_exist = Category::find($category_id);

			// if($_cat_exist){
			// 	$_sub_categories = $_cat_exist->getDescendants()->pluck(['id']);
			// }

			$_sub_categories[]=(Integer)$category_id;

			// return $_sub_categories = $this->catRepo->getChildrenOfCategory($category_id);

			$tmp = $tmp->whereIn('id',function($query)use($_sub_categories){
				$query->select('product_id')->from('product_category')->whereIn('category_id',$_sub_categories);
			});
		}

		if(!empty($status)){
			if($status == 1){
				$tmp = $tmp->whereIn('status',unserialize(PRODUCT_CHECK_STATUS))->whereNull('deleted_at');;
			}else{
				$tmp = $tmp->where('status',0)->whereNull('deleted_at');;
			}	
		}

		$tmp = $tmp->orderBy('created_at','desc');
		$product_details = $tmp->paginate(unserialize(SHOW_OPTION)[1]);

		return $product_details;
	}


    /**
     * This function is used to get product list
     * @param 
     * @return 
     */

    public function getProductList(){    	

    	$product_list = Product::select('*')
						->with('product_category.category.type','stock','country')
						->orderBy('created_at','desc')
						->whereNull('deleted_at')
				  		->paginate(unserialize(SHOW_OPTION)[1]);
    	if(!$product_list){
    		throw new Exception("Error occured while getting product list");
    	}else{
    		return $product_list;
    	}
    }

    /** 
     * This function is used to get produt details
     * @param Integer $id
     * @return 
     */

    public function getProductDetails($id){
    	return $product_details = Product::with(
						    		'product_category.category.type',
						    		'productImage',
						    		'product_category.categoryFilter.main_filter',
						    		'product_category.categoryFilter.filter_values.belongsProduct'
						    	)
						    	->where('id',$id)
						    	->whereNull('product.deleted_at')
						    	->first();
    }

    /**
     * This function is used to get only produt details
     * @param Integer $product_id
     * @return product_list object
     */

    public function get_product($product_id){
    	try{
    		$product = Product::select('id',
							'tax_type_id',
							'code',
							'name',
							'status',
							'created_at',
							'updated_at',
							'deleted_at')
					->where('id',$product_id)
					->whereNull('deleted_at')
					->get();
    		if(count($product) > 0){
    			return $product;
    		}else{
    			return [];
    		}
    	}catch(Exception $e){
    		throw new Exception($e->getMessage());
    	}
    }

    /** 
    * This function is used to update home page decide produts
    * @param Integer $status Integer $product_id
    * @return 
    */

    public function homePageProduct($section_id,$product_id){
    	DB::beginTransaction();
    	//check product have deal & select section is deal
    	if($section_id > 0){
	    	$checkProduct = Product::where('id',$product_id)->whereNull('deleted_at')->first();
	    	if(count($checkProduct) > 0){
	    		if($checkProduct->status == 2){
		    		if($section_id == 2){
		    			$homeProduct = Product::updateOrCreate([
			    		'id' 		  => $product_id
				    	],[
				    		'updated_at'  => date('Y-m-d H:i:s')
				    	]);
						if(!$homeProduct){
							throw new Exception("Error Processing Request", 1);
							DB::rollback();
						}
						DB::commit();
						return 1;
		    		}
		    	}else{
		    		$homeProduct = Product::updateOrCreate([
			    		'id' 		  => $product_id
			    	],[
			    		'updated_at'  => date('Y-m-d H:i:s')
			    	]);
					if(!$homeProduct){
						throw new Exception("Error Processing Request", 1);
						DB::rollback();
					}
					DB::commit();
					return 1;
	    		}
	    	}else{

	    	}
	    }else{
	    	$homeProduct = Product::updateOrCreate([
	    		'id' 		  => $product_id
	    	],[
	    		'updated_at'  => date('Y-m-d H:i:s')
	    	]);
			if(!$homeProduct){
				throw new Exception("Error Processing Request", 1);
				DB::rollback();
			}
			DB::commit();
			return 1;
	    }
    } 

    /** 
     * This function is used to delete product
     * @param Integer $product_id
     * @return 
     */

    public function deleteProduct($product_id){
    	$cartProduct = DB::table('cart')
    				 ->where('product_id',$product_id)
    				 ->whereNull('deleted_at')
    				 ->get();
    	
    	if(count($cartProduct) > 0){
    		return 0;
    	}else{
    		/** remove product from the promotion */
    		$product_promotion 	= PromotionAllocate::where('product_id',$product_id)->delete();		   
    		if(count($product_promotion) == 0){
    			throw new Exception("Error occured when deleting product from promotion",1);		
    		}
    		/** remove product **/
			$product = Product::where('id',$product_id)->delete();
			if(count($product) == 0){
				throw new Exception("Error occured when deleting product",1);
			}else{
				return 1;
			}    		
    	}
    }


    public function insertBulkImage($imageName, $imageNameWOExt=null){
        $productI = explode('_',$imageNameWOExt);

        if(count($productI) > 0) {
            $last = array_splice($productI,count($productI)-1,1)[0];
            $proCode = implode('_',$productI);

            $product = Product::where('code', $proCode)->first();

            if($product){
                $image = Image::where('image',$imageName)->first();

                if(!$image) {
                    return [
                        'product_id'    => $product->id,
                        'image'         => $imageName,
                        'status'        => 1,
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s')
                    ];
                }
            }
        }

        return null;
    }

    public function insertBulkDatasheet($sheetName, $sheetNameWOExt=null){
        $productI = explode('_',$sheetNameWOExt);

        if(count($productI) > 0) {
            $last = array_splice($productI,count($productI)-1,1)[0];
            $proCode = implode('_',$productI);

            $product = Product::where('code', $proCode)->first();

            if($product){
                $sheet = Datasheet::where('datasheet',$sheetName)->first();

                if(!$sheet) {
                    return [
                        'product_id'    => $product->id,
                        'datasheet'     => $sheetName,
                        'status'        => 1,
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s')
                    ];
                }
            }
        }

        return null;
    }

    public function getAllCountries(){
    	$countries = Country::whereNull('deleted_at')->lists('name','id');

    	return $countries;
    }

    public function uploadProductExcel($data)
	{

		$file 		= $data->data_file;
		$category 	= $data->sub_category;
		$country 	= $data->country;

		$data = [];
		Excel::load($file, function($reader) use (&$data, $category, $country){

			$reader->each(function($row) use (&$data, $category, $country){

				try{

					$_pro_exist = Product::where('code',$row->code)->first();

					if(!$_pro_exist){

						$product = Product::create([
							'country_id'	   		=> $country,
							'code'	   				=> $row->code,
							'name'	   				=> $row->name,
							'display_name'			=> $row->display_name,
							'description'			=> $row->description,
							'price_term'			=> $row->price_term,
							'selling_price'			=> $row->selling_price,
							'tax_type_id'   		=> 1,
							'moq'   				=> $row->moq,
							'lead_time'   			=> $row->lead_time,
							'specs'   				=> json_encode($row)
						]);

						if($product){
							$res = ProductCategory::create([
								'product_id'  	=> $product->id,
								'category_id'	=> $category
							]);

							array_push($data, $res);
						}else{
							array_push($data, [
								'code' => $row->code,
								'status' => 0,
								'message' => 'No product found'
							]);
						}

					}else{
						
						$_pro_exist->country_id	   	= $country;
						$_pro_exist->code	   		= $row->code;
						$_pro_exist->name	   		= $row->name;
						$_pro_exist->display_name	= $row->display_name;
						$_pro_exist->description	= $row->description;
						$_pro_exist->price_term		= $row->price_term;
						$_pro_exist->selling_price	= $row->selling_price;
						$_pro_exist->tax_type_id   	= 1;
						$_pro_exist->moq   			= $row->moq;
						$_pro_exist->lead_time   	= $row->lead_time;
						$_pro_exist->specs   		= json_encode($row);

						if($_pro_exist->save()){
							array_push($data, [
								'code' => $row->code,
								'status' => 1,
								'message' => 'Product Details Updated'
							]);
						}else{
							array_push($data, [
								'code' => $row->code,
								'status' => 0,
								'message' => 'No roduct found'
							]);
						}
						
					}
					
				}catch(\Exception $e){
					array_push($data, [
						'code' => $row->code,
						'status' => 0,
						'message' => $e->getMessage()
					]);
				}
			});
		});

		return $data;
	}

	public function separateProductExcel($data)
	{

		$file 		= $data->data_file;

		$data = [];

		Excel::load($file, function($reader) use (&$data){

			$reader->each(function($row) use (&$data){

				$data[]=$row;
				
			});

		});

		$tmp = $this->array_group_by($data,'product_category');

		foreach ($tmp as $key => $value) {
			$file_name = $key;

			$_sheet_data = [];

			array_push($_sheet_data, ['code','name','display_name','selling_price','price_term','description','moq','lead_time','Security Application','Unit Of Measure','UPC.','Carton QTY','Inner Pack QTY (MIN Order)','Carton Length (cm)','Carton Width (cm)','Carton Hight (cm)','Carton GW (kg)','Carton NW (kg)','Origin']);

			foreach ($value as $tmp1) {
				$dd = [];
				array_push($dd,$tmp1['item']);
				array_push($dd,$tmp1['description']);
				array_push($dd,$tmp1['description']);
				array_push($dd,$tmp1['price_orel']);
				array_push($dd,'');
				array_push($dd,$tmp1['description']);
				array_push($dd,'');
				array_push($dd,'');
				array_push($dd,$tmp1['security_application']);
				array_push($dd,$tmp1['unit_of_measure']);
				array_push($dd,$tmp1['upc.']);
				array_push($dd,$tmp1['carton_qty']);
				array_push($dd,$tmp1['inner_pack_qty_min_order']);
				array_push($dd,$tmp1['carton_length_cm']);
				array_push($dd,$tmp1['carton_width_cm']);
				array_push($dd,$tmp1['carton_hight_cm']);
				array_push($dd,$tmp1['carton_gw_kg']);
				array_push($dd,$tmp1['carton_nw_kg']);
				array_push($dd,$tmp1['origin']);

				array_push($_sheet_data, $dd);
			}

			// return $_sheet_data;

	        Excel::create($file_name, function($excel) use($_sheet_data) {

	            $excel->sheet('Sheetname', function($sheet) use($_sheet_data){

	                $sheet->fromArray($_sheet_data, null, 'A1', false, false);

	            });

	        })->store('xls', storage_path('excel/exports'));

		}

	}

	public function array_group_by(array $array, $key)
		{
			if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
				trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
				return null;
			}
			$func = (!is_string($key) && is_callable($key) ? $key : null);
			$_key = $key;
			// Load the new array, splitting by the target key
			$grouped = [];
			foreach ($array as $value) {
				$key = null;
				if (is_callable($func)) {
					$key = call_user_func($func, $value);
				} elseif (is_object($value) && property_exists($value, $_key)) {
					$key = $value->{$_key};
				} elseif (isset($value[$_key])) {
					$key = $value[$_key];
				}
				if ($key === null) {
					continue;
				}
				$grouped[$key][] = $value;
			}
			// Recursively build a nested grouping if more parameters are supplied
			// Each grouped array value is grouped according to the next sequential key
			if (func_num_args() > 2) {
				$args = func_get_args();
				foreach ($grouped as $key => $value) {
					$params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
					$grouped[$key] = call_user_func_array('array_group_by', $params);
				}
			}
			return $grouped;
		}
    	
}
