<?php namespace App\Modules\Product\Models;

/**
*
* Product Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model {
    
    use SoftDeletes;
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product';
     /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
    * Create relationship with location.
    */

    public function product_category(){
        return $this->hasMany('App\Models\ProductCategory','product_id','id')
                    ->where('status',1)
                    ->whereNull('deleted_at');
    }

    public function stock(){
        return $this->hasMany('App\Modules\Stock\Models\Stock','product_id','id');
    }

    //get product images
    public function productImage(){
        return $this->hasMany('App\Modules\Product\Models\Image','product_id','id');
    }

    //get product images
    public function productDatasheet(){
        return $this->hasMany('App\Modules\Product\Models\Datasheet','product_id','id');
    }

    //product has country
    public function country(){
        return $this->belongsTo('App\Models\Country','country_id','id');
    }        

}
