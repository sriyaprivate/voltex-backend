<?php namespace App\Modules\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
	use SoftDeletes;
    /**
	 * The database Tag table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'product_image';

	protected $dates = ['deleted_at'];

	/**
     * The id attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
}
