<?php

Route::group(['middleware' => ['auth']], function()
{ 
	Route::group(array('prefix'=>'product','namespace' => 'App\Modules\Product\Controllers'), function() {
		
	    /*get routes*/
	    Route::get('list', ['as' => 'product.list', 'uses' => 'ProductController@index']);
	    
	    Route::get('add', ['as' => 'product.add', 'uses' => 'ProductController@create']);

	    Route::get('tag', ['as' => 'product.add', 'uses' => 'ProductController@tag']);

	    Route::get('edit/{id}', ['as' => 'product.add', 'uses' => 'ProductController@edit']);

	    Route::get('get-category-hierarchy', ['as' => 'product.add', 'uses' => 'ProductController@getCategoryHierarchy']);

	    Route::get('get-sub-category', ['as' => 'product.add', 'uses' => 'ProductController@category_filter']);

	    Route::get('search', ['as' => 'product.add', 'uses' => 'ProductController@search']);

	    Route::get('view/{id}', ['as' => 'product.add', 'uses' => 'ProductController@show']);

	    Route::get('home-decide', ['as' => 'product.add', 'uses' => 'ProductController@homeDecide']);

	    Route::get('delete', ['as' => 'product.delete', 'uses' => 'ProductController@destroy']);

	    Route::get('upload', ['as' => 'product.upload', 'uses' => 'ProductController@uploadProductView']);

	    Route::get('bulk-upload', ['as' => 'product.bulkupload', 'uses' => 'ProductController@bulkUploadView']);

	    Route::get('excel-separate', ['as' => 'product.excel', 'uses' => 'ProductController@separateExcelView']);

	    /*post routes*/
	    Route::post('add', ['as' => 'product.add', 'uses' => 'ProductController@store']);

	    Route::post('edit/{id}', ['as' => 'product.add', 'uses' => 'ProductController@update']);

	    Route::post('upload', ['as' => 'product.upload', 'uses' => 'ProductController@uploadProducts']);

	    Route::post('bulk-upload', ['as' => 'product.bulkupload', 'uses' => 'ProductController@bulkUpload']);
	    
	    Route::post('excel-separate', ['as' => 'product.excel', 'uses' => 'ProductController@separateExcel']);
	}); 
});