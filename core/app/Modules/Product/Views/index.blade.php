@extends('layouts.back_master') @section('title','View Product')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/bootstrap-tokenfield.css')}}">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Product
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('product/list')}}}">Product List</a></li>
		<li class="active">View</li>
	</ol>
</section>
{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
    <form role="form" class="form-horizontal form-validation" method="post" autocomplete="off" enctype="multipart/form-data">
    	<div class="box">
            <!-- <div class="box"> -->
        		<div class="box-header with-border">
        			<h3 class="box-title">View Product</h3>
        		</div>
        	    <div class="box-body">
                    {!!Form::token()!!}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Item Code <span class="require">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control input-sm" value="" readonly>
                        </div>
                         <label class="col-sm-2 control-label">Item Name <span class="require">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control input-sm">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Big Store <span class="require">*</span></label>
                        <div class="col-sm-4">
                            <select id="big_store_category" class="form-control input-sm chosen" name="big_store_category">
                                <option value="">Select Big Store Category</option>
                                @foreach($big_store_category as $key => $value)
                                    <option value="{{$key}}" @if(Input::old('big_store_category') == $key) selected @endif>{{$value}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('big_store_category'))
                                <label id="label-error" class="error" for="label">The big store category field is required when other category is not selected</label>
                            @endif
                        </div>
                        <label class="col-sm-2 control-label">Bulk Market <span class="require">*</span></label>
                        <div class="col-sm-4">
                            <select id="bulk_market_category" class="form-control input-sm chosen" name="bulk_market_category">
                                <option value="">Select Bulk Market Category</option>
                                @foreach($bulk_market_category as $key => $value)
                                    <option value="{{$key}}" @if(Input::old('bulk_market_category') == $key) selected @endif>{{$value}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('bulk_market_category'))
                                <label id="label-error" class="error" for="label">The bulk market category field is required when other category is not selected</label>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Savings <span class="require">*</span></label>
                        <div class="col-sm-4">
                            <select id="savings" class="form-control input-sm chosen" name="savings">
                                <option value="">Select Savings Category</option>
                                @foreach($savings as $key => $value)
                                    <option value="{{$key}}" @if(Input::old('savings') == $key) selected @endif>{{$value}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('savings'))
                                <label id="label-error" class="error" for="label">The savings category field is required when other category is not selected</label>
                            @endif
                        </div>
                        <label class="col-sm-2 control-label">Supplier </label>
                        <div class="col-sm-4">
                           <select id="supplier" class="form-control input-sm chosen" name="supplier">
                                <option value="">Select Supplier</option>
                                @foreach($all_supplier as $key => $value)
                                    <option value="{{$key}}" @if($supplier == $key) selected @endif>{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Display Name </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control input-sm" name="display_name" placeholder="Product Display Name" value="{{Input::old('display_name')}}">
                            @if($errors->has('display_name'))
                                <label id="label-error" class="error" for="label">{{$errors->first('display_name')}}</label>
                            @endif
                        </div>                        
                    </div>
                    <div class="form-group @if($errors->has('supplier')) has-error @endif">
                        <label class="col-sm-2 control-label">Tags </label>
                        <div class="col-sm-10">
                            <input type="text"  class="form-control input-sm" name="tag" placeholder="Tags" id="tag" value="{{Input::old('tag')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Warranty</label>
                        <div class="col-sm-2">
                            <input type="radio" name="warranty_available" id="warrenty_available" value="1" @if(Input::old('warranty_available') == 1) checked @endif> Available
                        </div>
                        <div class="col-sm-2">
                            <input type="radio" name="warranty_available" id="warrenty_not_available" value="0" @if(Input::old('warranty_available') == 0) checked @endif> Not Available
                        </div>
                        <div class="warranty_period @if(Input::old('warranty_available') == 0) hide @endif">
                            <label class="col-sm-2 control-label">Warranty Period</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control input-sm" name="warrenty_period" placeholder="Warrenty Period" value="{{Input::old('warrenty_period')}}">
                                @if($errors->has('warrenty_period'))
                                    <label id="label-error" class="error" for="label">{{$errors->first('warrenty_period')}}</label>
                                @endif
                            </div>
                            <div class="col-sm-2">
                                <select id="period_type" class="form-control input-sm chosen" name="period_type">
                                    <option value="M">Months</option>
                                    <option value="D">Days</option>
                                    <option value="Y">Year</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group @if($errors->has('supplier')) has-error @endif">
                        <label class="col-sm-2 control-label">Dimensions </label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control input-sm" name="weight" placeholder="Weight(1G)" value="{{Input::old('weight')}}">
                            @if($errors->has('weight'))
                                <label id="label-error" class="error" for="label">{{$errors->first('weight')}}</label>
                            @endif
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control input-sm" name="length" placeholder="Length(1M)" value="{{Input::old('length')}}">
                            @if($errors->has('length'))
                                <label id="label-error" class="error" for="label">{{$errors->first('length')}}</label>
                            @endif
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control input-sm" name="breadth" placeholder="Breadth(1M)" value="{{Input::old('breadth')}}">
                            @if($errors->has('breadth'))
                                <label id="label-error" class="error" for="label">{{$errors->first('breadth')}}</label>
                            @endif
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control input-sm" name="height" placeholder="Height(1M)" value="{{Input::old('height')}}">
                            @if($errors->has('height'))
                                <label id="label-error" class="error" for="label">{{$errors->first('height')}}</label>
                            @endif
                        </div>
                    </div>
                    <div class="form-group @if($errors->has('supplier')) has-error @endif">
                        <label class="col-sm-2 control-label">Return</label>
                        <div class="col-sm-2">
                            <input type="radio" name="return_available" id="return_available" value="1"  @if(Input::old('return_available') == 1) checked @endif> Available
                        </div>
                        <div class="col-sm-2">
                            <input type="radio" name="return_available" id="return_not_available" value="0"  @if(Input::old('return_available') == 0) checked @endif> Not Available
                        </div>
                        <div class="return_period @if(Input::old('return_available') == 0) hide @endif">
                            <label class="col-sm-2 control-label">Return Period</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control input-sm" name="return" placeholder="Return Period" value="{{Input::old('return')}}">
                                @if($errors->has('return'))
                                    <label id="label-error" class="error" for="label">{{$errors->first('return')}}</label>
                                @endif
                            </div>
                            <div class="col-sm-2">
                                <select id="return_period" class="form-control input-sm chosen" name="return_period">
                                    <option value="M">Months</option>
                                    <option value="D">Days</option>
                                    <option value="Y">Year</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group @if($errors->has('supplier')) has-error @endif">
                        <label class="col-sm-2 control-label">Images</label>
                        <div class="col-sm-10">
                            <input id="input-b1" name="images[]" type="file" class="file" multiple>
                             @if($errors->has('images'))
                                <label id="label-error" class="error" for="label">{{$errors->first('images')}}</label>
                            @endif
                        </div>
                    </div>
        
                    <!-- <div class="form-group @if($errors->has('supplier')) has-error @endif">
                        <label class="col-sm-2 control-label">Delivery Type</label>
                        <div class="col-sm-1">
                            <div class="checkbox">
                                <label><input type="checkbox" name="delivery_type[]" id="fast" value="1"> Fast</label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="checkbox">
                                <label><input type="checkbox" name="delivery_type[]" id="direct" value="2" > Direct</label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <div class="checkbox">
                                <label><input type="checkbox" name="delivery_type[]" id="pickup" value="3" > Pickup</label>
                            </div>
                        </div>
                    </div> -->
                <!-- <div class="form-group pickup_area hide">
                    <div class="col-sm-2 col-sm-offset-2">
                        <input type="radio" name="island_delievry_status" id="island" value="0"> Island-wide
                    </div>
                    <div class="col-sm-2">
                        <input type="radio" name="island_delievry_status" id="inclusive" value="2"> Inclusive
                    </div>
                    <div class="col-sm-4 inclusive-city-area hide">
                        <select id="inclusive_city" class="form-control input-sm" name="inclusive_city[]" multiple="multiple">
                            <option value="1">Colombo</option>
                            <option value="2">Gampaha</option>
                            <option value="3">Mahargama</option>
                        </select>
                    </div>
                </div> -->
                <div class="form-group @if($errors->has('supplier')) has-error @endif">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <textarea id="summernote" name="description">{{Input::old('description')}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Specification</label>
                    <label class="col-sm-4 text-center">Title</label>
                    <label class="col-sm-4 text-center">Value</label>
                </div>

                <div class="specification_list">
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <input type="text" class="form-control input-sm specification" name="specification[]" placeholder="Specification">
                            <label id="label-error specification_empty" class="error specification_empty hide" for="label">Please Enter Specification</label>
                        </div>
                        <div class="col-sm-4">
                            <input type="text" class="form-control input-sm value" name="value[]" placeholder="Specification Value">
                            <label id="label-error specification_value_empty" class="error specification_value_empty hide" for="label">Please Enter Value</label>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-default btn-sm add_spec" id="add_spec"><i class="fa fa-plus"></i></button>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="box-footer with-border">
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-10">
                        <button type="submit" class="btn bg-purple btn-sm pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
@stop
@section('js')
<script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="{{asset('assets/dist/bootstrap-token/js/bootstrap-tokenfield.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    //product tag
    $('#tag').tokenfield({
      autocomplete: {
        source:'tag'
      }
    });
    $('#tag').on('tokenfield:createtoken', function (event) {
        var existingTokens = $(this).tokenfield('getTokens');
        $.each(existingTokens, function(index, token) {
            if (token.value === event.attrs.value){
               event.preventDefault();
            }
        });
    });
   //product tag

   //warrenty
   $('input[type=radio][name=warranty_available]').change(function() {
        if (this.value == '1') {
            $('.warranty_period').delay(1000).removeClass('hide');
        }else{
            $('.warranty_period').delay(1000).addClass('hide');
        }
    });
   //warrenty

   //return
   $('input[type=radio][name=return_available]').change(function() {
        if (this.value == '1') {
            $('.return_period').delay(1000).removeClass('hide');
        }else{
            $('.return_period').delay(1000).addClass('hide');
        }
    });
   //return

   //delivery
   $('#fast,#direct,#pickup,#island,#inclusive').change(function() {
        if($('#pickup,#direct,#fast').is(":checked")) {
            $('.pickup_area').delay(1000).removeClass('hide');
        }else{
            $('.pickup_area').delay(1000).addClass('hide');
        }

        if($('#island').is(":checked")) {
            $('.exclusive-city-area,.inclusive-city-area').delay(1000).addClass('hide');
        }

        if($('#inclusive').is(":checked")) {
            $('.inclusive-city-area').delay(1000).removeClass('hide');
            $('.exclusive-city-area').delay(1000).addClass('hide');
        }
    });
   //delivery
   
   //images
   $("#input-b1").fileinput({
        'showUpload': false
    });
   //images

   //description
   $('#summernote').summernote({
        height: 200
   });
   //description


   //location
    $('#exclusive_city,#inclusive_city').multiselect({
        buttonWidth: '100%',
        maxHeight: 200,
        buttonClass: 'btn btn-default',
        numberDisplayed: 1,
        enableFiltering: true,
        disableIfEmpty: true,
        includeSelectAllOption: true,
        allSelectedText: 'No More Locations'
    });
   //location

   //specification
   //add new row
   $(document).on('click','.add_spec',function() {
        if($(this).closest('.form-group').find('.specification').val() == ''){
            $(this).closest('.form-group').find('label.specification_empty').removeClass('hide');
            return false;
        }
        if($(this).closest('.form-group').find('.value').val() == ''){
            $(this).closest('.form-group').find('label.specification_value_empty').removeClass('hide');
            return false;
        }
        $(this).addClass('remove_spec').removeClass('add_spec');
        $('.remove_spec').children('i').removeClass('fa-plus').addClass('fa-times');
        $(this).closest('.form-group').find('label.specification_empty').addClass('hide');
        $(this).closest('.form-group').find('label.specification_value_empty').addClass('hide');
        var new_line = '<div class="form-group">'+
                            '<div class="col-sm-offset-2 col-sm-4">'+
                                '<input type="text" class="form-control input-sm specification" name="specification[]" placeholder="Specification">'+
                                '<label id="label-error specification_empty" class="error specification_empty hide" for="label">Please Enter Specification</label>'+
                            '</div>'+
                            '<div class="col-sm-4">'+
                                '<input type="text" class="form-control input-sm value" name="value[]" placeholder="Specification Value">'+
                                '<label id="label-error specification_value_empty" class="error specification_value_empty hide" for="label">Please Enter Value</label>'+
                            '</div>'+
                            '<div class="col-sm-2">'+
                                '<button type="button" class="btn btn-default btn-sm add_spec" id="add_spec"><i class="fa fa-plus"></i></button>'+
                            '</div>'+
                        '</div>';
        $('.specification_list').prepend(new_line);
   });
   //add new row
   //specification


   //remove row
   $(document).on('click','.remove_spec',function() {
         $(this).closest('.form-group').remove();
   });
   //remove row

});
</script>
@stop
