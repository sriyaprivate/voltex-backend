@extends('layouts.back_master') @section('title','Product List')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" href="{{asset('assets/dist/jquery-multiselect/css/multi-select.css')}}">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

<style type="text/css">
.ms-container {
    background: transparent url("{{asset('assets/dist/jquery-multiselect/img/switch3.png')}}") no-repeat 50% 50%;
    width: 100%;
}
.box {
    margin-bottom: 5px !important;
}
.fa-lg{
    font-size: 1em;
}
#product_name{
    height: 34px !important;
}
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h4>Product
	<small> Management</small>
	</h4>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Product List</li>
	</ol>
</section>
{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
	<div class="box">
	    <div class="box-body">
            <form role="form" method="get" action="{{url('product/search')}}" autocomplete="off">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <input type="text" name="product_name" class="form-control input-sm" placeholder="Search Product Name & Code" value="{{$product_name}}" id="product_name">
                        </div>
                        <div class="col-sm-3">
                            <select name="category_type" id="category_type" class="form-control input-sm chosen">
                                <option value="0">All Category Types</option>
                                @foreach($category_type_list as $key => $value)
                                    <option value="{{$key}}" @if($key == $category_type) selected @endif>{{$value}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-sm-3">
                            <select name="category" id="category" class="form-control input-sm chosen">
                                <option value="0">All Categories</option>
                                @foreach($categories as $key => $value)
                                    <option value="{{$key}}" @if($key == $category) selected @endif>{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        
                        <div class="col-sm-3">
                            <select name="status" class="form-control chosen">
                                <option value="0">All Product Status</option>
                                <option value="1" @if(1==$status) selected @endif>Active Product</option>
                                <option value="2" @if(2==$status) selected @endif>Deactive Product</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-md-offset-9">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-default btn-sm" id="plan">
                                <i class="fa fa-search" style="padding-right: 16px;width: 28px;"></i>Search
                            </button>
                            <a href="list" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh" style="padding-right: 16px;width: 20px;"></i>Refresh</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col refresh">
        <div class="box box-widget box-list box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Product List</h3>
                <div class="box-title pull-right">
                <a href="{{url('product/add')}}" class="btn bg-purple btn-sm pull-right" style="margin-top: 2px;">New Product</a>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-bordered bordered table-striped table-condensed" id="orderTable" >
                    <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th width="10%">Country</th>
                        <th width="10%">Product Code</th>
                        <th width="15%">Product Name</th>
                        <th width="12%" class="text-center">Category Type</th>
                        <th width="10%" class="text-right">Selling Price(Rs.)</th>
                        <th width="10%" class="text-right">MOQ</th>
                        <th width="5%" class="text-center">Status</th>
                        <th width="10%" class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                       $i = ($product_list->currentpage()-1)* $product_list->perpage() + 1;
                      ?>
                      @if(count($product_list) > 0)
                          @foreach($product_list as $value)
                              @include('Product::template.product')
                          <?php $i++;?>
                          @endforeach
                      @else
                          <tr><td colspan="10" class="text-center">No products found.</td></tr>
                      @endif
                      </tbody>
                    </table>
                    Showing {{$product_list->firstItem()}} to {{$product_list->lastItem()}} of {{$product_list->total()}} products
                    @if($product_list != null)
                        <div style="float: right;">{!! $product_list->appends($_GET)->render() !!}</div>
                    @endif
            </div>
        </div>
    </div>
</section>

@stop
@section('js')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">

    $(document).ready(function(){

        $('.modal').on("hidden.bs.modal", function () {
            $('#other-measurement').html('');
            $('#_mc').val(1);
        });

        $('#category_type').on('change',function(){

            if($('#category_type').val() > 0){

                $('.content').addClass('panel-refresh');
                $.ajax({
                    url: "{{URL::to('product/get-category-hierarchy')}}",
                    method: 'GET',
                    data: {
                        'category_type_id':$(this).val(),
                        'type':1
                    },
                    async: false,
                    success: function (data) {
                        $('.content').removeClass('panel-refreshing');
                        $('#category').html('');
                        $.each(data,function(key,value){
                            // console.log(value.id)
                            $('#category').append(
                                '<option value="'+value.id+'">'+value.name+'</option>'
                            );                        
                        });
                        $('#category').trigger("chosen:updated");                
                    },error: function () {
                        $.alert({
                            theme: 'material',
                            title: 'Error Occured',
                            type: 'red',
                            content: 'Error Occured when getting filters'
                        }); 
                    }
                });
            }else{
                $('#category').html('');
                $('#category').append('<option value="">All Category</option>');
                $('#category').trigger("chosen:updated");
            }
        });
    });

    function deleteProduct(product_id){
      $.confirm({
          title: 'Delete',
          content:'Do you want to delete this product ?',
          type:'red',
          buttons: {
              confirm: {
                  btnClass: 'btn-sm bg-default',
                  text: 'Yes',
                  keys: ['enter'],
                  action: function () {
                      $(".content").addClass('panel-refreshing');
                      $.ajax({
                        url: "{{URL::to('product/delete')}}",
                        method: 'GET',
                        data: {
                          'product':product_id
                        },
                        async: false,
                        success: function (data) {
                          if(data.data == 0){
                            $(".content").removeClass('panel-refreshing');
                            error_alert('Notice','Sorry! You cannot remove.<br>This product already added to cart by customer.',3)
                          }else{
                            $(".content").removeClass('panel-refreshing');
                            $.confirm({
                              title  :'Success',
                              content:'Product deleted successfully',
                              type   :'green',
                              autoClose:'cancelAction|500',
                              buttons: {
                                ok: {
                                    keys: [
                                        'enter'
                                    ],
                                    action: function () {
                                        location.reload();
                                    }
                                }
                                },
                            });
                          }
                        },error: function (data) {
                          $(".content").removeClass('panel-refreshing');
                           $.alert({
                              theme: 'material',
                              title: 'Error Occured',
                              type: 'red',
                              content: 'Error Occured when deleting product'
                          });
                        }
                      });
                  }
              },
              cancelAction: {
                  text: 'No',
                  action: function () {
                      $(".content").removeClass('panel-refreshing');
                  }
              }
          }
      });
    }
    
</script>
@stop
