@extends('layouts.back_master') @section('title','Upload Products')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/bootstrap-tokenfield.css')}}">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
<style type="text/css">
    .title{
        text-align: left !important;
    }
    .input-group-addon.error{
        border-color:#dd4b39;
    }
    .control-label.error{
        color:#dd4b39;
        font-weight:700 !important;
    }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Product
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('product/list')}}}">Product List</a></li>
		<li class="active">Upload</li>
	</ol>
</section>
{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
    	<div class="box">
    		<div class="box-header with-border">
    			<h3 class="box-title">Upload Products</h3>
    		</div>
    	    <div class="box-body">
                {!!Form::token()!!}
                <div class="form-group">
                    <label class="col-sm-2 control-label @if($errors->has('country')) has-error @endif">Country <span class="require">*</span></label>
                    <div class="col-sm-10 @if($errors->has('country')) has-error @endif">
                        <select id="country" class="form-control input-sm chosen @if($errors->has('country')) has-error @endif" name="country">
                            <option value="">Select Country</option>
                            @foreach($countries as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('country'))
                            <label id="label-error" class="error" for="label">The Country field is required</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label @if($errors->has('main_category')) has-error @endif">Main Category <span class="require">*</span></label>
                    <div class="col-sm-10 @if($errors->has('main_category')) has-error @endif">
                        <select id="main_category" class="form-control input-sm chosen" name="main_category">
                            <option value="">Select Main Category</option>
                            @foreach($main_category as $key => $value)
                                <option value="{{$key}}" @if(Input::old('main_category') == $key) selected @endif>{{$value}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('main_category'))
                            <label id="label-error" class="error" for="label">The Main category field is required when other category is not selected</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">     
                    <label class="col-sm-2 control-label  @if($errors->has('sub_category')) has-error @endif">Sub Category <span class="require">*</span></label>
                    <div class="col-sm-10  @if($errors->has('sub_category')) has-error @endif">
                        <select id="sub_category" class="form-control input-sm chosen" name="sub_category">
                            <option value="">Select Sub Category</option>
                        </select>
                        @if($errors->has('sub_category'))
                            <label id="label-error" class="error" for="label">The sub category field is required when main category is not selected</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('data_file')) has-error @endif">
                    <label class="col-sm-2 control-label">File <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input id="data_file" name="data_file" type="file" class="file">
                         @if($errors->has('data_file'))
                            <label id="label-error" class="error" for="label">{{$errors->first('data_file')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <a href="{{asset('assets/excel_formats/product_upload.xls')}}" class="btn btn-warning">
                            <i class="fa fa-download"></i> Sample Excel
                        </a>
                    </div>
                </div>        
            </div>
            <div class="box-footer with-border">
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-10">
                        <button type="submit" class="btn bg-purple btn-sm pull-right save"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                </div>
            </div>

            @if(isset($data))
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                            <tr>
                                <td width="20%">{{ $row['code'] }}</td>
                                <td>{{ $row['name'] }}</td>
                                <td><span class="{{($row['status'])? 'text-success':'text-danger'}}">{{ $row['message'] }}</span></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif

        </div>
    </form>
</section>
@stop
@section('js')
<script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="{{asset('assets/dist/bootstrap-token/js/bootstrap-tokenfield.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    
    /** ======= save click ==============*/
    $('.save').on('click',function(){
        setTimeout(function(){  $('.content').addClass('panel-refreshing'); $('.save').attr('disabled',true); }, 100);
    });

    //get Sub category when main category change
    $('#main_category').on('change',function(){

        if($('#main_category').val() > 0){

            $('.content').addClass('panel-refresh');
            $.ajax({
                url: "{{URL::to('product/get-category-hierarchy')}}",
                method: 'GET',
                data: {
                    'category_type_id':$(this).val(),
                    'type':1
                },
                async: false,
                success: function (data) {
                    $('.content').removeClass('panel-refreshing');
                    $('#sub_category').html('');
                    $.each(data,function(key,value){
                        // console.log(value.id)
                        $('#sub_category').append(
                            '<option value="'+value.id+'">'+value.name+'</option>'
                        );                        
                    });
                    $('#sub_category').trigger("chosen:updated");                
                },error: function () {
                    $.alert({
                        theme: 'material',
                        title: 'Error Occured',
                        type: 'red',
                        content: 'Error Occured when getting filters'
                    }); 
                }
            });
        }else{           
            $('#sub_category').html('');
            $('#sub_category').append('<option value="">Select Sub Category</option>');
            $('#sub_category').trigger("chosen:updated");
        }
    });
});

$("#data_file").fileinput({
    showUpload: false
});

</script>
@stop
