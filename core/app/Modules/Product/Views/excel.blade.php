@extends('layouts.back_master') @section('title','Upload Products')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/bootstrap-tokenfield.css')}}">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
<style type="text/css">
    .title{
        text-align: left !important;
    }
    .input-group-addon.error{
        border-color:#dd4b39;
    }
    .control-label.error{
        color:#dd4b39;
        font-weight:700 !important;
    }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Product
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('product/list')}}}">Product List</a></li>
		<li class="active">Upload</li>
	</ol>
</section>
{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
    	<div class="box">
    		<div class="box-header with-border">
    			<h3 class="box-title">Upload Products</h3>
    		</div>
    	    <div class="box-body">
                {!!Form::token()!!}
                <div class="form-group @if($errors->has('data_file')) has-error @endif">
                    <label class="col-sm-2 control-label">File <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input id="data_file" name="data_file" type="file" class="file">
                         @if($errors->has('data_file'))
                            <label id="label-error" class="error" for="label">{{$errors->first('data_file')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <a href="{{asset('assets/excel_formats/product_upload.xls')}}" class="btn btn-warning">
                            <i class="fa fa-download"></i> Sample Excel
                        </a>
                    </div>
                </div>        
            </div>
            <div class="box-footer with-border">
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-10">
                        <button type="submit" class="btn bg-purple btn-sm pull-right save"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                </div>
            </div>

        </div>
    </form>
</section>
@stop
@section('js')
<script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="{{asset('assets/dist/bootstrap-token/js/bootstrap-tokenfield.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    
    /** ======= save click ==============*/
    $('.save').on('click',function(){
        setTimeout(function(){  $('.content').addClass('panel-refreshing'); $('.save').attr('disabled',true); }, 100);
    });

});

$("#data_file").fileinput({
    showUpload: false
});

</script>
@stop
