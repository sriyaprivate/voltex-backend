@extends('layouts.back_master') @section('title','Add Product')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/bootstrap-tokenfield.css')}}">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
<style type="text/css">
    .title{
        text-align: left !important;
    }
    .input-group-addon.error{
        border-color:#dd4b39;
    }
    .control-label.error{
        color:#dd4b39;
        font-weight:700 !important;
    }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Product
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('product/list')}}}">Product List</a></li>
		<li class="active">Bulk Upload</li>
	</ol>
</section>
{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
    <form role="form" class="form-horizontal form-validation" method="post" autocomplete="off" enctype="multipart/form-data">
    	<div class="box">
    		<div class="box-header with-border">
    			<h3 class="box-title">Bulk Upload</h3>
    		</div>
    	    <div class="box-body">
                {!!Form::token()!!}
                <div class="form-group @if($errors->has('upload_type')) has-error @endif">
                    <label class="col-sm-2 control-label">Upload Type <span class="require">*</span></label>
                    <div class="col-sm-2">
                        <input type="radio" name="upload_type" checked value="1"> Bulk Image
                    </div>
                    <div class="col-sm-2">
                        <input type="radio" name="upload_type" value="2"> Bulk Datasheet
                    </div>
                    @if($errors->has('upload_type'))
                        <label id="label-error" class="error" for="label">{{$errors->first('upload_type')}}</label>
                    @endif
                </div>
                <div class="form-group @if($errors->has('file')) has-error @endif">
                    <label class="col-sm-2 control-label">Zip Archive <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input name="file" accept="application/zip, application/octet-stream, application/x-zip-compressed, multipart/x-zip" type="file" class="file">
                         @if($errors->has('file'))
                            <label id="label-error" class="error" for="label">{{$errors->first('file')}}</label>
                        @endif
                    </div>
                </div>
            </div>
            <div class="box-footer with-border">
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-10">
                        <button type="submit" class="btn bg-purple btn-sm pull-right save"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
@stop
@section('js')
<script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="{{asset('assets/dist/bootstrap-token/js/bootstrap-tokenfield.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    
    /** ======= save click ==============*/
    $('.save').on('click',function(){
        setTimeout(function(){  $('.content').addClass('panel-refreshing'); $('.save').attr('disabled',true); }, 100);
    });

    //get Sub category when main category change
    $('#main_category').on('change',function(){

        if($('#main_category').val() > 0){

            $('.content').addClass('panel-refresh');
            $.ajax({
                url: "{{URL::to('product/get-sub-category')}}",
                method: 'GET',
                data: {
                    'category_id':$(this).val(),
                    'type':1
                },
                async: false,
                success: function (data) {
                    $('.content').removeClass('panel-refreshing');
                    $('#sub_category').html('');
                    $('#sub_category').append(data);
                    $('#sub_category').trigger("chosen:updated");                
                },error: function () {
                    $.alert({
                        theme: 'material',
                        title: 'Error Occured',
                        type: 'red',
                        content: 'Error Occured when getting filters'
                    }); 
                }
            });
        }else{           
            $('#sub_category').html('');
            $('#sub_category').append('<option value="">Select Sub Category</option>');
            $('#sub_category').trigger("chosen:updated");
        }
    });
});

$("#product-image").fileinput({
    showUpload: false
});

</script>
@stop
