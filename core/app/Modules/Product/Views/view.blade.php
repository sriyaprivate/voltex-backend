@extends('layouts.back_master') @section('title','View Product')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/bootstrap-tokenfield.css')}}">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Product
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('product/list')}}}">Product List</a></li>
		<li class="active">View</li>
	</ol>
</section>
{{--<!-- Main content -->--}}
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header ">
            <h3 class="box-title"><strong>Product : {{ $details->name }}</strong></h3>
            <div class="box-title pull-right">
                <a href="{{url('product/edit')}}/{{$details->id}}" class="btn bg-purple btn-sm pull-right" style="margin-top: 2px;"><i class="fa fa-pencil"></i> Edit</a>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <td width="25%">Prodict ID</td>
                                    <td><strong>{{ $details->id?:'-' }}</strong></td>
                                </tr>
                                <tr>
                                    <td>Product Code</td>
                                    <td><strong>{{ $details->code?:'-' }}</strong></td>
                                </tr>
                                <tr>
                                    <td>Minimum Order Quantity</td>
                                    <td><strong>{{ $details->moq?:'Not yet set' }}</strong></td>
                                </tr>
                                <tr>
                                    <td>Product Display Name</td>
                                    <td><strong>{{ $details->display_name?:'-' }}</strong></td>
                                </tr>
                                <tr>
                                    <td>Product Category | Type</td>
                                    <td>
                                        @if(count($details->product_category) > 0)
                                            @foreach($details->product_category as $key => $value)
                                            <strong>{{ !empty($value->category->name)?$value->category->name:'-'}} | {{ !empty($value->category->type->name)?$value->category->type->name:'-' }}
                                            </strong>
                                            @endforeach
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>
                                        @if($value->status == 1)
                                            <span class="fa fa-check-circle" style="color: rgba(22, 160, 133,1.0);"></span>
                                        @else
                                            <span class="fa fa-check-circle" style="color: rgba(127, 140, 141,1.0);"></span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Selling Price</td>
                                    <td>
                                        <strong>
                                            @if(!empty($details->selling_price)) 
                                                $ {{ number_format($details->selling_price,2) }} 
                                            @else 
                                                - 
                                            @endif
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Price Term</td>
                                    <td>
                                        <strong>
                                            @if(!empty($details->price_term)) 
                                                {{ $details->price_term }} 
                                            @else 
                                                - 
                                            @endif
                                        </strong>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>Taxation</td>
                                    @if($details->tax_type_id == 1)
                                        <td><strong>NO TAX</strong></td>
                                    @elseif($details->tax_type_id == 2)    
                                        <td><strong>VAT</strong></td>
                                    @else
                                        <td><strong>NBT+VAT</strong></td>
                                    @endif
                                </tr>

                                <tr>
                                    <td>Description</td>
                                    <td><strong>{{str_replace("</p>",'',str_replace("<p>",'',$details->description))}}</strong></td>                                    
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                </div><!-- col-md-8 -->
                <div class="col-sm-6 col-md-6">
                    
                    <!-- Product Delivery Type -->
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <td colspan="2"><strong>Product Images</strong></td>
                            </tr>
                            <tr>
                                <td>
                                @if(count($details) > 0 && count($details->productImage) > 0)
                                    @foreach($details->productImage as $key => $value)  
                                        <div class="col-xs-12 col-md-3 thumbnail">
                                          <img src="{{url(UPLOAD_PATH.'uploads/images/product/'.$value->image)}}" alt="...">
                                      </div>
                                    @endforeach
                                @else
                                    <div class="col-xs-12 col-md-3 thumbnail">
                                       <img src="{{url(UPLOAD_PATH.DEFAULT_IMAGE)}}" alt="...">
                                    </div>
                                @endif                            
                                </td>
                            </tr>
                        </table>
                    </div>
                </div><!-- col-md-4 -->
            </div><!-- row -->  

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                @foreach($spec_keys as $val)
                                    <td>{{$val}}</td>
                                @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @foreach($spec_keys as $val)
                                        <td>{{$specs->$val}}</td>
                                    @endforeach 
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>  
            </div>      
            
        </div>
    </div>  
</section>
@stop
@section('js')
<script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="{{asset('assets/dist/bootstrap-token/js/bootstrap-tokenfield.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
   //description\
   $('#summernote').summernote({height:300})
   $('#summernote').summernote('disable');
});
</script>
@stop
