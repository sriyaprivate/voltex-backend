@if($value->deleted_at == Null)
<tr data-id="{{$value->id}}">
     <td data-id="{{$value->id}}">{{$i}}</td>
     <td><img src="{{url(UPLOAD_PATH.'uploads/images/flags/'.$value->country['flag'])}}" style="width: 30px" title="{{$value->country['name']}}"></td>
     <td>{{empty($value->code)?'-':$value->code}}</td>
     <td id="item-name-{{$value->id}}" name="item-name-{{$value->id}}">{{empty($value->name)?'-':$value->name}}</td>
     <?php $cat = ''; ?>
     @if($value->status == DEFAULT_STATUS)
         @if(count($value->product_category) > 0)
            @foreach($value->product_category as $key => $category)
                @if(count($category) > 0 && count($category->category) > 0 && count($category->category->type) > 0)
                <?php $cat .= $category->category->type->name ?>
                @else
                <?php $cat .= '' ?>
                @endif  
            @endforeach
            <td class="text-primary">{{$cat}}</td>
         @else
            <td> - </td>
         @endif
    @elseif($value->status == SAVINGS_CENTER_STATUS)
        <td class="text-success">Saving Center</td>
    @else
        <td class="text-success"> - </td>
    @endif
      <?php $total = 0; ?>
     @if(count($value->product_category) > 0) 
         @foreach($value->stock as $category)
            <?php $total = $total + $category->qty ?>  
         @endforeach
     @endif
    
    <td class="text-right">{{empty($value->selling_price)?' - ':number_format($value->selling_price,2)}}</td>
    <td class="text-right">{{empty($value->moq)?' - ':$value->moq}}</td>
    <td class="text-center">
        @if($value->status == 1 || $value->status == 2)
            <span class="fa fa-check-circle" style="color: rgba(22, 160, 133,1.0);"></span>
        @else
            <span class="fa fa-check-circle" style="color: rgba(127, 140, 141,1.0);"></span>
        @endif
    </td>
    
    <td class="text-center">
        <div class="btn-group">
            <a href="{{'view/'.$value->id}}" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye view"></i></a>
            <a href="{{'edit/'.$value->id}}" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></a>
        </div>
    </td>
</tr>
@endif