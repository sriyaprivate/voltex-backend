@extends('layouts.back_master') @section('title','Add Product')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/bootstrap-tokenfield.css')}}">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
<style type="text/css">
    .title{
        text-align: left !important;
    }
    .input-group-addon.error{
        border-color:#dd4b39;
    }
    .control-label.error{
        color:#dd4b39;
        font-weight:700 !important;
    }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Product
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('product/list')}}}">Product List</a></li>
		<li class="active">Add</li>
	</ol>
</section>
{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
    <form role="form" class="form-horizontal form-validation" method="post" autocomplete="off" enctype="multipart/form-data">
    	<div class="box">
    		<div class="box-header with-border">
    			<h3 class="box-title">Add Product</h3>
    		</div>
    	    <div class="box-body">
                {!!Form::token()!!}
                <div class="form-group">
                    <label class="col-sm-2 control-label @if($errors->has('country')) has-error @endif">Country <span class="require">*</span></label>
                    <div class="col-sm-4 @if($errors->has('country')) has-error @endif">
                        <select id="country" class="form-control input-sm chosen @if($errors->has('country')) has-error @endif" name="country">
                            <option value="">Select Country</option>
                            @foreach($countries as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('country'))
                            <label id="label-error" class="error" for="label">The Country field is required</label>
                        @endif
                    </div>

                    <label class="col-sm-2 control-label @if($errors->has('item_code')) has-error @endif">Item Code <span class="require">*</span></label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control input-sm" name="item_code" placeholder="Item Code" value="{{Input::old('item_code')}}">
                        @if($errors->has('item_code'))
                            <label id="label-error" class="error" for="label">{{$errors->first('item_code')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('item_name')) has-error @endif">
                    <label class="col-sm-2 control-label">Item Name <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="item_name" placeholder="Product Name" value="{{Input::old('item_name')}}">
                        @if($errors->has('item_name'))
                            <label id="label-error" class="error" for="label">{{$errors->first('item_name')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('display_name')) has-error @endif">
                    <label class="col-sm-2 control-label">Display Name </label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control input-sm" name="display_name" placeholder="Product Display Name" value="{{Input::old('display_name')}}">
                        @if($errors->has('display_name'))
                            <label id="label-error" class="error" for="label">{{$errors->first('display_name')}}</label>
                        @endif
                    </div>
                    <label class="col-sm-2 control-label @if($errors->has('selling_price')) error @endif">Selling Price <span class="require">*</span></label>
                    <div class="col-sm-4 @if($errors->has('selling_price')) has-error @endif">
                        <div class="input-group">
                          <div class="input-group-addon @if($errors->has('selling_price')) error @endif">Rs.</div>
                            <input type="text" class="form-control" id="selling_price" placeholder="Selling Price" name="selling_price" value="{{Input::old('selling_price')}}" onkeydown="acceptDecimal(this);" onkeyup="acceptDecimal(this);" onchange="acceptDecimal(this);">
                        </div>
                        @if($errors->has('selling_price'))
                            <label id="label-error" class="error" for="label">{{$errors->first('selling_price')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label @if($errors->has('main_category')) has-error @endif">Main Category <span class="require">*</span></label>
                    <div class="col-sm-4 @if($errors->has('main_category')) has-error @endif">
                        <select id="main_category" class="form-control input-sm chosen" name="main_category">
                            <option value="">Select Main Category</option>
                            @foreach($main_category as $key => $value)
                                <option value="{{$key}}" @if(Input::old('main_category') == $key) selected @endif>{{$value}}</option>
                            @endforeach
                        </select>
                        @if($errors->has('main_category'))
                            <label id="label-error" class="error" for="label">The Main category field is required when other category is not selected</label>
                        @endif
                    </div>

                    <label class="col-sm-2 control-label  @if($errors->has('sub_category')) has-error @endif">Sub Category <span class="require">*</span></label>
                    <div class="col-sm-4  @if($errors->has('sub_category')) has-error @endif">
                        <select id="sub_category" class="form-control input-sm chosen" name="sub_category">
                            <option value="">Select Sub Category</option>
                        </select>
                        @if($errors->has('sub_category'))
                            <label id="label-error" class="error" for="label">The sub category field is required when main category is not selected</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    
                </div>
                <div class="form-group @if($errors->has('tax_type')) has-error @endif">
                    <label class="col-sm-2 control-label">Taxation <span class="require">*</span></label>
                    @if(count($taxType) > 0)
                        @foreach($taxType as $type_id => $type)
                            <div class="col-sm-2">
                               <input type="radio" name="tax_type" id="{{$type}}" value="{{$type_id}}" @if(unserialize(TAX_TYPE)['NO_VAT'] == $type_id) checked @endif> {{$type}} 
                            </div>
                        @endforeach
                         @if($errors->has('tax_type'))
                            <label id="label-error" class="error" for="label">{{$errors->first('tax_type')}}</label>
                        @endif
                    @endif
                </div>

                <div class="form-group @if($errors->has('images')) has-error @endif">
                    <label class="col-sm-2 control-label">Images</label>
                    <div class="col-sm-10">
                        <input id="product-image" name="images[]" type="file" class="file" multiple>
                         @if($errors->has('images'))
                            <label id="label-error" class="error" for="label">{{$errors->first('images')}}</label>
                        @endif
                         <label id="label-error" class="error pull-right" for="label">Image Size (400px * 400px)</label>
                    </div>
                </div>
                
                <div class="form-group @if($errors->has('description')) has-error @endif">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <textarea id="summernote" name="description">{{Input::old('description')}}</textarea>
                    </div>
                </div>                
            </div>
            <div class="box-footer with-border">
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-10">
                        <button type="submit" class="btn bg-purple btn-sm pull-right save"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
@stop
@section('js')
<script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="{{asset('assets/dist/bootstrap-token/js/bootstrap-tokenfield.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){
    
    /** ======= save click ==============*/
    $('.save').on('click',function(){
        setTimeout(function(){  $('.content').addClass('panel-refreshing'); $('.save').attr('disabled',true); }, 100);
    });

    //get Sub category when main category change
    $('#main_category').on('change',function(){

        if($('#main_category').val() > 0){

            $('.content').addClass('panel-refresh');
            $.ajax({
                url: "{{URL::to('product/get-sub-category')}}",
                method: 'GET',
                data: {
                    'category_id':$(this).val(),
                    'type':1
                },
                async: false,
                success: function (data) {
                    $('.content').removeClass('panel-refreshing');
                    $('#sub_category').html('');
                    $('#sub_category').append(data);
                    $('#sub_category').trigger("chosen:updated");                
                },error: function () {
                    $.alert({
                        theme: 'material',
                        title: 'Error Occured',
                        type: 'red',
                        content: 'Error Occured when getting filters'
                    }); 
                }
            });
        }else{           
            $('#sub_category').html('');
            $('#sub_category').append('<option value="">Select Sub Category</option>');
            $('#sub_category').trigger("chosen:updated");
        }
    });
});

$("#product-image").fileinput({
    showUpload: false
});

</script>
@stop
