 @extends('layouts.back_master') @section('title','Edit Product')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/bootstrap-tokenfield.css')}}">
<link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Product
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li><a href="{{{url('product/list')}}}">Product List</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
{{--<!-- Main content -->--}}
<section class="content">
    {{--<!-- Default box -->--}}
    <form role="form" class="form-horizontal form-validation" method="post" autocomplete="off" enctype="multipart/form-data">
        <div class="box">
            <!-- <div class="box"> -->
            <div class="box-header with-border">
                <h3 class="box-title">Edit Product</h3>
            </div>
            <div class="box-body">
                {!!Form::token()!!}
                <div class="form-group @if($errors->has('item_code')) has-error @endif">
                    <label class="col-sm-2 control-label">Item Code <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="item_code" placeholder="Item Code" value="{{$product_detail->code}}" readonly>
                        @if($errors->has('item_code'))
                            <label id="label-error" class="error" for="label">{{$errors->first('item_code')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('item_name')) has-error @endif">
                    <label class="col-sm-2 control-label">Item Name <span class="require">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control input-sm" name="item_name" placeholder="Product Name" value="{{$product_detail->name}}">
                        @if($errors->has('item_name'))
                            <label id="label-error" class="error" for="label">{{$errors->first('item_name')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('display_name')) has-error @endif">
                    <label class="col-sm-2 control-label">Display Name </label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control input-sm" name="display_name" placeholder="Product Display Name" value="{{$product_detail->display_name}}">
                        @if($errors->has('display_name'))
                            <label id="label-error" class="error" for="label">{{$errors->first('display_name')}}</label>
                        @endif
                    </div>

                    <label class="col-sm-2 control-label @if($errors->has('selling_price')) error @endif">Selling Price <span class="require">*</span></label>
                    <div class="col-sm-4 @if($errors->has('selling_price')) has-error @endif">
                        <div class="input-group">
                          <div class="input-group-addon @if($errors->has('selling_price')) error @endif">Rs.</div>
                            <input type="text" class="form-control" id="selling_price" placeholder="Selling Price" name="selling_price" value="{{$product_detail->selling_price}}" onkeydown="acceptDecimal(this);" onkeyup="acceptDecimal(this);" onchange="acceptDecimal(this);">
                        </div>
                        @if($errors->has('selling_price'))
                            <label id="label-error" class="error" for="label">{{$errors->first('selling_price')}}</label>
                        @endif
                    </div>
                </div>
                
                <div class="form-group">
                    
                </div>
                
                
                <div class="form-group @if($errors->has('tax_type')) has-error @endif">
                    <label class="col-sm-2 control-label">Taxation <span class="require">*</span></label>
                    @if(count($taxType) > 0)
                        @foreach($taxType as $type_id => $type)
                            <div class="col-sm-2">
                               <input type="radio" name="tax_type" id="{{$type}}" value="{{$type_id}}" @if($product_detail->tax_type_id == $type_id) checked @endif> {{$type}} 
                            </div>
                        @endforeach
                         @if($errors->has('tax_type'))
                            <label id="label-error" class="error" for="label">{{$errors->first('tax_type')}}</label>
                        @endif
                    @endif
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">Uploaded Images</label>
                    <div class="col-sm-10">
                     @if(count($product_detail) > 0 && count($product_detail->productImage) > 0)
                        @foreach($product_detail->productImage as $key => $value) 
                                <div class="col-xs-12 col-md-3 thumbnail" style="height: 240px;">
                                    <img src="{{url(UPLOAD_PATH.'uploads/images/product/'.$value->image)}}" alt="..." style="height: 100%;">
                                    <center>
                                        <div style="position: absolute;bottom: 5px;">
                                            <a href="javascript:void(0);" data-id="{{$value->id}}" id="{{$value->id}}" class="btn btn-xs remove-image text-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="View More" style="border: 1px solid #00000024;margin-top:3px; "><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </center>
                                </div>
                        @endforeach
                    @else
                        <div class="col-xs-12 col-md-3 thumbnail">
                           <img src="{{url(UPLOAD_PATH.DEFAULT_IMAGE)}}" alt="...">
                        </div>
                    @endif
                    <input type="hidden" name="delete_images" id="delete_image">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Images</label>
                    <div class="col-sm-10">
                        <input id="input-b1" name="images[]" type="file" class="file" multiple>
                         @if($errors->has('images'))
                            <label id="label-error" class="error" for="label">{{$errors->first('images')}}</label>
                        @endif
                    </div>
                </div>
                
                <div class="form-group @if($errors->has('supplier')) has-error @endif">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <textarea id="summernote" name="description">{{$product_detail->description}}</textarea>
                    </div>
                </div>
                
            <div class="box-footer with-border">
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-10">
                        <button type="submit" class="btn bg-purple btn-sm pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
@stop
@section('js')
<script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" src="{{asset('assets/dist/bootstrap-token/js/bootstrap-tokenfield.js')}}"></script>
<script type="text/javascript">
$(document).ready(function(){

    //description
    $('#summernote').summernote({
        height: 200
    });
    //description
    
    //get Sub category when main category change
    $('#main_category').on('change',function(){

        if($('#main_category').val() > 0){

            $('.content').addClass('panel-refresh');
            $.ajax({
              url: "{{URL::to('product/get-sub-category')}}",
              method: 'GET',
              data: {
                'category_id':$(this).val(),
                'type':1
              },
              async: false,
              success: function (data) {
                $('.content').removeClass('panel-refreshing');
                    $('#sub_category').html('');
                    $('#sub_category').append(data);
                    $('#sub_category').trigger("chosen:updated");                
              },error: function () {
                $.alert({
                    theme: 'material',
                    title: 'Error Occured',
                    type: 'red',
                    content: 'Error Occured when getting filters'
                }); 
              }
            }); 
        }else{           
            $('#sub_category').html('');
            $('#sub_category').append('<option value="">Select Sub Category</option>');
            $('#sub_category').trigger("chosen:updated");
        }
    });

   //delete images
   var remove_img = [];
   $('.remove-image').on('click',function(){
        var img_id = $(this).data('id');
        $.confirm({
            theme: 'material',
            title: 'Image Delete',
            content: 'Do you want to delete this image ?',
            buttons: {
                confirm: {
                    btnClass: 'btn-red',
                    keys: ['enter'],
                    action: function(){
                        remove_img.push(img_id);
                        $('#delete_image').val(remove_img);
                        $('#'+img_id).parent().parent().parent().remove();
                    }
                },
                cancel: function () {

                }
            }
        });
   });
   //delete image
});
$("#input-b1").fileinput({
    showUpload: false
});
</script>
@stop
