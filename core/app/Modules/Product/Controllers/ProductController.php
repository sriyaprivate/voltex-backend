<?php namespace App\Modules\Product\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Intervention\Image\Facades\Image;
use Exception;

use App\Modules\Product\BusinessLogics\ProductLogic;
use App\Classes\Functions;
use App\Modules\Category\CategoryRepository\CategoryRepository;
use Illuminate\Support\Collection;
use App\Modules\Product\Models\Image as ProductImage;
use App\Modules\Product\Models\Datasheet;
use Zipper;



class ProductController extends Controller {


	protected $product;
	protected $category;
	protected $common;

    public function __construct(ProductLogic $product,Functions $function,CategoryRepository $category_repository,Functions $common){
    
        $this->product   = $product;
        $this->funcation = $function;
        $this->category  = $category_repository;
        $this->common    = $common;
    }


	/**
	 * Display a listing of the product.
	 *
	 * @return Response
	 */
	public function index()
	{
		$product_list  	= $this->product->getProductList();
		$category_types = $this->category->getAllCategoryType();
		$categories 	= $this->category->getAllCategories();

		return view('Product::list')->with([
			'category_type_list'=> $category_types,
			'categories'		=> $categories,
			'product_list' 		=> $product_list,
			'product_code' 		=> '',
			'product_name' 		=> '',
			'category_type' 	=> '',
			'category' 			=> '',
			'status'       		=> '',
			'home_section'	    => ''
		]);
	}

	/**
	 * Search Product in the List.
	 *
	 * @return Response
	 */
	public function search(Request $request)
	{
		try{

			$category_types = $this->category->getAllCategoryType();
			$categories 	= $this->category->getAllCategoryByType($request->get('category_type'));
			
			$product_list 	= $this->product->searchProduct(
				$request->get('product_name'),
				$request->get('category_type'),
				$request->get('category'),
				$request->get('status')
			);

			return view('Product::list')->with([
				'category_type_list'=> $category_types,
				'categories'		=> $categories,
				'product_list'    	=> $product_list,
				'product_code'    	=> $request->get('product_code'),
				'product_name'    	=> $request->get('product_name'),
				'category_type' 	=> $request->get('category_type'),
				'category' 			=> $request->get('category'),
				'status'       	  	=> $request->get('status')
			]);

		}catch(Exception $e){
			return redirect('product/list')->with([
	                'error' 	   => true,
	                'error.title'  => 'Error..!',
	                'error.message'=> $e->getMessage()
	        ]); 
		}
		//return view('Product::list')->with(['product_list' => $product_list]);
	}

	/**
	 * Show the form for creating a new product.
	 *
	 * @return Response
	 */
	public function create()
	{
		$main_category 	  = $this->category->getAllCategoryType();
		$taxType	  = $this->product->getTaxType();	
		$countries 		= $this->product->getAllCountries();

		return view("Product::add")->with([
			'main_category' 	=> $main_category,	
			'countries'			=> $countries,
			'taxType'			=> $taxType		
		]);
	}

	/**
	 * This function is used to all tag
	 *
	 * @return Response
	 */
	public function tag(Request $request)
	{
		$all_tag = $this->product->searchTag($request->get('term'));
		return Response::json($all_tag);
	}

	/**
	 * This function is used to get category filters 
	 * @param 
	 * @return 
	 */

	public function category_filter(Request $request)
	{
		if($request->ajax()){
			$category_filter = $this->category->getSubCategory($request->get('category_id'));
			
			$html = '';

			if(count($category_filter) > 0){
				
				$html .= '<option value="">Select Sub Category</option>';
				foreach ($category_filter as $main_value) {
										
					$html .= '<option value="'.$main_value->id.'">'.$main_value->name.'</option>';

				}
			}else{
				$html .= '<option value="">No Sub Category to Select</option>';
			}

			return Response::json($html);
		}else{
			return Response::json([]);
		}
	}
	/**
	 * Store a newly created product in db.
	 * @param No
	 * @return Response
	 */
	public function store(Request $request)
	{	
		/** validate form request data **/
		$this->validate($request,[
			'country'				=> 'required',
			'item_name'				=> 'required',
            'item_code' 			=> 'required|unique:product,code',
            'main_category'			=> 'required_without_all:main_category',
            'sub_category'  		=> 'required_without_all:sub_category',
            'tax_type'				=> 'required',
            'selling_price'			=> 'required|numeric'
        ]);

		try{
			/** ============== product images upload =============================*/
			$this->common->makeDirNotExist('uploads/images/product',0777,true);
			$img_arr = array();
            if($request->hasFile('images')){
                $files = $request->file('images');
                $i = 0;
                foreach($files as $file){
                    $extn = $file->getClientOriginalExtension(); //get extension
                    $fileName = 'item-'.$i.date('YmdHis').'.'.$extn; //set file name
                    $destinationPath = storage_path('uploads/images/product'); // destination
                    $file->move($destinationPath, $fileName);//move to file
                    //resize image
                    // $img = Image::make($destinationPath . '/' . $fileName)->fit(400,300, function ($c) {
                    //     $c->upsize();
                    // },"top");
                    // $img->save($destinationPath . '/' . $fileName);
                    array_push($img_arr,$fileName);
                    $i++;
                }
            }
	        $this->product->addProduct($request->all(),$img_arr);
	        return redirect( 'product/add' )->with([
	            'success' 		 => true,
	            'success.title'  => 'Success..!',
	            'success.message'=> 'Product added successfully'
	        ]);
	    }catch(Exception $e){
	    	return redirect('product/add')->with([
	                'error' 	   => true,
	                'error.title'  => 'Error..!',
	                'error.message'=> $e->getMessage()
	        ])->withInput(); 
	    }
	}

	/**
	 * This function is used  to make the PRODUCT VIEW
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$warrenty = null;
			$return   = null;

			$product_details = $this->product->getProductDetails($id);

			$specs = json_decode($product_details['specs']);

			unset($specs->code,$specs->name,$specs->display_name,$specs->price_term,$specs->description,$specs->moq,$specs->selling_price);

			$spec_keys = array_keys((array)$specs);

			if(count($product_details->warrenty) > 0 && $product_details->warrenty->warrenty_period > 0){
				$warrenty = $this->common->readDate($product_details->warrenty->warrenty_period,$product_details->warrenty->period_type);
			}else{
				$warrenty = 'Life Time Warrenty';
			}
			if(count($product_details->return_product) > 0 && !empty($product_details->return_product)){
				$return = $this->common->readDate($product_details->return_product->return_period,$product_details->return_product->period_type);
			}

			return view('Product::view')->with([
						'details' => $product_details,
						'warrenty'=> $warrenty,
						'return'  => $return,
						'specs'  => $specs,
						'spec_keys'  => $spec_keys
					]);
		}catch(\Exception $e){
			return redirect('product/list')->with([
                'error' 	   => true,
                'error.title'  => 'Error..!',
                'error.message'=> $e->getMessage()
	        ]);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

		try{
			
			$main_category 	  = $this->category->getAllCategoryType();
			$taxType	  = $this->product->getTaxType();
			$product_detail       	= $this->product->getProductDetails($id);

			return view('Product::edit')->with([
				'product_detail' 		=> $product_detail,				
				'main_category' 		=> $main_category,
				'id'					=> $id,
				'taxType'			    => $taxType
			]);
		}catch(\Exception $e){
			return redirect('product/list')->with([
                'error' 	   => true,
                'error.title'  => 'Error..!',
                'error.message'=> $e->getMessage()
	        ]);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{	
		//validate form request data
		$this->validate($request,[
			'item_name'				=> 'required|unique:product,name,'.$id,
            'display_name'			=> 'unique:product,name,'.$id.'|unique:product,display_name,'.$id,
            'tax_type'				=> 'required',
            'selling_price'			=> 'required|numeric'
        ]);
       
		try{
			/** ============== product images upload =============================*/
			$this->common->makeDirNotExist('uploads/images/product',0777,true);
			$img_arr = array();
            if($request->hasFile('images')){
                $files = $request->file('images');
                $i = 0;
                foreach($files as $file){
                    $extn = $file->getClientOriginalExtension(); //get extension
                    $fileName = 'item-'.$i.date('YmdHis').'.'.$extn; //set file name
                    $destinationPath = storage_path('uploads/images/product'); // destination
                    $file->move($destinationPath, $fileName);//move to file
                    
                    array_push($img_arr,$fileName);
                    $i++;
                }
            }
	        $this->product->updateProduct($id,$request->all(),$img_arr);
	        return redirect( 'product/list' )->with([
	            'success' 		 => true,
	            'success.title'  => 'Success..!',
	            'success.message'=> 'Product updated successfully'
	        ]);
	    
        }catch(Exception $e){
        	return back()->with([
	                'error' 	   => true,
	                'error.title'  => 'Error..!',
	                'error.message'=> $e->getMessage()
	        ])->withInput();
        }
	}

	/**
	 *This function is used to add highly moving items
	 * @param
	 * @return 
	 */

	public function homeDecide(Request $request)
	{
		if($request->ajax()){
			try{
				$homeDecide = $this->product->homePageProduct($request->get('section_id'),$request->get('product'));
				return Response(['data' => $homeDecide]);
			}catch(\Exception $e){
				return Response([]);
			}
		}else{
			return Response([]);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request)
	{
		if($request->ajax()){
			$deleteProduct = $this->product->deleteProduct($request->get('product'));
			return Response(['data' => $deleteProduct]);
		}else{
			return Response([]);
		}
	}

	public function bulkUploadView(Request $request)
	{
	    return view('Product::bulkimage');
    }

    public function bulkUpload(Request $request)
    {
        $this->validate($request,[
            'upload_type'	=> 'required',
            'file'			=> 'required|mimes:zip'
        ]);

        try {

            $savePath = '';

            if ($request->upload_type == 2) {
                $savePath = 'uploads/datasheet/product';
                $this->common->makeDirNotExist($savePath, 0777, true);
            } else {
                $savePath = 'uploads/images/product';
                $this->common->makeDirNotExist($savePath, 0777, true);
            }

            if ($request->hasFile('file')) {
                $file = $request->file('file');

                $extractTo = Zipper::make($file)->extractTo(storage_path($savePath));
                Zipper::close();

                $it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(storage_path($savePath)));
                $it->setMaxDepth(0);
                $it->rewind();

                $count = 0;
                $files = [];

                while ($it->valid()) {
                    if (!$it->isDot()) {

                        //Bulk Image Upload Part
                        if ($request->upload_type == 1) {
                            $allowedMimeTypes = ['image/jpeg', 'image/jpg', 'image/png'];
                            $contentType = mime_content_type(storage_path($savePath) . '/' . $it->current()->getFilename());

                            if (in_array($contentType, $allowedMimeTypes)) {
                                $image = $this->product->insertBulkImage(
                                    $it->current()->getFilename(),
                                    $it->current()->getBasename('.' . $it->current()->getExtension())
                                );

                                if ($image) {
                                    array_push($files, $image);
                                    $count++;
                                }
                            }

                            //Bulk Datasheet Upload Part
                        } elseif ($request->upload_type == 2) {
                            $allowedMimeTypes = ['application/pdf'];
                            $contentType = mime_content_type(storage_path($savePath) . '/' . $it->current()->getFilename());

                            if (in_array($contentType, $allowedMimeTypes)) {
                                $sheet = $this->product->insertBulkDatasheet(
                                    $it->current()->getFilename(),
                                    $it->current()->getBasename('.' . $it->current()->getExtension())
                                );

                                if ($sheet) {
                                    array_push($files, $sheet);
                                    $count++;
                                }
                            }
                        }

                    }

                    $it->next();
                }

                if ($request->upload_type == 1) {
                    ProductImage::insert($files);

                    return redirect('product/bulk-upload')->with([
                        'success' => true,
                        'success.title' => 'Success!',
                        'success.message' => $count . ' images uploaded successfully'
                    ]);
                } elseif ($request->upload_type == 2) {
                    Datasheet::insert($files);

                    return redirect('product/bulk-upload')->with([
                        'success' => true,
                        'success.title' => 'Success!',
                        'success.message' => $count . ' datasheets uploaded successfully'
                    ]);
                }
            }
        }catch(Exception $e){
            return redirect('product/bulk-upload')->with([
                'error' 	   => true,
                'error.title'  => 'Error!',
                'error.message'=> $e->getMessage().' in file '.$e->getFile().' at line '.$e->getLine()
            ])->withInput();
        }
    }

	/**
	 * Display upload product view.
	 *
	 * @return Response
	 */
	public function uploadProductView(Request $request)
	{
		$countries 		= $this->product->getAllCountries();
		$category_types = $this->category->getAllCategoryType();
		$categories 	= $this->category->getAllCategories();

		return view('Product::upload')->with([
			'countries'			=> $countries,
			'main_category'		=> $category_types,
			'categories'		=> $categories
		]);
	}

	public function getCategoryHierarchy(Request $request){
		return $this->category->getCategoryHierarchyList($request);
	}

	/**
	 * upload product.
	 *
	 * @return Response
	 */
	public function uploadProducts(Request $request)
	{
		$this->validate($request, [
			'main_category'	=> 'required_without_all:main_category',
            'sub_category'  => 'required_without_all:sub_category',
			'data_file' 	=> 'required',
			'country' 		=> 'required'
		]);

		set_time_limit(0);
		$data = $this->product->uploadProductExcel($request);

		return redirect('product/upload')->with([
			'data' => $data
		]);
	}

	/**
	 * Display upload product view.
	 *
	 * @return Response
	 */
	public function separateExcelView(Request $request)
	{
		return view('Product::excel');
	}

	public function separateExcel(Request $request)
	{
		set_time_limit(0);
		return $data = $this->product->separateProductExcel($request);
	}


}
