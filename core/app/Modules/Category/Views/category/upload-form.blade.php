<div class="form-group {{ $errors->has('category_type') ? 'has-error' : ''}}">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right required" title="category_type">Category type</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        {!! 
            Form::select('category_type', $category_types, isset($category_info->category_type_id)? $category_info->category_type_id : old('category_type'), [
                'class'       => 'form-control chosen', 
                'name'          => 'category_type',
                'id'          => 'category_type'
            ]) 
        !!}
        @if($errors->has('category_type'))
            <span class="help-block">
                {{ $errors->first('category_type') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('parent_category') ? 'has-error' : ''}}">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right" title="parent_category">Parent category</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        {!! 
            Form::select('parent_category', $categories, isset($category_info->parent_id)? $category_info->parent_id : old('parent_category'), [
                'class'       => 'form-control chosen', 
                'name'        => 'parent_category',
                'id'          => 'parent_category',
            ]) 
        !!}
        @if($errors->has('parent_category'))
            <span class="help-block">
                {{ $errors->first('parent_category') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('data_file') ? 'has-error' : ''}}">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right" title="data_file">Upload File</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        <input id="data_file" name="data_file" type="file" class="file">
        @if($errors->has('data_file'))
            <span class="help-block">
                {{ $errors->first('data_file') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <label class="control-label pull-right" title="">&nbsp;</label>
    </div>
    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary bg-purple pull-right', 'id' => 'btn_save' , 'style' => 'border:0px;', 'disabled' => true]) !!}
    </div>
</div>
@section('common_script')
<script type="text/javascript">
$(document).ready(function() {
    
    $('#category_form').on('change keyup', 'input, select', function(e){
        if($('#name').val() == "" || $('#display_name').val() == "" || $('#category_type').val() == "")
        {
            $('#btn_save').attr('disabled', true);
        }
        else
        {
            $('#btn_save').attr('disabled', false);
        }
    });

    //load categories to category selectbox
    $('#category_type').on('change', function(event){
        if($(this).val() !== '')
        {
            $.ajax({
                url: "{{URL::to('category/get-category-hierarchy')}}",
                method: 'GET',
                data: {
                    'category_type_id':$(this).val()
                },
                async: false,
                success: function (data) {
                    $('#parent_category').html('');
                    $('#parent_category').append(
                        '<option value="root">root</option>'
                    );
                    $.each(data,function(key,value){
                        // console.log(value.id)
                        $('#parent_category').append(
                            '<option value="'+value.id+'">'+value.name+'</option>'
                        );                        
                    });
                    $('#parent_category').trigger("chosen:updated");                
                },error: function () {
                    $.alert({
                        theme: 'material',
                        title: 'Error Occured',
                        type: 'red',
                        content: 'Error Occured when getting filters'
                    }); 
                }
            });
        }
        else
        {
            console.log('category type id cannot be empty.');
        }
    });   
});
</script>
@stop