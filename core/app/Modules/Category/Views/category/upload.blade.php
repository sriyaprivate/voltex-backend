
@extends('layouts.back_master') @section('title','Add Menu')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/bootstrap-tokenfield.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/jquery-ui.css')}}">
<style type="text/css">
      
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Category 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li>Category</li>
        <li class="active">Add</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Category</h3>
            <div class="box-title pull-right">
                <a href="{{ route('category.index') }}" class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </div>
        </div>
        <div class="box-body">
            <form method="post" action="{{ route('category.upload') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" id="category_form">
                {!! csrf_field() !!}
                @include ('Category::category.upload-form')
            </form>
        </div>
    </div>  
</section>
@stop
@section('js')
<script src="{{asset('assets/dist/bootstrap-token/js/bootstrap-tokenfield.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-token/js/jquery-ui.js')}}"></script>
@yield('common_script')
@stop