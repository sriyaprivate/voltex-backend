@extends('layouts.back_master') @section('title','Add Menu')
@section('css')
<style type="text/css">
  
    
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Category 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li>Category</li>
        <li class="active">List</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">        
        <div class="box-body">
           {!! Form::open(['method' => 'GET', 'route' => 'category.index', 'role' => 'search'])  !!}
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">search</label>
                        {!! 
                            Form::select('status', 
                                ['all' => 'All', '1' => 'Active', '0' => 'Inactive'], 
                                Input::get('status'), 
                                [
                                'class' => 'form-control chosen'
                            ]);
                        !!}
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label class="control-label">Category Type</label>
                        {!! 
                            Form::select('category_type', 
                                $category_types, 
                                Input::get('category_type'), 
                                [
                                'class' => 'form-control chosen'
                            ]);
                        !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">&nbsp;&nbsp;</label>
                        <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ Input::get('search')?:old('search') }}">
                    </div>
                </div>
                <div class="col-md-2">
                    <label class="control-label">&nbsp;&nbsp;</label>
                    <button class="btn btn-default pull-right btn-block" type="submit"><span class="fa fa-search"></span> Search</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div> 

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Category</h3>
            <div class="box-title pull-right">
                <a href="{{ route('category.create') }}" class="btn btn-success btn-sm" title="Add New Category">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                </a>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Category Type</th>
                            <th>Category</th>
                            <th>Parent Category</th>
                            <th>Description</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($categories) > 0)
                        @foreach($categories as $key => $category)
                            <tr>
                                <td>{{ (++$key) }}</td>
                                <td>{{ $category->type->name?:'-' }}</td>
                                <td>{{ str_repeat("-", $category->depth)." ".$category->display_name?:'-' }}</td>
                                <td>{{ $category->parent_name?:'-' }}</td>
                                <td>{{ $category->description?:'-' }}</td>
                                <td>{{ $category->created_at->format('Y-m-d') }}</td>
                                <td>
                                    @if($category->status == 1)
                                        <span class="fa fa-check-circle" style="color: rgba(22, 160, 133,1.0);"></span>
                                    @else
                                        <span class="fa fa-check-circle" style="color: rgba(127, 140, 141,1.0);"></span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('category.show', $category->category_id) }}" title="View Category"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                    <a href="{{ route('category.edit', $category->category_id) }}" title="Edit Category"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                    <button type="button" data-url="{{ route('category.destroy', ['id' => $category->category_id]) }}" title="Delete Category" class="btn btn-danger btn-xs" name="btn-delete">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Delete
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="10">
                                <h3 align="center" class="text-grey">Data not found!.</h3>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <div class="pagination-wrapper"> {!! count($categories) > 0?$categories->appends(['search' => Request::get('search'), 'status' => Request::get('status'), 'category_type' => Request::get('category_type')])->render():'' !!} </div>
            </div>            
        </div>
    </div>  
</section>



@stop
@section('js')

<script type="text/javascript">
$(document).ready(function() {
    $('button[name=btn-delete]').click(function(){
        var link = $(this).data('url');
        console.log(link);
        //params [link, title, message]
        confirm_delete(link, 'Are you sure?', 'You wanna delete this category?.');
    });
});
</script>
@stop


































