

@extends('layouts.back_master') @section('title','Add Menu')
@section('css')
<style type="text/css">
    .tb{
        margin-left: -10px;
    }
    .tb td{
        padding: 10px;
    }    
    .tb td:first-child{
        border-right: 1px solid #000;
    }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Category 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li><a href="{{ route('category.index')}}">Category</a></li>
        <li class="active">Show</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Category : {{ $category->display_name }}</h3>
            <div class="box-title pull-right">
                <button type="button" onclick="goBack()" class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
                <a href="{{ route('category.edit', $category->category_id) }}" title="Edit Category"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                <button type="button" data-url="{{ route('category.destroy', ['id' => $category->category_id]) }}" title="Delete Category" class="btn btn-danger btn-xs" name="btn-delete">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Delete
                </button>
            </div>
        </div>
        <div class="box-body">
            <br/>
            <br/>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <td width="20%">ID</td>
                            <td><strong>{{ $category->category_id?:'-' }}</strong></td>
                        </tr>
                        <tr>
                            <td>Category Type</td>
                            <td><strong>{{ $category->type->name?:'-' }}</strong></td>
                        </tr>
                        <tr>
                            <td>Parent Category</td>
                            <td><strong>{{ $category->parent_name?:'-' }}</strong></td>
                        </tr>
                        <tr>
                            <td>Category</td>
                            <td><strong>{{ $category->display_name?:'-' }}</strong></td>
                        </tr>
                        <tr>
                            <td>Description</td>
                            <td><strong>{{ $category->description?:'-' }}</strong></td>
                        </tr>
                        <t>
                            <td>Filters</td>
                            <td>
                                @if(count($category->filter) > 0)
                                    <table class="tb">
                                        @foreach($category->filter as $data)
                                            <tr>
                                                <td width="20%"><strong>{{ $data->main_filter->name?:'-' }}</strong></td>
                                                <td>
                                                    @if(count($data->filter_values) > 0)
                                                        <?php $array = [];?>
                                                        @foreach($data->filter_values as $key => $info)
                                                            <?php array_push($array, $info->value) ?>
                                                        @endforeach
                                                        {{ join(', ', $array) }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @else
                                    <strong>-</strong>
                                @endif
                            </td>
                        </t>
                        <tr>
                            <td>Date</td>
                            <td><strong>{{ $category->created_at?:'-' }}</strong></td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>
                                @if($category->status == 1)
                                    <span class="fa fa-check-circle" style="color: rgba(22, 160, 133,1.0);"></span>
                                @else
                                    <span class="fa fa-check-circle" style="color: rgba(127, 140, 141,1.0);"></span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Icon</td>
                            <td>
                                @if(!empty($category->icon_path))
                                    <img src="{{ url(UPLOAD_PATH.UPLOADS_DIR.$category->icon_path) }}" width="150" height="150" alt="category icon">
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Image</td>
                            <td>
                                @if(!empty($category->image_path))
                                    <img src="{{ url(UPLOAD_PATH.UPLOADS_DIR.$category->image_path) }}" width="150" height="150" alt="category image">
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>  
</section>

@stop
@section('js')

<script type="text/javascript">
$(document).ready(function() {
    $('button[name=btn-delete]').click(function(){
        var link = $(this).data('url');
        console.log(link);
        //params [link, title, message]
        confirm_delete(link, 'Are you sure?', 'You wanna delete this category?.');
    });
});
</script>
@stop




























