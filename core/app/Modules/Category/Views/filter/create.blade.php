
@extends('layouts.back_master') @section('title','Add Filter')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/bootstrap-tokenfield.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/jquery-ui.css')}}">
<style type="text/css">
      
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Filter 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li>Category</li>
        <li>Filter</li>
        <li class="active">Add</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Filter</h3>
            <div class="box-title pull-right">
                <a href="{{ route('category.filter.index') }}" class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </div>
        </div>
        <div class="box-body">
            <form method="post" action="{{ route('category.filter.store') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" id="filter_form">
                {!! csrf_field() !!}
                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <label class="control-label pull-right required" title="name">Filter Name</label>
                    </div>
                    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Enter filter name..." value="{{ old('name') }}">
                        @if($errors->has('name'))
                            <span class="help-block">
                                {{ $errors->first('name') }}
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <label class="control-label pull-right required" title="type">Category Type</label>
                    </div>
                    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
                        {!! 
                            Form::select('category_type', $category_types, old('category_type'), [
                                'class'       => 'form-control chosen', 
                                'name'        => 'category_type',
                                'id'          => 'category_type'
                            ]) 
                        !!}
                        
                        @if($errors->has('category_type'))
                            <span class="help-block">
                                {{ $errors->first('category_type') }}
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <label class="control-label pull-right" title="type"></label>
                    </div>
                    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
                        <button type="submit" class="btn btn-primary bg-purple pull-right">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>  
</section>
@stop
@section('js')
<script src="{{asset('assets/dist/bootstrap-token/js/bootstrap-tokenfield.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-token/js/jquery-ui.js')}}"></script>
@yield('common_script')
@stop