
@extends('layouts.back_master') @section('title','Add Menu')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/bootstrap-tokenfield.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/jquery-ui.css')}}">
<style type="text/css">
  
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Filter
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li><a href="{{ route('category.index') }}">Category</a></li>
        <li><a href="{{ route('category.filter.index') }}">Filter</a></li>
        <li class="active"><a href="{{ route('category.filter.edit', ['id' => $id]) }}">Edit</a></li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Filter</h3>
            <div class="box-title pull-right">
                <a href="{{ route('category.filter.index') }}" class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </div>
        </div>
        <div class="box-body">
            <form action="{{ route('category.filter.update', $id) }}" method="post" class="form-horizontal" enctype="multipart/form-data" accept-charset="UTF-8" id="filter_form">
                {!! csrf_field() !!}
                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <label class="control-label pull-right required" title="filter name">Filter name</label>
                    </div>
                    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Enter filter name..." value="{{ isset($filter->name)? $filter->name : old('name') }}">
                        @if($errors->has('name'))
                            <span class="help-block">
                                {{ $errors->first('name') }}
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('category_type') ? 'has-error' : ''}}">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <label class="control-label pull-right required" title="category_type">Category type</label>
                    </div>
                    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
                        {!! 
                            Form::select('category_type', $category_types, isset($filter->type_id)? $filter->type_id : old('category_type'), [
                                'class' => 'form-control chosen', 
                                'name'  => 'category_type',
                                'id'    => 'category_type'
                            ]) 
                        !!}
                        @if($errors->has('category_type'))
                            <span class="help-block">
                                {{ $errors->first('category_type') }}
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-2">
                        <label class="control-label pull-right">Status</label>
                    </div>
                    <div class="col-lg-3">
                        <select name="status" class="form-control chosen">
                            @if(isset($filter) && $filter->status == '1')
                                <option value="1" selected>Active</option>
                                <option value="0">Inactive</option>
                            @elseif(isset($filter) && $filter->status == '0')
                                <option value="1">Active</option>
                                <option value="0" selected>Inactive</option>
                            @else
                                <option value="">Status not found!.</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <label class="control-label pull-right" title="">&nbsp;</label>
                    </div>
                    <div class="col-lg-9 col-md-9 col-ms-9 col-xs-9">
                        <button type="submit" name="btn_save" id="btn_save" class="btn btn-primary bg-purple pull-right" disabled>Update</button>
                    </div>
                </div>

            </form>
        </div>
    </div>  
</section>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('#filter_form').on('change keyup', 'input, select, textarea', function(e){
        checkAllOk();
    });

    function checkAllOk()
    {
        if($('#name').val() == "" || $('#category_type').val() == "")
        {
            $('#btn_save').attr('disabled', true);
        }
        else
        {
            $('#btn_save').attr('disabled', false);
        }
    }
});
</script>
@stop