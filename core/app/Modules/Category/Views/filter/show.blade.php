@extends('layouts.back_master') @section('title','Filter show')
@section('css')
<style type="text/css">
    .tb{
        margin-left: -10px;
    }
    .tb td{
        padding: 10px;
    }    
    .tb td:first-child{
        border-right: 1px solid #000;
    }
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Filter
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li><a href="{{{route('category.index')}}}">Category</a></li>
        <li><a href="{{{route('category.filter.index')}}}">Filter</a></li>
        <li class="active"><a href="{{{route('category.filter.show', ['id' => $filter->id])}}}">Show</a></li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Filter : {{ $filter->name }}</h3>
            <div class="box-title pull-right">
                <a href="{{ route('category.filter.index') }}" class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                <a href="{{ route('category.filter.edit', $filter->id) }}" title="Edit filter"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                <button type="button" data-url="{{ route('category.filter.destroy', ['id' => $filter->id]) }}" title="Delete Filter" class="btn btn-danger btn-xs" name="btn-delete">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Delete
                </button>
            </div>
        </div>
        <div class="box-body">
            <br/>
            <br/>
            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <td width="20%">ID</td>
                            <td><strong>{{ $filter->id?:'-' }}</strong></td>
                        </tr>
                        <tr>
                            <td>Category Type</td>
                            <td><strong>{{ $filter->type?:'-' }}</strong></td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td><strong>{{ $filter->name?:'-' }}</strong></td>
                        </tr>
                        <tr>
                            <td>Date</td>
                            <td><strong>{{ $filter->created_at?:'-' }}</strong></td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>
                                @if($filter->status == 1)
                                    <span class="fa fa-check-circle" style="color: rgba(22, 160, 133,1.0);"></span>
                                @else
                                    <span class="fa fa-check-circle" style="color: rgba(127, 140, 141,1.0);"></span>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>  
</section>

@stop
@section('js')

<script type="text/javascript">
$(document).ready(function() {
    $('button[name=btn-delete]').click(function(){
        var link = $(this).data('url');
        console.log(link);
        //params [link, title, message]
        confirm_delete(link, 'Are you sure?', 'You wanna delete this category?.');
    });
});
</script>
@stop




























