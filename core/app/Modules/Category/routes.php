<?php
/**
 * CATEGORY MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-09
 */
 
Route::group(['middleware' => ['auth']], function(){
    Route::group(['prefix' => 'category', 'namespace' => 'App\Modules\Category\Controllers'],function(){

    	/*===================================
					GEt request
    	===================================*/
		Route::get('create', [
            'uses' => 'CategoryController@create',
            'as'   => 'category.create'
        ]);

        Route::get('upload', [
			'uses' => 'CategoryController@uploadCategoryView',
			'as'   => 'category.upload'
        ]);

        Route::get('/', [
			'uses' => 'CategoryController@index',
			'as'   => 'category.index'
        ]);

        Route::get('delete/{id}', [
			'uses' => 'CategoryController@destroy',
			'as'   => 'category.destroy'
        ]);

        Route::get('show/{id}', [
            'uses' => 'CategoryController@show',
            'as'   => 'category.show'
        ]);

        Route::get('edit/{id}', [
            'uses' => 'CategoryController@edit',
            'as'   => 'category.edit'
        ]);

        Route::get('get-category-hierarchy', [
            'uses' => 'CategoryController@getCategoryHierarchy',
            'as' => 'category.hierarchy'
        ]);

        Route::get('get-sub-category', [
            'uses' => 'CategoryController@getSubCategory',
            'as' => 'category.sub.hierarchy'
        ]);

        /*===================================
					POST request
    	===================================*/
    	Route::post('store', [
            'uses' => 'CategoryController@store',
            'as'   => 'category.store'
        ]);

        Route::post('upload', [
    		'uses' => 'CategoryController@uploadCategory',
    		'as'   => 'category.upload'
    	]);

        Route::post('load', [
            'uses' => 'CategoryController@load_category',
            'as'   => 'category.load' 
        ]);

        Route::post('update/{id}', [
            'uses' => 'CategoryController@update',
            'as'   => 'category.update'
        ]);
	});  	
}); 

