<?php
/**
 * CATEGORY MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-09
 */
 
namespace App\Modules\Category\Requests;

use App\Http\Requests\Request;

class CategoryRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$id = $this->id;
		$rules = [
			'name'            => 'required',
			'display_name'    => 'required',
			'parent_category' => 'required'
		];
		
		return $rules;
	}

}
