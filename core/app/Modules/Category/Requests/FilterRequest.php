<?php
/**
 * FILTER MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-12-24
 */
 
namespace App\Modules\Category\Requests;

use App\Http\Requests\Request;

class FilterRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
        $id = $this->id;
        
		$rules = [
			'name'            => 'required|unique:filter,name,'.$id.',,deleted_at,NULL',
			'category_type'   => 'required'
		];
		
		return $rules;
	}

}
