<?php
/**
 * CATEGORY MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-09
 */

namespace App\Modules\Category\CategoryRepository;

use Exception;
use Excel;
use Response;
use DB;

use App\Modules\Category\Models\Category;
use App\Modules\Category\Models\CategoryType;
use App\Classes\Functions;

class CategoryRepository {
	/**
	 * category type, except category ids [], 
	 * return type 'get' for get all data and 'list' for get list to selectbox  
	 */	
	public function getAllCategory($category_type_id = null, $except_id = null, $return_type = 'get'){
		try {
			if($category_type_id !== null){
				
				if($return_type == 'get'){
				
					if($except_id !== null){
						$category = Category::where('category_type_id', $category_type_id)
							->orderBy('lft', 'ASC')
							->get()
							->except($except_id)
							->toArray();

						if(count($category) > 0){
							return Functions::getCategoryTree($category);
						}else{
							return [];	
						}
					}else{
						throw new \Exception('invalid parameters...except id cannot found!.');
					}
				}else if('list'){
					$category = Category::where('category_type_id', $category_type_id)
						->orderBy('lft', 'ASC')
						->get()
						->except($except_id)
						->toArray();

					$category = array_merge(['root' => 'root'], $category);

					if(count($category) > 0){
						return Functions::getCategoryTree($category);
					}else{
						return [];	
					}
				}else{
					return [];
				}
			}else{
				throw new \Exception('category id not found!.');
			}
		} catch (\Exception $e) {
			throw new \Exception('message: '.$e->getMessage());
		}
	}

	//get all category type
	public function getAllCategoryByType($type){
		try {
			$category = Category::where('category_type_id',$type)->lists('name', 'id');
			
			if(count($category) > 0){
				return $category;
			}else{
				return [];
			}
		} catch (\Exception $e) {
			throw new \Exception('message: '.$e->getMessage());
		}
	}

	//get all categories
	public function getAllCategories(){
		try {
			$category = Category::lists('name', 'id');
			
			if(count($category) > 0){
				return $category;
			}else{
				return [];
			}
		} catch (\Exception $e) {
			throw new \Exception('message: '.$e->getMessage());
		}
	}

	//get all category type
	public function getAllCategoryType(){
		try {
			$categoryType = CategoryType::lists('name', 'id');
			
			if(count($categoryType) > 0){
				return $categoryType;
			}else{
				return [];
			}
		} catch (\Exception $e) {
			throw new \Exception('message: '.$e->getMessage());
		}
	}
		
	//save category in db
	public function saveCategory($request, $status = 1){
		try {
			DB::beginTransaction();

			//variable declaration and assign values
			$name                   = $request->input('name');
			$display_name           = $request->input('display_name');
			$category_type_id       = $request->input('category_type');
			$parent_category        = $request->input('parent_category');
			$description            = $request->input('description');

			//save category data in category table
			$category                   = new Category();
			$category->category_type_id = $category_type_id; 
			$category->name             = $name; 
			$category->display_name     = $display_name;
			$category->description      = $description; 
			$category->status           = $status; 
			$category->created_at       = date('Y-m-d h:i:s'); 
			$category->save();

			if(count($category) > 0){
				
				if($parent_category !== "root"){
					//make tree hierarchy
					$parent = $this->getCategoyById($parent_category);
		        	$category->makeChildOf($parent);
				}

				DB::commit();
				return $category;
				
			}else{
				DB::rollback();
				throw new \Exception("Something went wrong, Category couldn't be saved!.");
			}
	    } catch (\Exception $e) {
			throw new \Exception($e->getMessage());
		}
	}
		
	//save category in db
	public function uploadCategory($request, $status = 1)
	{

		$file 		= $request->data_file;
		$data = [];
		Excel::load($file, function($reader) use (&$data){
			$reader->each(function($row) use (&$data){
				$data[] = $row->name;
			});
		});

		$category_type_id       = $request->input('category_type');
		$parent_category        = $request->input('parent_category');
		
		foreach ($data as $value) {
			$category = new Category();
			$category->category_type_id = $category_type_id; 
			$category->name             = $value; 
			$category->display_name     = $value;
			$category->description      = ""; 
			$category->status           = 1; 
			$category->created_at       = date('Y-m-d h:i:s');

			if($category->save()){
				if($parent_category !== "root"){
					//make tree hierarchy
					$parent = $this->getCategoyById($parent_category);
		        	$category->makeChildOf($parent);
				}
				
			}else{
				// DB::rollback();
				throw new \Exception("Something went wrong, Category couldn't be saved!.");
			}
		}

		return 1;
	}

	//get category by id
	public function getCategoyById($id)
	{
		try {
			if(!empty($id)){
				$category = Category::find($id);

				if($category){
					return $category;
				}else{
					throw new \Exception('Category "'.$id.'" not found');
				}
			}	
			else{
				throw new \Exception('Category id not found!.');
			}		
		} catch (\Exception $e) {
			throw new \Exception($e->getMessage());
		}
	}

	//get all category with filters and filter value
	public function getCategories($id = null, $request = null, $perPage = null)
	{	
		if(!empty($request)){
			$keyword       = trim($request->input('search'));
			$status        = $request->input('status');
			$category_type = $request->input('category_type');
		}else{
			$keyword       = '';
			$status        = '';
			$category_type = '';
		}

		try {
			$categories = Category::select('category.id as category_id', 
				'category.name', 
				'category.id', 
				'category.depth', 
				'category.display_name', 
				'category.image_path', 
				'category.icon_path', 
				'category.description', 
				'category.parent as parent_id', 
				'category.status', 
				'category.created_at',
				'category.category_type_id', 
				DB::raw('(SELECT name FROM category WHERE id = parent_id) as parent_name'),
				
					DB::raw('
								(
									SELECT 
										c.name 
									FROM 
										category c 
									WHERE c.id = category.parent
								) as parent'
							)
				);


			//filter by id
			if(!empty($id)){
				 $categories = $categories->where('category.id', $id);
			}

			//filter by status
			if($status != '' && $status != 'all'){
				$categories = $categories->where('category.status', $status);				
			}

			//filter by category type
			if($category_type != '' && $category_type != 'all'){
				$categories = $categories->where('category.category_type_id', $category_type);				
			}

			//filter by keyword
			if(!empty($keyword) && !empty($perPage)){
				$categories = $categories->where(function($categories) use ($keyword){
					$categories->where('category.display_name', 'LIKE', '%'.$keyword.'%')
						->orWhere('category.description', 'LIKE', '%'.$keyword.'%');
				});
			}

			//paginate result
			if(!empty($perPage)){
				$categories = $categories
					->orderBy('category_type_id', 'ASC')
					->orderBy('lft', 'ASC')
					->paginate($perPage);
			}else{
				$categories = $categories->first();
			}

			//if category found return category array
			if(count($categories) > 0){
				return $categories;
			}else{
				return $categories = [];
			}
		} catch (\Exception $e) {
			throw new \Exception('message: '.$e->getMessage());
		}
	}

	//delete category
	public function deleteCategory($cate_id)
	{
		try {
			DB::beginTransaction();
			if(!empty($cate_id)){
				$deleted = Category::where('id', $cate_id)->delete();

				if($deleted){
					DB::commit();
					return $deleted;
				}else{
					DB::rollback();
					throw new \Exception("Something went wrong, category couldn't be deleted!.");
				}
			}else{
				DB::rollback();
				throw new \Exception("Category "."\"".$cate_id."\" couldn't found!.");
			}
		} catch (\Exception $e) {
			throw new \Exception('message: '.$e->getMessage());	
		}
	}

	//update category
	/*
	* params: category id, data request, image path, icon path, status
	*/
	public function is_value_in_array($needle, $haystack, $strict = false)
	{
		if(count($haystack) > 0){
			foreach($haystack as $key => $item) {
				if(($strict ? $item === $needle : $item == $needle) || (is_array($item) && count($this->is_value_in_array($needle, $item, $strict)) > 0)) {
					return $key;
				}
			}
		}
	}
	
	public function updateCategory($category_id, $request)
	{		
		try {
			DB::beginTransaction();

			//variable declaration and assigned
			$name              = $request->input('name');
			$display_name      = $request->input('display_name');
			$category_type_id  = $request->input('category_type');
			$parent_category   = $request->input('parent_category');
			$description       = $request->input('description');
			$old_category_id   = $request->input('old_category_id');
			$status            = $request->input('status');

			//update category table
			$category                   = Category::find($category_id);
			$category->category_type_id = $category_type_id; 
			$category->name             = $name; 
			$category->display_name     = $display_name;
			$category->description      = $description; 

			if(empty($is_update_photo)){
				$category->image_path   = $img_path; 
			}

			if(empty($is_update_icon)){
				$category->icon_path    = $icon_path;
			}
			$category->status           = $status; 
			$category->created_at       = date('Y-m-d h:i:s'); 	
			$category->update();
			
			if(count($category) > 0){
				//update tree hierarchy
				if($parent_category !== "root"){
					$parent = $this->getCategoyById($parent_category);
		        	$category->makeChildOf($parent);
				}else{
		        	$category = Category::find($category_id);
		        	$category->parent = null;
		        	$category->update();
				}

				DB::commit();
				return $category;
				
			}else{
				DB::rollback();
				throw new \Exception("Something went wrong, Category couldn't be updated!.");
			}

	    } catch (\Exception $e) {
			throw new \Exception($e->getMessage());
		}
	}

	/** 
	 * This function is used to get categories to populate dropdown list
	 * @param 
	 * @return $category_object
	 */

	public function getCategory(){
		return $category = Category::select('id','name','category_type_id')->where('status',1)->get();
	}

	/** 
	 * This function is used to get sub categories to populate dropdown list
	 * @param 
	 * @return $category_object
	 */

	public function getSubCategory($category_id){
		return $category = Category::select('id','name','category_type_id')->where('category_type_id',$category_id)->get();
	}

	public function getLocationHierarchyList1(){
		$data = Category::with(['parentCategory'])->first()->getDescendantsAndSelf();
        $jsonList = array();
        $i=1;
        $jsonList[0]="Root";
        foreach ($data as $key => $category) {
            $label = '--';
            for($j=0; $j < $category->depth; $j++) {
                $label .= '--';
            }

            $label .= ' '.$category->name;
            $jsonList[$category->id]=$label;
        }

        return $jsonList;
	}

	public function getCategoryHierarchyList($request){

		$data = Category::with(['parentCategory'])->where('category_type_id',$request->category_type_id)->whereNull('parent')->get();

		$jsonList = array();

		foreach ($data as $key => $value) {

			$jsonList[] = ['id'=>$value->id, 'name'=> $value->name];

			$_tmp = $value->getDescendants();

			foreach ($_tmp as $child) {
				$jsonList[] = ['id'=>$child->id, 'name'=> '--'.$child->name];

				$_tmp_node = $child->getDescendants();

				foreach ($_tmp_node as $grand_child) {
					$jsonList[] = ['id'=>$grand_child->id, 'name'=> '----'.$grand_child->name];
				}

			}

		}
        
        return $jsonList;
	}

	public function getCategoryHierarchyListByTypeId($category_type_id){

		$data = Category::with(['parentCategory'])->where('category_type_id',$category_type_id)->whereNull('parent')->get();

		$jsonList = array();

		foreach ($data as $key => $value) {

			$jsonList[] = ['id'=>$value->id, 'name'=> $value->name];

			$_tmp = $value->getDescendants();

			foreach ($_tmp as $child) {
				$jsonList[] = ['id'=>$child->id, 'name'=> '--'.$child->name];

				$_tmp_node = $child->getDescendants();

				foreach ($_tmp_node as $grand_child) {
					$jsonList[] = ['id'=>$grand_child->id, 'name'=> '----'.$grand_child->name];
				}

			}

		}
        
        return $jsonList;
	}


	public function getChildrenOfCategory($category_id){

		$data = Category::find($category_id)->getDescendants();

		$jsonList = array();

		foreach ($data as $key => $value) {

			$jsonList[] = $value->id;

			$_tmp = $value->getDescendants();

			foreach ($_tmp as $child) {
				$jsonList[] = $child->id;

				$_tmp_node = $child->getDescendants();

				foreach ($_tmp_node as $grand_child) {
					$jsonList[] = $grand_child->id;
				}

			}

		}
        
        return $jsonList;
	}
	
}