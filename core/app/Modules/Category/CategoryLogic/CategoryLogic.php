<?php
/**
 * CATEGORY MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-09
 */
 
namespace App\Modules\Category\CategoryLogic;
use App\Modules\Category\Models\Category;
use App\Modules\Category\CategoryRepository\CategoryRepository;
use App\Classes\Functions;
use DB;

class CategoryLogic {
	//variable declaration.
	public $comman       = null;
	public $categoryRepo = null;

	//assign dependancy injection.
	public function __construct(Functions $comman, CategoryRepository $categoryRepo){
		$this->comman       = $comman;
		$this->categoryRepo = $categoryRepo;
	}

	//save image in storage
	public function saveCategory($request){
		try {			
			$res = $categoryRepo->saveCategory($request);
		} catch (\Exception $e) {
			throw new \Exception('message'.$e->getMessage());
		}
	}

	//update category -- save image
	public function updateCategory($category_id, $request)
	{
		try {
			DB::beginTransaction();

			$category = Category::find($category_id);

			$category->category_type_id = $request->get('category_type'); 
			$category->name             = $request->get('name'); 
			$category->display_name     = $request->get('display_name');

			if($category->save()){

				if($request->get( 'parent_category' )!="root"){
					$parent = Category::find( $request->get( 'parent_category' ) );
					$category->makeChildOf($parent);
				}else{
					$category->makeRoot();				
				}

				Category::rebuild();

				DB::commit();
				return $category;
			}else{
				DB::rollback();
				throw new \Exception("Something went wrong, Category couldn't be updated!.");
			}

		} catch (\Exception $e) {
			throw new \Exception('message: '.$e->getMessage());
		}
	}

	public function is_value_in_array($needle, $haystack, $strict = false) {
		foreach ($haystack as $key => $item) {
			if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->is_value_in_array($needle, $item, $strict))) {
				return $key;
			}
		}
	
		return [];
	}
}