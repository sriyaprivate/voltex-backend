<?php
/**
 * CATEGORY MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-09
 */

namespace App\Modules\Category\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryFilter extends Model
{
    use SoftDeletes;
    protected $table      = 'category_filter';
    protected $primarykey = 'id';
    protected $timestamp  = true;
    protected $fillable   = [
        'category_id',
        'filter_id',
        'status', 
        'created_at', 
        'updated_at', 
        'deleted_at'
    ];

    public function category()
    {
        return $this->belongsTo('App\Modules\Category\Models\Category','category_id','id');
    }

    public function filter_values()
    {
        return $this->hasMany('App\Models\FilterValue','category_filter_id','id');
    }

    public function main_filter()
    {
        return $this->belongsTo('App\Models\Filter','filter_id','id');
    }

    


}
