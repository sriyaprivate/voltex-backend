<?php
/**
 * CATEGORY MANAGEMENT
 *
 * @version 1.0.0
 * @author Lahiru Dilshan (Lahiru4unew4@gmail.com)
 * @copyright 2017-10-09
 */

namespace App\Modules\Category\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Baum\Node;

class Category extends Node
{

    use SoftDeletes;

    protected $table = 'category';

    // 'parent_id' column name
    protected $parentColumn = 'parent';

    // 'lft' column name
    protected $leftColumn = 'lft';

    // 'rgt' column name
    protected $rightColumn = 'rgt';

    protected $orderColumn = 'id';

    // guard attributes from mass-assignment
    protected $guarded = array('id', 'parent', 'lft', 'rgt', 'depth');

    protected $scoped = array('category_type_id');

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_type_id', 
        'name', 
        'display_name',
        'description', 
        'image_path', 
        'status', 
        'created_at', 
        'updated_at', 
        'deleted_at'
    ];

    public function parentCategory() {
        return $this->belongsTo($this, 'parent', 'id');
    }

    public function type()
    {
        return $this->belongsTo('App\Modules\Category\Models\CategoryType','category_type_id','id');
    }

    
}
