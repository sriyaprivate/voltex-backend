<?php

namespace App\Modules\Category\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Category\Models\Category;
use App\Modules\Category\CategoryLogic\CategoryLogic;
use App\Modules\Category\CategoryRepository\CategoryRepository;
use Illuminate\Http\Request;
use App\Modules\Category\Requests\CategoryRequest;
use App\Classes\Functions;
use Session;

class CategoryController extends Controller
{
    public $categoryRepo  = null;
    public $categoryLogic = null;
    public $common        = null;

    public function __construct(CategoryLogic $categoryLogic, CategoryRepository $categoryRepo, Functions $common){
        $this->categoryLogic = $categoryLogic;
        $this->categoryRepo  = $categoryRepo;
        $this->common       = $common;  
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        try {
            $perPage        = 25;
            //params [id, request, page count]

            $category_types = $this->categoryRepo->getAllCategoryType();
            
            if(count($category_types) > 0){
                $category_types = $category_types->prepend('All', 'all');
            }

            $categories = $this->categoryRepo->getCategories('', $request, $perPage);

            return view('Category::category.index', compact('categories', 'category_types'));   
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'category.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            ); 
        }
    }

    /**
     * view of category create page
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $category_type = 1;
        $categories     = $this->categoryRepo->getAllCategory($category_type, '', 'list'); //param [type_id, return type]
        $category_types = $this->categoryRepo->getAllCategoryType();

        return view('Category::category.create', compact('categories', 'category_types'));
    }

    /**
     * view of category create page
     *
     * @return \Illuminate\View\View
     */
    public function uploadCategoryView()
    {
        $category_type = 1;
        $categories     = $this->categoryRepo->getAllCategory($category_type, '', 'list'); //param [type_id, return type]
        $category_types = $this->categoryRepo->getAllCategoryType();

        return view('Category::category.upload', compact('categories', 'category_types'));
    }

    /**
     * Store a newly created category
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function uploadCategory(Request $request)
    {
        // return $request->all();
        try {
            //save data in database
            $saved = $this->categoryRepo->uploadCategory($request);

            if($saved == 1)
            {
                return $this->common->redirectWithAlert(
                    'category.upload', 
                    'success', 
                    'Done!.', 
                    'Category has been successfully added!.'
                );
            }
            else
            {
                throw new \Exception("something went wrong!.");
            }
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'category.upload', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    public function store(CategoryRequest $request)
    {
        try {
            //save data in database
            $saved = $this->categoryRepo->saveCategory($request);

            if(count($saved) > 0)
            {
                return $this->common->redirectWithAlert(
                    'category.create', 
                    'success', 
                    'Done!.', 
                    'Category has been successfully added!.'
                );
            }
            else
            {
                throw new \Exception("something went wrong!.");
            }
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'category.create', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    /**
     * Display the specified category.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        try {
            //params [id, keyword, page count]
            $category = $this->categoryRepo->getCategories($id, '', '');

            if(count($category) > 0)
            {
                return view('Category::category.show', compact('category'));
            }
            else
            {
                throw new \Exception('category not found!.');
            }   
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'category.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    /**
     * Show the form for editing the category.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            $category_type_id  = 1;
            $category_info  = $this->categoryRepo->getCategories($id, '', '');

            if(count($category_info) > 0)
            {
                $category_type_id = $category_info->category_type_id;
            }

            $categories     = $this->categoryRepo->getCategoryHierarchyListByTypeId($category_type_id);
            // $filters        = $this->categoryRepo->getAllFilter();
            $category_types = $this->categoryRepo->getAllCategoryType();

            if(count($category_info) > 0)
            {
                return view('Category::category.edit', compact('categories', 'category_types', 'id', 'category_info'));   
            }
            else
            {
                throw new \Exception('data not found for this id: '.$id);
            }
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'category.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    public function getCategoryHierarchy(Request $request){
        return $this->categoryRepo->getCategoryHierarchyList($request);
    }

    /**
     * Update the specified category.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            $saved = $this->categoryLogic->updateCategory($id, $request);

            if($saved)
            {
                return $this->common->redirectWithAlert(
                    'category.index', 
                    'success', 
                    'Done!.', 
                    'Category has been successfully updated!.'
                );
            }
            else
            {
                throw new \Exception("something went wrong!.");
            }
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'category.index', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    /**
     * delete the specified category from db.
     *
     * @param  int  $id
     *
     * @return true or false
     */
    public function destroy($id)
    {
        try {
            $delete = $this->categoryRepo->deleteCategory($id);
            if($delete)
            {
                return $this->common->redirectWithAlert(
                    'category.index', 
                    'success', 
                    'Done!.', 
                    'Category has been successfully deleted!.'
                );    
            }
            else
            {
                throw new \Exception("message: something went wrong, category couldn't deleted!.");
            }
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'category.create', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }

    //load category by type
    public function load_category(Request $request)
    {
        try {
            $category_type_id = $request->input('category_type_id');
            if(!empty($category_type_id))
            {
                $categories = $this->categoryRepo->getAllCategory($category_type_id, '', 'get');

                if(count($categories) > 0)
                {
                    return ['success' => true, 'data' => $categories];
                }
                else
                {
                    return ['success' => false, 'data' => []];
                }
            }
            else
            {
                throw new \Exception('message: category type empty.');
            }
        } catch (\Exception $e) {
            return $this->common->redirectWithAlert(
                'category.create', 
                'error', 
                'Error!.', 
                $e->getMessage()
            );
        }
    }
    
}
