<?php


Route::group(array('prefix'=>'others', ['middleware' => ['auth']], 'module' => 'Others', 'namespace' => 'App\Modules\Others\Controllers'), function() {

    Route::get('newsletter', 'OthersController@index');
    
}); 