@extends('layouts.back_master') @section('title','Newsletter')
@section('css')
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Others
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{ url('/') }}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li>Others Management</li>
		<li class="active"><a href="{{ url('others/newsletter') }}">Newsletter</a></li>
	</ol>
</section>
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header ">
            <h3 class="box-title"><strong>Newsletter</strong></h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-sm-8 col-md-8">
                    
                </div><!-- col-md-8 -->
                <div class="col-sm-4 col-md-4">
                </div><!-- col-md-4 -->
            </div><!-- row -->
        </div>
    </div>  
</section>
@stop
@section('js')
<script type="text/javascript">
$(document).ready(function(){
   
});
</script>
@stop
