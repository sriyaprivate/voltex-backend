<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Customer Order - <strong>{{$records['order']['order_no']}}</strong></h3>        
    </div>
    <div class="box-body">
        <div class="col-md-12">
            
            <div class="row">
                <div class="col-md-6">
                    <div class="row gap">
                        <div class="col-md-3 font-weight-bold">Order ID</div>
                        <div class="col-md-1 font-weight-bold">:</div>
                        <div class="col-md-8"><strong>{{$records['order']['order_no']}}</strong></div>
                    </div>

                    <div class="row gap">
                        <div class="col-md-3 font-weight-bold">Order Date</div>
                        <div class="col-md-1 font-weight-bold">:</div>
                        <div class="col-md-8"><strong>{{$records['order']['order_date']}}</strong></div>
                    </div>
                    
                    <div class="row gap">
                        <div class="col-md-3 font-weight-bold">Delivery Type</div>
                        <div class="col-md-1 font-weight-bold">:</div>
                        <div class="col-md-8"><strong>{{$records['order']['deliveryType']->name}}</strong></div>
                    </div>
                </div>

                <div class="col-md-6">                            
                    <div class="row gap">
                        <div class="col-md-4 font-weight-bold">Shipping Address</div>
                        <div class="col-md-1 font-weight-bold">:</div>
                        <div class="col-md-7"><strong>{{$records['order']['delivery_address']}}</strong></div>
                    </div>
                    
                    <div class="row gap">
                        <div class="col-md-4 font-weight-bold">Billing Address</div>
                        <div class="col-md-1 font-weight-bold">:</div>
                        <div class="col-md-7"><strong>{{$records['order']['billing_address']}}</strong></div>
                    </div>

                    <div class="row gap">
                        <div class="col-md-4 font-weight-bold">Status</div>
                        <div class="col-md-1 font-weight-bold">:</div>
                        <div class="col-md-7">
                            @if($records['order']->status == 0)
                                <span class="badge badge-custome pending">PENDING</span>
                            @elseif($records['order']->status == 1)
                                <span class="badge badge-custome approve">APPROVED</span>
                            @elseif($records['order']->status == 2)
                                <span class="badge badge-custome rejected">REJECTED</span>
                            @elseif($records['order']->status == 3)
                                <span class="badge badge-custome preparing">PREPARING</span>
                            @elseif($records['order']->status == 4)
                                <span class="badge badge-custome ondelivery">PREPARED</span>
                            @elseif($records['order']->status == 5)
                                <span class="badge badge-custome ondelivery">ONDELIVERY</span>
                            @elseif($records['order']->status == 6)
                                <span class="badge badge-custome delivered">DELIVERED</span>
                            @elseif($records['order']->status == 7)
                                <span class="badge badge-custome delivered">PICKUPED</span>
                            @endif
                        </div>
                    </div>

                    <div class="row gap">
                        <div class="col-md-4 font-weight-bold">Delivered Date :</div>
                        <div class="col-md-1 font-weight-bold">:</div>
                        <div class="col-md-7"><strong>{{$records['order']['delivery_date']}}</strong></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <table class="table table-sm table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Product</th>
                            <th>ProductCode</th>
                            <th class="text-center">Qty.</th>
                            <th class="text-right">Unit Price</th>
                            <th class="text-right">Discounted Price(per unit)</th>
                            <th class="text-right">Amount(Without Discount)</th>
                            <th class="text-right">Amount(Discounted)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1;$total_dis=0;$total=0; ?>
                        @foreach ($records['details'] as $detail)
                            <tr>
                                <td class="text-center">{{$i}}</td>
                                <td>{{$detail['product']->display_name}}</td>
                                <td>{{$detail['product']->code}}</td>
                                <td class="text-center">{{$detail['qty']}}</td>
                                <td class="text-right">{{$detail['price']}}</td>
                                <td class="text-right">{{$detail['discounted_price']?:'-'}}</td>
                                
                                <?php $total+= $detail['qty']*$detail['price']; ?>
                                <?php $total_dis+= $detail['qty']*$detail['discounted_price']; ?>
                                
                                <td class="text-right"><?php echo number_format(($detail['qty']*$detail['price']),2); ?></td>
                                <td class="text-right"><?php echo number_format(($detail['qty']*$detail['discounted_price']),2); ?></td>
                            </tr>
                            <?php $i++; ?>    
                        @endforeach
                    </tbody>                        
                    <tfoot>
                        <tr>
                            <td colspan="7" class="text-right"><strong>{{number_format($total,2)}}</strong></td>
                            <td class="text-right"><strong>{{number_format($total_dis,2)}}</strong></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-8"></div>
                <div class="col-md-2" class="summery" style="text-align: right;border-bottom: 1px solid black;"><strong>Gross Amount</strong></div>
                <div class="col-md-2" class="summery" style="text-align: right;border-bottom: 1px solid black;"><strong>{{number_format($total,2)}}</strong></div>
            </div>

            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-8"></div>
                <div class="col-md-2" class="summery" style="text-align: right;border-bottom: 1px solid black;"><strong>Discount Amount</strong></div>
                <div class="col-md-2" class="summery" style="text-align: right;border-bottom: 1px solid black;"><strong> - {{number_format(($total-$total_dis),2)}}</strong></div>
            </div>

            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-8"></div>
                <div class="col-md-2" class="summery" style="text-align: right;border-bottom: 1px solid black;"><strong>Delivery Charges</strong></div>
                <div class="col-md-2" class="summery" style="text-align: right;border-bottom: 1px solid black;"><strong> + {{number_format($records['order']->delivery_charges,2)}}</strong></div>
            </div>

            <div class="row" style="margin-bottom: 5px">
                <div class="col-md-8"></div>
                <div class="col-md-2" class="summery-footer" style="text-align: right;border-bottom-style: double;"><strong>Total Amount</strong></div>
                <div class="col-md-2" class="summery-footer" style="text-align: right;border-bottom-style: double;"><strong>Rs. {{number_format(($records['order']->delivery_charges+$total_dis),2)}}</strong></div>
            </div>

            <div class="row">
                
                <div class="col-md-6">
                    <div class="row gap">
                        <div class="col-md-4 font-weight-bold">Payment Transaction : </div>
                        <div class="col-md-1 font-weight-bold">{{$records['order']['payment']['transaction']->transaction_no}}</div>
                        <div class="col-md-7"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    
                    <div class="row gap">
                        <div class="col-md-4 font-weight-bold">Dealer Address</div>
                        <div class="col-md-1 font-weight-bold">:</div>
                        <div class="col-md-8"></div>
                    </div>

                    <div class="row gap">
                        <div class="col-md-4 font-weight-bold">Dealer Contact</div>
                        <div class="col-md-1 font-weight-bold">:</div>
                        <div class="col-md-7"></div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>