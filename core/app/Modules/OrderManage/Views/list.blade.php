@extends('layouts.back_master') @section('title','List Customer Order')

@section('css')
    <style type="text/css">  
        .reject{color: white !important;background-color: red !important;}.open{color: white !important;background-color: green !important;}.pending{color: white !important;background-color: black !important;}.approved{color: white !important;background-color: green !important;}.rejected{color: white !important;background-color: red !important;}.preparing{color: white !important;background-color: sandybrown !important;}.prepared{color: white !important;background-color: olivedrab !important;}.ondelivery{color: white !important;background-color: darkorange !important;}.delivered{color: white !important;background-color: darkblue !important;}.pickuped{color: white !important;background-color: darkblue !important;} 

        .reject:disabled{color: white !important;background-color: gray !important;}.open:disabled{color: white !important;background-color: gray !important;}.pending:disabled{color: white !important;background-color: gray !important;}.approved:disabled{color: white !important;background-color: gray !important;}.rejected:disabled{color: white !important;background-color: gray !important;}.preparing:disabled{color: white !important;background-color: gray !important;}.prepared:disabled{color: white !important;background-color: gray !important;}.ondelivery:disabled{color: white !important;background-color: gray !important;}.delivered:disabled{color: white !important;background-color: gray !important;}.pickuped:disabled{color: white !important;background-color: gray !important;}    
    </style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Customer Order 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li class="active">Customer Order List</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">        
        <div class="box-body">
            <form action="{{ route('order.list') }}" method="get">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">Country</label>
                        <select class="form-control chosen" name="country">
                            <option value ="" @if($old->country == null) selected @endif>ALL</option>
                            @if(count($countries) > 0)
                                @foreach($countries as $country)
                                <option value="{{$country->id}}" @if(old('country') == $country->id) selected @endif>{{$country->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Order No</label>
                        <input type="text" name="order_no" class="form-control" placeholder="Search for..." value="{{ $old->order_no }}">
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Status</label>
                        <select class="form-control chosen" name="status">
                            <option value ="" @if($order_status === null) selected @endif>All</option>
                            <option value="{{NEWLY_CREATED}}" @if($order_status == NEWLY_CREATED) selected @endif>New</option>
                            <option value="{{APPROVE_PENDING}}" @if($order_status === APPROVE_PENDING) selected @endif>Pending Approval</option>
                            
                            <option value="{{CUSTOMER_CONFIRMED}}"@if($order_status == CUSTOMER_CONFIRMED) selected @endif>CONFIRMED</option>
                            <option value="{{CUSTOMER_REJECTED}}"@if($order_status == CUSTOMER_REJECTED) selected @endif>Rejected</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-default" style="margin-top:19px;"><i class="fa fa-search" style="padding-right: 16px;width: 20px;"></i>Search</button>
                        <a href="list" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" style="margin-top:19px;"><i class="fa fa-refresh" style="padding-right: 16px;width: 20px;"></i>Refresh</a>
                    </div>
                </div>
            </form>
        </div>
    </div> 

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Customer Order</h3>            
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered bordered table-striped table-condensed">
                    <thead>
                        <tr>
                            <th style="text-align:center" width="3%">#</th>
                            <th style="text-align:left">Country</th>
                            <th style="text-align:left">Order No</th>
                            <th style="text-align:left">Perform Invoice</th>
                            <th style="text-align:left">Customer</th>
                            <th style="text-align:right;">Amount</th>
                            <th style="text-align:right;">Total</th>
                            <th style="text-align:center">Order Date</th>
                            <th style="text-align:center" width="4%">Status</th>
                            <th style="text-align:center" colspan="3">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($records) > 0)
                        @foreach($records as $key => $item)
                            <tr>
                                <td style="text-align:center;">{{ (($records->currentPage()-1)*$records->perPage())+($key+1) }}</td>
                                <td style="text-align:left;">
                                    @if($item['country'])
                                        <img src="{{asset('assets/images/flags')}}/{{$item['country']->flag}}" style="width: 20px"> {{ $item['country']->name }} 
                                    @endif
                                </td>
                                <td>
                                    <a href="{{url('order/view')}}/{{$item['id']}}" title="View Detail" style="text-decoration: underline;">
                                        {{ $item->order_no }}
                                    </a>
                                </td>
                                <td>
                                    @if($item->performaInvoice)
                                        <a href="{{url('order/perform-invoice/view')}}/{{$item['performaInvoice']->id}}" title="View Perform Invoice" style="text-decoration: underline;">
                                            {{ $item->performaInvoice->pf_no }}
                                        </a>
                                    @else
                                        - 
                                    @endif
                                </td>
                                <td>
                                    @if($item['customer'])
                                        {{$item['customer']->username}}
                                        <br>
                                        <span style="color: #3c8dbc">{{$item['customer']->email}}</span>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style="text-align:right;">{{ number_format($item->amount,2) }}</td>
                                <td style="text-align:right;">{{ number_format(($item->amount),2) }}</td>
                                <td style="text-align:center">
                                    {{Carbon\Carbon::parse($item->order_date)->format('j M Y')}}
                                    <br>
                                    {{Carbon\Carbon::parse($item->order_date)->format('g:ia')}}
                                </td>
                                <td style="text-align:center">
                                    @if($item->status == NEWLY_CREATED)
                                        <span style="width: 100%" class="badge badge-custome pending">NEWLY_CREATED</span>
                                    @elseif($item->status == CUSTOMER_CONFIRMED)
                                        <span style="width: 100%" class="badge badge-custome approve">CUSTOMER_CONFIRMED</span>
                                    @elseif($item->status == CUSTOMER_REJECTED)
                                        <span style="width: 100%" class="badge badge-custome rejected">CUSTOMER_REJECTED</span>
                                    @elseif($item->status == APPROVE_PENDING)
                                        <span style="width: 100%" class="badge badge-custome preparing">APPROVE_PENDING</span>
                                    @endif
                                </td>
                                <td style="text-align:center;">
                                    <button class="btn btn-primary btn-xs" onclick="location.href='{{url('order/view')}}/{{$item['id']}}';" data-id="{{$item->id}}" title="View">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </button>
                                </td>
                                <td style="text-align:center;">
                                    @if($item->status == NEWLY_CREATED)
                                        <button class="btn btn-warning btn-xs create-performa" data-id="{{$item->id}}" title="Preparing">
                                            Send PI
                                        </button>
                                    @endif                                    
                                </td>
                                <td style="text-align:center;">
                                    @if($item->status == CUSTOMER_CONFIRMED)
                                        <button class="btn btn-success btn-xs create-sales-order" data-id="{{$item->id}}" title="Create Sales Order">
                                            Sales Order
                                        </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <td colspan="10">
                            <h2 align="center" class="lbl-gray">Data not found!</h2>
                        </td>
                    @endif
                    </tbody>
                </table>
                @if(count($records) > 0)
                <div class="pagination-wrapper pull-right"> {!! $records->appends($old->except('page'))->render() !!} </div>
                @endif
            </div>            
        </div>
    </div>  
</section>

@stop
@section('js')
    <script type="text/javascript">
        
        var title   = 'Confirm!.';

        $(document).ready(function() {

            $('.reject').click(function(){    
                var id = $(this).data('id');
                var message = 'Are you sure you want to reject this order ?.'

                $.confirm({
                    theme: 'material',
                    title: title,
                    content: message,
                    buttons: {
                        confirm: {
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function(){
                                ajaxRequest( '{{url('order/reject')}}', { 'id' : id }, 'post', successFunc);
                            }
                        },
                        cancel: function () {

                        }
                    }
                });
            });

            $('.open').click(function(){    
                var id = $(this).data('id');
                var message = 'Are you sure you want to re-open order ?.'

                $.confirm({
                    theme: 'material',
                    title: title,
                    content: message,
                    buttons: {
                        confirm: {
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function(){
                                ajaxRequest( '{{url('order/reopen')}}', { 'id' : id }, 'post', successFunc);
                            }
                        },
                        cancel: function () {

                        }
                    }
                });
            });

            $('.create-performa').click(function(){    
                var id = $(this).data('id');
                var message = 'Are you sure you want to create performa invoice ?.'

                $.confirm({
                    theme: 'material',
                    title: title,
                    content: message,
                    buttons: {
                        confirm: {
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function(){
                                ajaxRequest( '{{url('order/create-performa-invoice')}}', { 'id' : id }, 'post', successFunc);
                            }
                        },
                        cancel: function () {

                        }
                    }
                });
            });

            $('.prepared').click(function(){    
                var id = $(this).data('id');
                var message = 'Are you sure you want to complete preparation ?.'

                $.confirm({
                    theme: 'material',
                    title: title,
                    content: message,
                    buttons: {
                        confirm: {
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function(){
                                ajaxRequest( '{{url('order/prepared')}}', { 'id' : id }, 'post', successFunc);
                            }
                        },
                        cancel: function () {

                        }
                    }
                });
            });

            $('.ondelivery').click(function(){    
                var id = $(this).data('id');
                var message = 'Are you sure you want to start delivery ?.'

                $.confirm({
                    theme: 'material',
                    title: title,
                    content: message,
                    buttons: {
                        confirm: {
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function(){
                                ajaxRequest( '{{url('order/onDelivery')}}', { 'id' : id }, 'post', successFunc);
                            }
                        },
                        cancel: function () {

                        }
                    }
                });
            });

            $('.delivered').click(function(){    
                var id = $(this).data('id');
                var message = 'Are you sure you order delivered ?.'

                $.confirm({
                    theme: 'material',
                    title: title,
                    content: message,
                    buttons: {
                        confirm: {
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function(){
                                ajaxRequest( '{{url('order/delivered')}}', { 'id' : id }, 'post', successFunc);
                            }
                        },
                        cancel: function () {

                        }
                    }
                });
            });

            $('.pickuped').click(function(){    
                var id = $(this).data('id');
                var message = 'Are you sure you order pickuped by customer ?.'

                $.confirm({
                    theme: 'material',
                    title: title,
                    content: message,
                    buttons: {
                        confirm: {
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function(){
                                ajaxRequest( '{{url('order/pickuped')}}', { 'id' : id }, 'post', successFunc);
                            }
                        },
                        cancel: function () {

                        }
                    }
                });
            });

            $('.create-sales-order').click(function(){    
                var id = $(this).data('id');

                $.ajax({
                    url: "{{URL::to('order/create-sales-order')}}/"+id,
                    method: 'POST',
                    cache: false,
                    success: function(response){
                        alert(response);
                        console.log(response);
                    },
                    error: function(xhr){
                        console.log(xhr);
                    } 
                });

            });

        });

        function successFunc(data){

            console.log(data);
            
            // if(data.title=='Done'){
            //     $.confirm({
            //         title: data.title,
            //         content: data.msg,
            //         type: 'green',
            //         typeAnimated: true,
            //         buttons: {
            //             close: function () {}
            //         }
            //     });
            //     window.location.reload();
            // }else{
            //     $.confirm({
            //         title: data.title,
            //         content: data.msg,
            //         type: 'red',
            //         typeAnimated: true,
            //         buttons: {
            //             close: function () {}
            //         }
            //     });
            // }
        }
    </script>
@stop


































