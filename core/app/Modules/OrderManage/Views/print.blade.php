<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<style>
    table{
        font-size:8px;
    }
    
    .full-border{
        border: 1px solid #000;
    }
    
    .border{
        border-color: #000;
        border-style: solid;
    }
    .price{
        text-align: center;
    }
</style>
<body>
<img src="{{url('assets/images/invoice-header.jpg')}}">
<div style="font-size: 8px;text-align: left;">No. 49, Sri Jinarathana Road, Colombo 02. Tel: 0117 445 717 Email: ostore@orelcorp.com Web: www.ostore.lk</div>
<div>
    <table>
        <tr>
            <td style="text-align:left">
                <strong>Order ID : 
                    @if(count($records['order']) > 0)
                        {{$records['order']->order_no}}
                    @else
                        - 
                    @endif
                </strong>
            </td>
             @if(count($records['order']) > 0 && count($records['order']->deliveryType) > 0 && !empty($records['order']->deliveryType->name))
                @if(strtolower($records['order']->deliveryType->name) != PICKUP)
                    <td style="text-align:left">
                        <strong>Shipping Address : 
                            @if(count($records['order']) > 0)
                                {{$records['order']->delivery_address}}
                            @else
                                - 
                            @endif
                        </strong>
                    </td>
                @else
                    <td></td>
                @endif
            @else
                <td></td>
            @endif
        </tr>
        <tr>
            <td style="text-align:left">
                <strong>Customer : 
                    @if(count($records['order']) > 0)
                        {{!empty($records['order']->customer->first_name)?$records['order']->customer->first_name:'-'}} {{!empty($records['order']->customer->last_name)?$records['order']->customer->last_name:''}}
                    @else
                        - 
                    @endif
                </strong>
            </td>
            <td style="text-align:left">
                <strong>Email : 
                    @if(count($records['order']) > 0)
                        {{!empty($records['order']->customer->email)?$records['order']->customer->email:'-'}}
                    @else
                        - 
                    @endif
                </strong>
            </td>
        </tr>
        <tr>
            <td style="text-align:left">
                <strong>Order Date : 
                    @if(count($records['order']) > 0)
                        {{$records['order']->order_date}}
                    @else
                        - 
                    @endif
                </strong>
            </td>
            <td style="text-align:left">
                <strong>Billing Address : 
                    @if(count($records['order']) > 0)
                        {{$records['order']->billing_address}}
                    @else
                        - 
                    @endif
                </strong>
            </td>
        </tr>
        <tr>
            <td style="text-align:left">
                <strong>Transaction No : 
                    @if(count($records['order']) > 0 && $records['order']['payment']&& $records['order']['payment']['transaction'])
                        {{$records['order']['payment']['transaction']->transaction_no}}
                    @else
                        - 
                    @endif
                </strong>
            </td>
            <td style="text-align:left">
                <strong>Status : 
                    @if(count($records['order']) > 0)
                        @if($records['order']->status == 0)
                            <span class="badge badge-custome pending">PENDING</span>
                        @elseif($records['order']->status == 1)
                            <span class="badge badge-custome approve">PAID</span>
                        @elseif($records['order']->status == 2)
                            <span class="badge badge-custome rejected">REJECTED</span>
                        @elseif($records['order']->status == 3)
                            <span class="badge badge-custome preparing">PREPARING</span>
                        @elseif($records['order']->status == 4)
                            <span class="badge badge-custome ondelivery">PREPARED</span>
                        @elseif($records['order']->status == 5)
                            <span class="badge badge-custome ondelivery">ONDELIVERY</span>
                        @elseif($records['order']->status == 6)
                            <span class="badge badge-custome delivered">DELIVERED</span>
                        @elseif($records['order']->status == 7)
                            <span class="badge badge-custome delivered">PICKUPED</span>
                        @endif
                    @else
                        -
                    @endif
                </strong>
            </td>
        </tr>
        <tr>
            <td style="text-align:left">
                @if(count($records['order']) > 0 && count($records['order']['deliveryType']) > 0 && !empty($records['order']['deliveryType']->name))
                <strong>Delivery Type : {{$records['order']['deliveryType']->name}}</strong>
                @else
                <strong>Delivery Type : {{'-'}}
                @endif
            </td>
            <td style="text-align:left">
                @if(count($records['order']) > 0 && count($records['order']->deliveryType) > 0 && !empty($records['order']->deliveryType->name))
                    @if(strtolower($records['order']->deliveryType->name) == PICKUP)
                        <strong>Pickup Date : 
                            @if(!empty($records['order']->delivery_date))
                                {{$records['order']->delivery_date}}
                            @else
                                {{'-'}} 
                            @endif
                        </strong>
                    @else
                        <strong>Delivered Date : 
                            @if(!empty($records['order']->delivery_date))
                                {{$records['order']->delivery_date}}
                            @else
                                {{'-'}}
                            @endif
                        </strong>
                    @endif

                @endif
            </td>
        </tr>
        <tr><td colspan="2" style="height: 20%"></td></tr>
        @if(count($records['order']) > 0 && count($records['order']->deliveryType) > 0 && !empty($records['order']->deliveryType->name))
            @if(strtolower($records['order']->deliveryType->name) == PICKUP)
            <tr>
                <td colspan="2" style="text-align:left">
                    <strong>Regional Centre : @if(count($records['order']->location) > 0 && !empty($records['order']->location->contact_no)) {{$records['order']->location->name}} @else {{'-'}} @endif</strong>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:left">
                    <strong>Pickup Address : @if(count($records['order']->location) > 0 && !empty($records['order']->location->address)) {{$records['order']->location->address}} @else {{'-'}} @endif</strong>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:left">
                    <strong>Contact No : @if(count($records['order']->location) > 0 && !empty($records['order']->location->contact_no)) {{$records['order']->location->contact_no}} @else {{'-'}} @endif</strong>
                </td>
            </tr>
            @endif
        @endif
    </table>
</div>
<br/>
<table class="full-border">
    <tr style="background-color:#000;line-height: 3">
        <th style="color:#fff;width: 5%">#</th>
        <th style="color:#fff;">Code</th>
        <th style="color:#fff;width:25%">Item</th>
        <th style="color:#fff;">Quantity</th>
        <th style="color:#fff;">Unit Price (Rs.)</th>
        <th style="color:#fff;">Discount (Rs.)</th>
        <th style="color:#fff;">Discounted Price</th>
    </tr>
    <?php 
        $i  = 1;
        $total_discount = 0;
        $total_gross_amount = 0;
        $total_amount = 0;
    ?>
    @foreach ($records['details'] as $detail)
        <?php $pro_dis_price; ?>
        @if($detail['discounted_price'] > 0)
            <?php 
            $pro_dis_price = $detail['price'] - (($detail['price'] * $detail['discounted_price'])/100); 
            ?>
        @endif
        <tr style="line-height: 2">
            <td>{{$i}}</td>
            <td class="text-right">{{$detail['product']->code}}</td>
            <td>{{$detail['product']->display_name}}</td>
            <td>{{$detail['qty']}}</td>
            <td class="price">{{number_format($detail['price'],2)}}</td>
            <td class="price">
                @if($detail['discounted_price'] > 0)
                    {{$detail['discounted_price']}}
                @else
                    -
                @endif
            </td>

            <td class="price">
                @if($detail['discounted_price'] > 0)
                    <?php 
                    $discounted_item_price = $detail['price'] - $detail['discounted_price'] 
                    ?>
                    {{number_format($discounted_item_price,2)}}
                @else
                    {{number_format(number_format($detail['price'],2))}}
                @endif
            </td>
            
            <?php 
                $total_gross_amount += $detail['qty'] * $detail['price']; 
            ?>

            @if($detail['discounted_price'] > 0)
                <?php $total_discount += $detail['qty'] * $detail['discounted_price'] ?>
            @endif
        </tr>
        <?php $i++; ?>    
    @endforeach
    <?php $total_amount +=  ($total_gross_amount - $total_discount) + $records['order']->delivery_charges ?>
</table>

<div></div>
<br/>
<table class="">
    <tr style="line-height: 2">
        <td colspan="7" style="text-align:right"><strong>Gross Amount (Rs.)</strong></td>
        <td style="text-align:right"><strong>{{number_format($total_gross_amount,2)}}</strong></td>
    </tr>
    <tr style="line-height: 2">
        <td colspan="7" style="text-align:right"><strong>Total Discount (Rs.)</strong></td>
        <td style="text-align:right"><strong> - {{number_format($total_discount,2)}}</strong></td>
    </tr>
    <tr style="line-height: 2">
        <td colspan="7" style="text-align:right"><strong>Delivery Charges (Rs.)</strong></td>
        <td style="text-align:right"><strong> + {{number_format($records['order']->delivery_charges,2)}}</strong></td>
    </tr>
    <tr style="line-height: 2">
        <td colspan="7" style="text-align:right;"><strong>Total Amount (Rs.)</strong></td>
        <td style="text-align:right;border-bottom-style:double;border-top-style:solid;"><strong> Rs. {{number_format($total_amount,2)}}</strong></td>
    </tr>
</table>
</body>
</html>
<!-- <img style="bottom:0;" src="{{url('assets/images/invoice-footer.jpg')}}"> -->