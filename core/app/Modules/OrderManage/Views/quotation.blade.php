<style>
    table{
        font-size:8px;
    }
    
    .full-border{
        border: 1px solid #000;
    }
    
    .border{
        border-color: #000;
        border-style: solid;
    }
</style>

<img src="{{url(assets/imaged/invoice-header.jpg)}}">
<span style="font-size: 7px;">No.34,Old Road,Nawinna,Maharagama. Tel: +94 11 4792 100 Fax: +94 11 4792 128 Email: sales@orelpower.com Web: www.orelpower.com VAT Reg.No: 114721255 7000</span>
<div></div>

<table>
    <tr>
        <td class="full-border" style="width:49%;font-size:9px;line-height:2;text-align:center">CUSTOMER DETAILS</td>
        <td style="width:2%;"></td>
        <td class="full-border" style="width:49%;font-size:9px;line-height:2;text-align:center">PAYMENT & BANK DETAILS</td>
    </tr>
    <tr>
        <td style="border-left-width:1px;border-right-width:1px;">
            Customer: 
        </td>
        <td></td>
        <td style="border-left-width:1px;border-right-width:1px">
            Payment Term :
            <p style="font-size:6px;">
                
            </p>
            Bank Name<br />
            Bank A/C
        </td>
    </tr>
  
    <tr>
        <td style="border-left-width:1px;border-right-width:1px;">
            Remarks : 
        </td>
        <td></td>
        <td style="border-left-width:1px;border-right-width:1px"></td>
    </tr>
    <tr>
        <td style="border-left-width:1px;border-right-width:1px;border-bottom-width:1px">
            Sales Executive: 
        </td>
        <td></td>
        <td style="border-left-width:1px;border-right-width:1px;border-bottom-width:1px;font-size:7px;">‘Items are non-returnable, unless otherwise due to a manufacturing defect.’</td>
    </tr>
</table>

<div></div>

<table class="full-border">
    <tr style="background-color:#000;line-height:2;">
        <th style="color:#fff;text-align:center;">Proforma No</th>
        <th style="color:#fff;text-align:center;">Proforma Date</th>
        <th style="color:#fff;text-align:center;">Expire Date</th>
        <th style="color:#fff;text-align:center;">Delivery Term</th>
    </tr>
    <tr>
        <td style="border-right-width:1px;text-align:center">5454</td>
        <td style="border-right-width:1px;text-align:center">2017-05-25</td>
        <td style="border-right-width:1px;text-align:center">7-Jun-2016</td>
        <td style="text-align:center"></td>
    </tr>
</table>

<div></div>

<table class="full-border">
    <tr style="background-color:#000;line-height: 2;">
        <th style="width:5%;color:#fff;text-align: center;">#</th>
        <th style="width:15%;color:#fff;text-align: center;">Modal No</th>
        <th style="width:40%;color:#fff;text-align: center;">Description</th>
        <th style="width:5%;color:#fff;text-align: center;">Qty.</th>
        <th style="width:15%;color:#fff;text-align: center;">Price (Rs.)</th>
        <th style="width:20%;color:#fff;text-align: center;">Amount (Rs.)</th>
    </tr>
    
    <tr>
        <td>1</td>
        <td>sdf</td>
        <td>dfgdfg</td>
        <td>dfgdfg</td>
        <td>dfgdfg</td>
        <td>dfgdfg</td>
    </tr>
</table>

<div><br /></div>


<span style="font-size: 8px;text-align:center;">
  Conditions :- Prevailing statutory levies will be applicable at the time of invoicing and prices will vary subject to USD fluctuations
</span>

<div></div>

<span style="font-size: 8px;text-align:center;">
  Should you require further information, please call 0114 792221.<br />
Whilst assuaring you of our best services at all times, we look forward your most valued order.
</span>

<div><br /></div>

<img style="bottom:0;" src="{{url(assets/imaged/invoice-footer.jpg)}}">