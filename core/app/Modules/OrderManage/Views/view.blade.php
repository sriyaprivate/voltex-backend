@extends('layouts.back_master') @section('title','View Customer Order')

@section('css')
    <style type="text/css">  
        .gap{
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .reject{color: white !important;background-color: red !important;}.open{color: white !important;background-color: green !important;}.pending{color: white !important;background-color: black !important;}.approved{color: white !important;background-color: green !important;}.rejected{color: white !important;background-color: red !important;}.preparing{color: white !important;background-color: sandybrown !important;}.prepared{color: white !important;background-color: olivedrab !important;}.ondelivery{color: white !important;background-color: darkorange !important;}.delivered{color: white !important;background-color: darkblue !important;}.pickuped{color: white !important;background-color: darkblue !important;}

        .summery{
            text-align: right;
            padding: 5px;
            
        }
        .summery-footer{
            text-align: right;
            padding: 5px;
            border-bottom-style: double;
        }

    </style>
@stop

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Customer Order 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li><a href="{{{url('order/list')}}}"></i>Customer Order List</a></li>
        <li class="active">Customer Order Detail View</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Customer Order Detail - <strong>{{$records['order']['order_no']}}</strong></h3>
            <div class="pull-right">
                <a class="btn btn-default btn-sm float-right" target="_blank" href="{{url('order/print')}}/{{$records['order']->id}}"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
            </div>
        </div>
        <div class="box-body">
            <div class="col-md-12">
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="row gap">
                            <div class="col-md-4 font-weight-bold">Order ID</div>
                            <div class="col-md-1 font-weight-bold">:</div>
                            <div class="col-md-7"><strong>{{$records['order']['order_no']}}</strong></div>
                        </div>
                        <div class="row gap">
                            <div class="col-md-4 font-weight-bold">Order Date</div>
                            <div class="col-md-1 font-weight-bold">:</div>
                            <div class="col-md-7"><strong>{{$records['order']['order_date']}}</strong></div>
                        </div>
                        <div class="row gap">
                            <div class="col-md-4 font-weight-bold">Customer</div>
                            <div class="col-md-1 font-weight-bold">:</div>
                            <div class="col-md-7">
                                <strong>
                                    {{!empty($records['order']->customer->username)?$records['order']->customer->username:'-'}}
                                </strong>
                            </div>
                        </div>
                        <div class="row gap">
                            <div class="col-md-4 font-weight-bold">Contact No</div>
                            <div class="col-md-1 font-weight-bold">:</div>
                            <div class="col-md-7"><strong>{{!empty($records['order']->customer->phone_no)?$records['order']->customer->email:'-'}}</strong></div>
                        </div>
                        <div class="row gap">
                            <div class="col-md-4 font-weight-bold">Billing Address</div>
                            <div class="col-md-1 font-weight-bold">:</div>
                            <div class="col-md-7"><strong>{{$records['order']['billing_address']}}</strong></div>
                        </div>
                        
                        <br/>
                        @if(count($records['order']) > 0 && 
                        count($records['order']->deliveryType) > 0 && !empty($records['order']->deliveryType->name))
                            @if(strtolower($records['order']->deliveryType->name) == PICKUP)
                                <div class="row gap">
                                    <div class="col-md-4 font-weight-bold">Regional Centre :</div>
                                    <div class="col-md-1 font-weight-bold">:</div>
                                    <div class="col-md-7"><strong>{{count($records['order']->location) > 0?$records['order']->location->name:'-'}}</strong></div>
                                </div> 
                                <div class="row gap">
                                    <div class="col-md-4 font-weight-bold">Pickup Address :</div>
                                    <div class="col-md-1 font-weight-bold">:</div>
                                    <div class="col-md-7"><strong>{{count($records['order']->location) > 0?$records['order']->location->address:'-'}}</strong></div>
                                </div>
                                <div class="row gap">
                                    <div class="col-md-4 font-weight-bold">Contact No :</div>
                                    <div class="col-md-1 font-weight-bold">:</div>
                                    <div class="col-md-7"><strong>{{count($records['order']->location) > 0?$records['order']->location->contact_no:'-'}}</strong></div>
                                </div> 
                            @endif   
                        @endif
                    </div>
                    <div class="col-md-6">
                        @if(count($records['order']) > 0 && 
                        count($records['order']->deliveryType) > 0 && !empty($records['order']->deliveryType->name))
                            @if(strtolower($records['order']->deliveryType->name) != PICKUP)                            
                            <div class="row gap">
                                <div class="col-md-4 font-weight-bold">Shipping Address</div>
                                <div class="col-md-1 font-weight-bold">:</div>
                                <div class="col-md-7"><strong>{{$records['order']['delivery_address']}}</strong></div>
                            </div>
                            @endif
                        @endif
                        <div class="row gap">
                            <div class="col-md-4 font-weight-bold">Payment Transaction</div>
                            <div class="col-md-1 font-weight-bold">:</div>
                            <div class="col-md-7"><strong>
                            @if($records['order']['payment'] && $records['order']['payment']['transaction'])
                                {{$records['order']['payment']['transaction']->transaction_no}}
                            @else
                                {{'-'}}
                            @endif</strong>
                            </div>
                        </div>
                        <div class="row gap">
                            <div class="col-md-4 font-weight-bold">Status</div>
                            <div class="col-md-1 font-weight-bold">:</div>
                            <div class="col-md-7">
                                @if($records['order']->status == NEWLY_CREATED)
                                    <span class="badge badge-custome pending">NEWLY_CREATED</span>
                                @elseif($records['order']->status == CUSTOMER_CONFIRMED)
                                    <span class="badge badge-custome approve">CUSTOMER_CONFIRMED</span>
                                @elseif($records['order']->status == CUSTOMER_REJECTED)
                                    <span class="badge badge-custome rejected">CUSTOMER_REJECTED</span>
                                @elseif($records['order']->status == APPROVE_PENDING)
                                    <span class="badge badge-custome preparing">APPROVE_PENDING</span>
                                @endif
                            </div>
                        </div>
                        <div class="row gap">
                            <div class="col-md-4 font-weight-bold">Email</div>
                            <div class="col-md-1 font-weight-bold">:</div>
                            <div class="col-md-7"><strong>{{!empty($records['order']->customer->email)?$records['order']->customer->email:'-'}}</strong></div>
                        </div>
                        @if(count($records['order']) > 0 && 
                        count($records['order']->deliveryType) > 0 && !empty($records['order']->deliveryType->name))
                            @if(strtolower($records['order']->deliveryType->name) == PICKUP)
                            <div class="row gap">
                                <div class="col-md-4 font-weight-bold">Pickup Date :</div>
                                <div class="col-md-1 font-weight-bold">:</div>
                                <div class="col-md-7"><strong>{{!empty($records['order']['delivery_date'])?$records['order']['delivery_date']:'-'}}</strong></div>
                            </div>
                            @else
                                <div class="row gap">
                                <div class="col-md-4 font-weight-bold">Delivered Date :</div>
                                <div class="col-md-1 font-weight-bold">:</div>
                                <div class="col-md-7"><strong>{{!empty($records['order']['delivery_date'])?$records['order']['delivery_date']:'-'}}</strong></div>
                            </div>
                            @endif
                        @endif
                    </div>
                </div>
                <br/>
                <br/>
                <div class="row">
                    <table class="table table-sm table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>ProductCode</th>
                                <th>Product</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-right">Unit Price</th>
                                <th class="text-right">Discount</th>
                                <th class="text-right">Discounted Price(Per unit)</th>
                                <th class="text-right">Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $i = 1;
                                $total_discount = 0;
                                $total_gross_amount = 0;
                                $total_amount = 0;
                            ?>
                            @foreach ($records['details'] as $detail)
                                <?php $pro_dis_price; ?>
                                @if($detail['discounted_price'] > 0)
                                    <?php 
                                    $pro_dis_price = $detail['price'] - (($detail['price'] * $detail['discounted_price'])/100); 
                                    ?>
                                @endif

                                <tr>
                                    <td class="text-center">{{$i}}</td>
                                    <td>
                                        <a href="{{url('product/view')}}/{{$detail['product']->id}}">{{$detail['product']->code}}</a>
                                    </td>
                                    <td>
                                        <a href="{{url('product/view')}}/{{$detail['product']->id}}">
                                        {{!empty($detail['product']->display_name)?$detail['product']->display_name:$detail['product']->name}}
                                        </a>
                                    </td>
                                    <td class="text-center">{{$detail['qty']}}</td>
                                    <td class="text-right">{{number_format($detail['price'],2)}}</td>
                                    <td class="text-right">
                                        @if($detail['discounted_price'] > 0)
                                            {{number_format($detail['discounted_price'],2)}}
                                        @else
                                            -
                                        @endif
                                    </td>

                                    <td class="text-right">
                                        @if($detail['discounted_price'] > 0)
                                            <?php 
                                            $discounted_item_price = $detail['price'] - $detail['discounted_price'] 
                                            ?>
                                            {{number_format($discounted_item_price,2)}}
                                        @else
                                            {{number_format(number_format($detail['price'],2))}}
                                        @endif
                                    </td>
                                    
                                    <?php 
                                        $total_gross_amount += $detail['qty'] * $detail['price'];  
                                    ?>

                                    @if($detail['discounted_price'] > 0)
                                        <?php $total_discount += $detail['qty'] * $detail['discounted_price'] ?>
                                    @endif
                                    
                                    <td class="text-right">
                                        @if($detail['discounted_price'] > 0)
                                           <?php $row_total = $detail['qty'] * $discounted_item_price ?>
                                           {{number_format($row_total,2)}}
                                        @else
                                            <?php $row_total = $detail['qty'] * $detail['price'] ?>
                                            {{number_format($row_total,2)}}
                                        @endif
                                    </td>
                                </tr>
                                <?php $i++; ?>    
                            @endforeach
                            <?php $total_amount +=  ($total_gross_amount - $total_discount) + $records['order']->delivery_charges ?>
                        </tbody>                        
                    </table>
                </div>
                <div class="row" style="margin-bottom: 5px">
                    <div class="col-md-8"></div>
                    <div class="col-md-2" class="summery" style="text-align: right;"><strong>Gross Amount</strong></div>
                    <div class="col-md-2" class="summery" style="text-align: right;"><strong>Rs. {{number_format($total_gross_amount,2)}}</strong></div>
                </div>
                 <div class="row" style="margin-bottom: 5px">
                    <div class="col-md-8"></div>
                    <div class="col-md-2" class="summery" style="text-align: right;"><strong>Total Discount</strong></div>
                    <div class="col-md-2" class="summery" style="text-align: right;"><strong>Rs. {{number_format($total_discount,2)}}</strong></div>
                </div>
                <div class="row" style="margin-bottom: 5px">
                    <div class="col-md-8"></div>
                    <div class="col-md-2" class="summery" style="text-align: right;"><strong>Delivery Charges</strong></div>
                    <div class="col-md-2" class="summery" style="text-align: right;"><strong>Rs. {{number_format($records['order']->delivery_charges,2)}}</strong></div>
                </div>

                <div class="row" style="margin-bottom: 5px">
                    <div class="col-md-8"></div>
                    <div class="col-md-2" class="summery-footer" style="text-align: right;border-top: 1px solid black;border-bottom-style: double;"><strong>Total Amount</strong></div>
                    <div class="col-md-2" class="summery-footer" style="text-align: right;border-top: 1px solid black;border-bottom-style: double;"><strong>Rs. {{number_format($total_amount,2)}}</strong></div>
                </div>                

            </div>
        </div>
    </div>  
</section>



@stop
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('.delete-btn').click(function(){    
        var link    = $(this).data('url');
        var title   = 'Confirm!.';
        var message = 'Are you sure you want to delete this location?.'

        confirm_delete(link, title, message);
    });
});
</script>
@stop


































