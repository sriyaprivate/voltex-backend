@extends('layouts.back_master') @section('title','List Performa Invoice')

@section('css')
    <style type="text/css">  
        .reject{color: white !important;background-color: red !important;}.open{color: white !important;background-color: green !important;}.pending{color: white !important;background-color: black !important;}.approved{color: white !important;background-color: green !important;}.rejected{color: white !important;background-color: red !important;}.preparing{color: white !important;background-color: sandybrown !important;}.prepared{color: white !important;background-color: olivedrab !important;}.ondelivery{color: white !important;background-color: darkorange !important;}.delivered{color: white !important;background-color: darkblue !important;}.pickuped{color: white !important;background-color: darkblue !important;} 

        .reject:disabled{color: white !important;background-color: gray !important;}.open:disabled{color: white !important;background-color: gray !important;}.pending:disabled{color: white !important;background-color: gray !important;}.approved:disabled{color: white !important;background-color: gray !important;}.rejected:disabled{color: white !important;background-color: gray !important;}.preparing:disabled{color: white !important;background-color: gray !important;}.prepared:disabled{color: white !important;background-color: gray !important;}.ondelivery:disabled{color: white !important;background-color: gray !important;}.delivered:disabled{color: white !important;background-color: gray !important;}.pickuped:disabled{color: white !important;background-color: gray !important;}    
    </style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Customer Order 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li class="active">Performa Invoice List</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">        
        <div class="box-body">
            <form action="{{ route('order.performa.list') }}" method="get">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">Country</label>
                        <select class="form-control chosen" name="country">
                            <option value ="" @if($old->country === null) selected @endif>ALL</option>
                            <option value="{{1}}" @if($old->country == 1) selected @endif>CHINA</option>
                            <option value="{{2}}" @if($old->country === 2) selected @endif>INDIA</option>1
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Performa Inv. No</label>
                        <input type="text" name="pfi_no" class="form-control" placeholder="Search for..." value="{{ $old->pfi_no }}">
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">Order No</label>
                        <input type="text" name="order_no" class="form-control" placeholder="Search for..." value="{{ $old->order_no }}">
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">Status</label>
                        <select class="form-control chosen" name="status">
                            <option value ="" @if($order_status === null) selected @endif>ALL</option>
                            <option value="{{NEWLY_CREATED}}" @if($order_status == NEWLY_CREATED) selected @endif>NEWLY_CREATED</option>
                            <option value="{{APPROVE_PENDING}}" @if($order_status === APPROVE_PENDING) selected @endif>APPROVE_PENDING</option>
                            
                            <option value="{{CUSTOMER_CONFIRMED}}"@if($order_status == CUSTOMER_CONFIRMED) selected @endif>CUSTOMER_CONFIRMED</option>
                            <option value="{{CUSTOMER_REJECTED}}"@if($order_status == CUSTOMER_REJECTED) selected @endif>CUSTOMER_REJECTED</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-default" style="margin-top:19px;"><i class="fa fa-search" style="padding-right: 16px;width: 20px;"></i>Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div> 

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Customer Order</h3>            
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered bordered table-striped table-condensed">
                    <thead>
                        <tr>
                            <th style="text-align:center" width="3%">#</th>
                            <th style="text-align:left">Country</th>
                            <th style="text-align:left">Perform Invoice</th>
                            <th style="text-align:left">OrderID</th>
                            <th style="text-align:left">Customer</th>
                            <th style="text-align:right;">Amount</th>
                            <th style="text-align:center">Order Date</th>
                            <th style="text-align:center">Confirmed Date</th>
                            <th style="text-align:center" width="4%">Status</th>
                            <th style="text-align:center" colspan="1">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($records) > 0)
                        @foreach($records as $key => $item)
                            <tr>
                                <td style="text-align:center;">{{ (($records->currentPage()-1)*$records->perPage())+($key+1) }}</td>
                                <td style="text-align:center;">
                                    <img src="{{asset('assets/images/flags')}}/{{$item['order']['country']->flag}}" style="width: 20px"> {{ $item['order']['country']->name }} 
                                </td>
                                <td>
                                    <a href="{{url('order/perform-invoice/view')}}/{{$item->id}}" title="View Perform Invoice" style="text-decoration: underline;">
                                        {{ $item->pf_no }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{url('order/view')}}/{{$item['order_id']}}" title="View Detail" style="text-decoration: underline;">
                                        {{ $item['order']->order_no }}
                                    </a>
                                </td>
                                <td>
                                    @if($item['customer'])
                                        {{$item['customer']->username}}
                                        <br>
                                        <span style="color: #3c8dbc">{{$item['customer']->email}}</span>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style="text-align:right;">{{ number_format($item->amount,2) }}</td>
                                <td style="text-align:center">
                                    {{Carbon\Carbon::parse($item['order']->order_date)->format('j M Y')}}
                                    <br>
                                    {{Carbon\Carbon::parse($item['order']->order_date)->format('g:ia')}}
                                </td>
                                <td style="text-align:center">
                                    @if($item->confirmed_date)
                                        {{Carbon\Carbon::parse($item->confirmed_date)->format('j M Y')}}
                                        <br>
                                        {{Carbon\Carbon::parse($item->confirmed_date)->format('g:ia')}}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td style="text-align:center">
                                    @if($item->status == NEWLY_CREATED)
                                        <span class="badge badge-custome pending">NEWLY_CREATED</span>
                                    @elseif($item->status == CUSTOMER_CONFIRMED)
                                        <span class="badge badge-custome approve">CUSTOMER_CONFIRMED</span>
                                    @elseif($item->status == CUSTOMER_REJECTED)
                                        <span class="badge badge-custome rejected">CUSTOMER_REJECTED</span>
                                    @elseif($item->status == APPROVE_PENDING)
                                        <span class="badge badge-custome preparing">APPROVE_PENDING</span>
                                    @endif
                                </td>
                                <td style="text-align:center;">
                                    <button class="btn btn-primary btn-xs" onclick="location.href='{{url('order/performa-invoice/view')}}/{{$item['id']}}';" data-id="{{$item->id}}" title="View">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </button>
                                    @if($item->status == NEWLY_CREATED)
                                        <button class="btn btn-warning btn-xs create-performa" data-id="{{$item->id}}" title="Preparing">
                                            Send PI
                                        </button>
                                    @endif
                                    
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <td colspan="10">
                            <h2 align="center" class="lbl-gray">Data not found!</h2>
                        </td>
                    @endif
                    </tbody>
                </table>
                @if(count($records) > 0)
                <div class="pagination-wrapper pull-right"> {!! $records->appends($old->except('page'))->render() !!} </div>
                @endif
            </div>            
        </div>
    </div>  
</section>



@stop
@section('js')
    <script type="text/javascript">
        
        var title   = 'Confirm!.';

        $(document).ready(function() {

            $('.reject').click(function(){    
                var id = $(this).data('id');
                var message = 'Are you sure you want to reject this order ?.'

                $.confirm({
                    theme: 'material',
                    title: title,
                    content: message,
                    buttons: {
                        confirm: {
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function(){
                                ajaxRequest( '{{url('order/reject')}}', { 'id' : id }, 'post', successFunc);
                            }
                        },
                        cancel: function () {

                        }
                    }
                });
            });

            $('.create-performa').click(function(){    
                var id = $(this).data('id');
                var message = 'Are you sure you want to create performa invoice ?.'

                $.confirm({
                    theme: 'material',
                    title: title,
                    content: message,
                    buttons: {
                        confirm: {
                            btnClass: 'btn-blue',
                            keys: ['enter'],
                            action: function(){
                                ajaxRequest( '{{url('order/create-performa-invoice')}}', { 'id' : id }, 'post', successFunc);
                            }
                        },
                        cancel: function () {

                        }
                    }
                });
            });

        });

        function successFunc(data){

            console.log(data);
            
            if(data.title=='Done'){
                $.confirm({
                    title: data.title,
                    content: data.msg,
                    type: 'green',
                    typeAnimated: true,
                    buttons: {
                        close: function () {}
                    }
                });
                window.location.reload();
            }else{
                $.confirm({
                    title: data.title,
                    content: data.msg,
                    type: 'red',
                    typeAnimated: true,
                    buttons: {
                        close: function () {}
                    }
                });
            }
        }
    </script>
@stop


































