<?php namespace App\Modules\OrderManage\BusinessLogics;

use App\Modules\OrderManage\Repositories\OrderRepository;

/**
* Business Logics 
* Define all the busines logics in here
* @author Sriya <csriyarathne@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use App\Classes\OrderTrans;

use App\Modules\OrderManage\Models\Order;
use App\Modules\OrderManage\Models\OrderDetail;
use App\Modules\OrderManage\Models\PerformaInvoice;

use App\Exceptions\TransactionException;
use PhpSpec\Exception\Exception;
use DB;
use Sentinel;

class OrderLogic{

	protected $repository;

	public function __construct(OrderRepository $repository){
		$this->repository = $repository;
	}

	public function allOrder(){
		return $order = Order::with(['details','customer','user.customer','deliveryType'])->get();
	}

	public function filterRecords($data){
		$conditions = [];
		
		if($data->order_no != ''){
			$conditions['order_no'] = ['operator' => 'LIKE', 'value' => '%'.$data->order_no.'%'];
		}

		if($data->country != ''){
			$conditions['country_id'] = ['operator' => '=', 'value' => $data->country];
		}

		if($data->status != ''){
			$conditions['status'] = ['operator' => '=', 'value' => $data->status];
		}

		return $this->repository->getRecords($conditions, 'id','DESC');
	}

	public function details($data){
		
		$order = Order::where('id',$data)
					  ->with('customer','location')
					  ->first();

        $details = OrderDetail::where('order_id',$data)->with(['product' => function($sql){
        	$sql->select('id','code','display_name')
        		->whereNull('deleted_at')
        		->orWhereNotNull('deleted_at');
        }])->get();

        return ['order' => $order, 'details' => $details];
	}

	public function filterRecordsPerformaInvoices($data){
		$conditions = [];
		
		if($data->pfi_no != ''){
			$conditions['pf_no'] = ['operator' => 'LIKE', 'value' => '%'.$data->pfi_no.'%'];
		}

		if($data->order_no != ''){
			$conditions['order_no'] = ['operator' => 'LIKE', 'value' => '%'.$data->order_no.'%'];
		}

		if($data->country != ''){
			$conditions['country_id'] = ['operator' => '=', 'value' => $data->country_id];
		}

		if($data->status != ''){
			$conditions['status'] = ['operator' => '=', 'value' => $data->status];
		}

		return $this->repository->getPerformaInvoiceRecords($conditions, 'id','DESC');
	}

	public function performainvoiceDetails($data){
		
		$pfi = PerformaInvoice::where('id',$data)
					  ->with('order.customer','order.location')
					  ->first();

        $details = OrderDetail::where('order_id',$pfi->order_id)->with(['product' => function($sql){
        	$sql->select('id','code','display_name')
        		->whereNull('deleted_at')
        		->orWhereNotNull('deleted_at');
        }])->get();

        return ['pfi' => $pfi, 'details' => $details];
	}

	public function createPerformaInvoice($order_id){
		
		$order = Order::find($order_id);

		if($order){

			$_res_pf = PerformaInvoice::create([
				'order_id' 	=> $order->id,
				'pf_no' 	=> 'PFI_'.$order->id,
				'customer_id' 	=> $order->user_id,
				'user_id' 	=> Sentinel::getUser()->id,
				'status' 	=> APPROVE_PENDING
			]);

			if($_res_pf){
				$order->status = APPROVE_PENDING;
				$order->save();

				if(!OrderTrans::store($order_id,$order->status,'PerformaInvoice Created'.$_res_pf->pf_no)){
					throw new TransactionException("Tansaction Faild !", 100);
				}
			}else{
				throw new TransactionException("Cannot create performa invoice !", 102);
			}
			
		}else{
			throw new TransactionException("Invalid Order !", 101);
		}
	}

	public function reject($order_id){
		
		$order = Order::find($order_id);

		if($order){
			$order->status = REJECTED;
			$order->save();

			if(!OrderTrans::store($order_id,$order->status,'Order Rejected')){
				throw new TransactionException("Tansaction Faild !", 100);
			}
		}else{
			throw new TransactionException("Invalid Order !", 101);
		}
	}

	public function reopen($order_id){
		
		$order = Order::find($order_id);

		if($order){
			$order->status=PENDING;
			$order->save();

			if(!OrderTrans::store($order_id,$order->status,'Order Re-Opened')){
				throw new TransactionException("Tansaction Faild !", 100);
			}
		}else{
			throw new TransactionException("Invalid Order !", 101);
		}
	}

}
