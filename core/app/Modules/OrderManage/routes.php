<?php

Route::group(['middleware' => ['auth']], function()
{
    Route::group(array('prefix'=>'order','namespace' => 'App\Modules\OrderManage\Controllers'), function()
    {
        //GET Routes
        Route::get('list', ['as' => 'order.list', 'uses' => 'OrderManageController@listView']);

        Route::get('view/{id}', ['as' => 'order.view', 'uses' => 'OrderManageController@detailView']);

        Route::get('perform-invoice/list', ['as' => 'order.performa.list', 'uses' => 'OrderManageController@listPerformaInvoiceView']);

        Route::get('perform-invoice/view/{id}', ['as' => 'order.performa.view', 'uses' => 'OrderManageController@detailPerformaInvoiceView']);

        Route::get('print/{id}', ['as' => 'order.view', 'uses' => 'OrderManageController@printOrder']);

        Route::post('reject', ['as' => 'order.reject', 'uses' => 'OrderManageController@rejectOrder']);

        Route::post('reopen', ['as' => 'order.reopen', 'uses' => 'OrderManageController@reopenOrder']);

        Route::post('create-performa-invoice', ['as' => 'order.performa.invoice', 'uses' => 'OrderManageController@createPerformaInvoice']);

        Route::post('create-sales-order/{id}', ['as' => 'order.view', 'uses' => 'OrderManageController@createSalesOrder']);   
        
    });
});