<?php 
namespace App\Modules\OrderManage\Controllers;

/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Classes\PdfTemplate;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Classes\Functions;

use App\Modules\OrderManage\BusinessLogics\OrderLogic;
use App\Modules\OrderManage\Repositories\OrderRepository;
use App\Models\Country;

use App\Exceptions\TransactionException;
use PhpSpec\Exception\Exception;
use DB;
use PDF;

class OrderManageController extends Controller {

	protected $orderLogic;
	protected $repository;
	protected $common;

	public function __construct(OrderLogic $orderLogic, OrderRepository $repository,Functions $common){
		$this->orderLogic = $orderLogic;
		$this->repository = $repository;
		$this->common 	  = $common;
	}

	public function listView(Request $request)
	{
		// return $request->all();
		try{
			$records = $this->orderLogic->filterRecords($request);
		}catch(\Exception $e){
			$records = [];
		}

		if($request->get('status') === null){
			$status = CUSTOMER_CONFIRMED;
		}else{
			$status = $request->get('status');
		}

		$countries = Country::whereNull('deleted_at')->select(['name','id'])->get();

		return view('OrderManage::list')->with([
			'old' 	  		=> $request, 
			'records' 		=> $records,
			'countries' 	=> $countries,
			'order_status'  => $status
		]);
	}

	public function detailView($id)
	{
		try{
			$records = $this->orderLogic->details($id);
			if(count($records['order']) > 0){
				return view('OrderManage::view')->with(['records' => $records]);
			}else{
				return $this->common->redirectWithAlert(
	                'order/list', 
	                'warning', 
	                'Notice!.', 
	                'Order not found !',
	                'url'
            	); 
			}
		}catch(\Exception $e){
        	return $this->common->redirectWithAlert(
                'order/list', 
                'error', 
                'Notice!.', 
                $e->getMessage(),
                'url'
            ); 
		}
		
	}

	public function listPerformaInvoiceView(Request $request)
	{
		try{
			$records = $this->orderLogic->filterRecordsPerformaInvoices($request);
		}catch(\Exception $e){
			$records = [];
		}

		if($request->get('status') === null){
			$status = CUSTOMER_CONFIRMED;
		}else{
			$status = $request->get('status');
		}

		// return $records;

		return view('OrderManage::performa-invoice-list')->with([
			'old' 	  		=> $request, 
			'records' 		=> $records,
			'order_status'  => $status
		]);
	}

	public function detailPerformaInvoiceView($id)
	{
		try{
			$records = $this->orderLogic->performainvoiceDetails($id);
			if(count($records['pfi']) > 0){
				return view('OrderManage::performa-invoice-view')->with(['records' => $records]);
			}else{
				return $this->common->redirectWithAlert(
	                'order/list', 
	                'warning', 
	                'Notice!.', 
	                'Order not found !',
	                'url'
            	); 
			}
		}catch(\Exception $e){
        	return $this->common->redirectWithAlert(
                'order/list', 
                'error', 
                'Notice!.', 
                $e->getMessage(),
                'url'
            ); 
		}
		
	}

	public function printOrder($id)
	{
		$records = $this->orderLogic->details($id);
		if(count($records['order']) == 0){
			return $this->common->redirectWithAlert(
                'order/list', 
                'warning', 
                'Notice!.', 
                'Order not found !',
                'url'
        	); 
		}	

		$page1 =  view('OrderManage::print')->with(['records'=>$records])->render();

		$pdf   = new PdfTemplate(["no"=>date('Y-m-d')]);
        $pdf->SetMargins(5, 0, 5);
        $pdf->SetTitle("CUSTOMER_ORDER_".$records['order']->order_no."_".date('Y_m_d'));
        $pdf->SetFont('helvetica', 5);
        $pdf->SetAutoPageBreak(TRUE, 30);
        $pdf->AddPage();
        $pdf->writeHtml($page1);
        $pdf->output($records['order']->order_no."_".date('Y_m_d').".pdf", 'I');
	}

	public function createPerformaInvoice(Request $request)
	{
		try{
			DB::transaction(function () use ($request) {

				$this->orderLogic->createPerformaInvoice($request->get('id'));

			});
			
			return ['title'=>'Done','msg'=>'Performa Invoice Created !','type'=>'success'];

		} catch (TransactionException $e) {
            return ['title'=>'Faild','msg'=>$e,'type'=>'error'];
        } catch(Exception $e){
			return ['title'=>'Faild','msg'=>$e,'type'=>'error'];
		}

	}

	public function rejectOrder(Request $request)
	{
		try{
			DB::transaction(function () use ($request) {
				$this->orderLogic->reject($request->get('id'));
			});
			
			return ['title'=>'Done','msg'=>'Order Rejected !','type'=>'success'];

		} catch (TransactionException $e) {
            return ['title'=>'Faild','msg'=>$e,'type'=>'error'];
        } catch(Exception $e){
			return ['title'=>'Faild','msg'=>$e,'type'=>'error'];
		}

	}

	public function reopenOrder(Request $request)
	{

		try{
			DB::transaction(function () use ($request) {
				$this->orderLogic->reopen($request->get('id'));				
			});
			
			return ['title'=>'Done','msg'=>'Order Re-Opened !','type'=>'success'];

		} catch (TransactionException $e) {
            return ['title'=>'Faild','msg'=>$e,'type'=>'error'];
        } catch(Exception $e){
			return ['title'=>'Faild','msg'=>$e,'type'=>'error'];
		}

	}

	public function createSalesOrder($id)
	{

		try
		{
			$dataset = [];

			$client = new \GuzzleHttp\Client();

		    $_odoo = $client->post('http://164.132.97.87:8069/api/auth/get_tokens?username=admin&password=api@ctit');

		    $_token = json_decode($_odoo->getBody(),true)['token'];

			$expire_in = date("Y-m-d");
			
			$records = $this->orderLogic->details($id);

			$dataset['token'] 				= $_token;
			$dataset['status'] 				= 2;
			$dataset['foreign_reference'] 	= $records['order']->order_no;
			$dataset['customer_id'] 		= $records['order']->customer_id;
			$dataset['order_date'] 			= date('Y-m-d');
			$dataset['sales_person'] 		= $records['order']->user_id;
			$dataset['expire_date'] 		= $expire_in;
			$dataset['paid'] 				= floatval($records['order']->amount);
			$dataset['payment_type'] 		= 1;

			$dataset['order_lines']			= [];

			foreach ($records['details'] as $pro) {
				$dd = [];

				$dd['product_id'] 	= $pro->product_id;
				$dd['qty'] 			= $pro->qty;
				$dd['unit_price'] 	= floatval($pro->price);

				array_push($dataset['order_lines'], $dd);
			}

			return $client->post('http://164.132.97.87:8069/api/sale_order', $dataset);

		}catch(\Exception $e){
        	return $e->getMessage();
		}
		
	}

}
