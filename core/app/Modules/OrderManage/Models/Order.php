<?php
/**
 * ORDER MANAGEMENT
 *
 * @version 1.0.0
 * @author Sriya (csriyarathne@gmail.com)
 * @copyright 2017-11-05
 */

namespace App\Modules\OrderManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function details(){
        return $this->hasMany('App\Modules\OrderManage\Models\OrderDetail','order_id');
    }

    public function customer(){
        return $this->belongsTo('App\Models\User','customer_id','id');
    }

    public function performaInvoice(){
        return $this->hasOne('App\Modules\OrderManage\Models\PerformaInvoice','order_id');
    }

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function location(){
        return $this->belongsTo('App\Modules\LocationManage\Models\Location','location_id','id');
    }

    public function country(){
        return $this->belongsTo('App\Models\Country','country_id','id');
    }
}
