<?php
/**
 * ORDER DETAIL MANAGEMENT
 *
 * @version 1.0.0
 * @author Sriya (csriyarathne@gmail.com)
 * @copyright 2017-11-05
 */

namespace App\Modules\OrderManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_detail';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function order(){
        return $this->belongsTo('App\Modules\OrderManage\Models\Order','order_id','id');
    }

    public function product(){
        return $this->belongsTo('App\Modules\Product\Models\Product','product_id','id');
    }
}
