<?php
/**
 * ORDER MANAGEMENT
 *
 * @version 1.0.0
 * @author Sriya (csriyarathne@gmail.com)
 * @copyright 2017-11-05
 */

namespace App\Modules\OrderManage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerformaInvoice extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'performa_invoice';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function order(){
        return $this->belongsTo('App\Modules\OrderManage\Models\Order','order_id','id');
    }
    
    public function customer(){
        return $this->belongsTo('App\Models\User','customer_id','id');
    }

    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }
}
