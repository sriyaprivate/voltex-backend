<?php
namespace App\Modules\OrderManage\Repositories;

use App\Modules\OrderManage\Models\Order;
use App\Modules\OrderManage\Models\PerformaInvoice;
use Sentinel;

class OrderRepository{

    public function getRecords($conditions, $orderColoum, $orderBy,$paginate = true, $records = 20)
    {

        $_usr = Sentinel::getUser();

        $orders = Order::with(['details','customer','performaInvoice','country']);

        if($_usr->country_id){
            $orders = $orders->where('country_id',$_usr->country_id);
        }

        if(!$orderColoum){
            $orderColoum = 'order_no';
        }

        if(count($conditions) > 0){
            foreach($conditions as $column => $condition){
                $orders = $orders->where($column, $condition['operator'], $condition['value']);
            }
        }

        if($paginate){
            $orders = $orders->orderBy($orderColoum, $orderBy)->paginate($records);
        }else{
            $orders = $orders->orderBy($orderColoum, $orderBy)->get();
        }

        if(count($orders) > 0){
            return $orders;
        }else{
            return [];
        }
    }

    public function getPerformaInvoiceRecords($conditions, $orderColoum, $orderBy,$paginate = true, $records = 20){
        $pfi = PerformaInvoice::with(['order','order.details','customer','order.deliveryType','order.country']);

        if(!$orderColoum){
            $orderColoum = 'pf_no';
        }

        if(count($conditions) > 0){
            foreach($conditions as $column => $condition){
                if($column=='order_no'){
                    $pfi->whereIn('order_no',function($q1)use($column,$condition){
                        $q1->select('id')->from('orders')->where($column, $condition['operator'], $condition['value']);
                    });
                }elseif($column=='country_id'){
                    $pfi->whereIn('country_id',function($q1)use($column,$condition){
                        $q1->select('id')->from('orders')->where($column, $condition['operator'], $condition['value']);
                    });
                }else{
                    $pfi->where($column, $condition['operator'], $condition['value']);                    
                }
            }
        }

        if($paginate){
            $pfi = $pfi->orderBy($orderColoum, $orderBy)->paginate($records);
        }else{
            $pfi = $pfi->orderBy($orderColoum, $orderBy)->get();
        }

        if(count($pfi) > 0){
            return $pfi;
        }else{
            return [];
        }
    }

}