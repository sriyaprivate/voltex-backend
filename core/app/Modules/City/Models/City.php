<?php namespace App\Modules\City\Models;

/**
*
* City Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class City extends Model {

	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'city';
     /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}
