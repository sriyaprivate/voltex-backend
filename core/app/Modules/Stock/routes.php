<?php

Route::group(['middleware' => ['auth']], function()
{ 
	Route::group(array('prefix'=>'stock','namespace' => 'App\Modules\Stock\Controllers'), function() {
		
	    /*get routes*/
	    Route::get('add', ['as' => 'stock.add', 'uses' => 'StockController@index']);

	    Route::get('get-product', ['as' => 'stock.add', 'uses' => 'StockController@product_details']);

	    Route::get('get-stock', ['as' => 'stock.add', 'uses' => 'StockController@get_stock']);

	    Route::get('search', ['as' => 'stock.add', 'uses' => 'StockController@search']);
	    
	    /*post routes */ 

	    Route::get('add-stock', ['as' => 'stock.add', 'uses' => 'StockController@store']);
	    
	}); 
});