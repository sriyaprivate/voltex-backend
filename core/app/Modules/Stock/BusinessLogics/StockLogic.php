<?php namespace App\Modules\Stock\BusinessLogics;


/**
* Business Logics 
* Define all the busines logics in here
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use Illuminate\Database\Eloquent\Model;
use App\Modules\Product\Models\Product;
use App\Modules\Stock\Models\Stock;
use App\Modules\Stock\Models\StockTransaction;
use App\Modules\PromotionAllocate\Models\PromotionAllocate;
use DB;

class StockLogic extends Model {

	/** 
	 * The function is used to get all product and its current stock
	 * @param No
	 * @return
	 */

	public function produt_stock(){
		$stock = DB::table('product')
			->leftJoin('stock','product.id','=','stock.product_id')
			->select('stock.id as sid',
					 'product.id as product_id',
					 'product.code',
					 'product.name',
					 DB::raw('max(stock.id) as stock_id'),
					 DB::raw('sum(stock.qty) as stock_qty'))
			->groupBy('product.id')
			->where('product.status','<>',0)
			->whereNull('product.deleted_at')
			->paginate(20);
		if(!$stock){
			throw new Exception("Error occured while getting product stock");
		}else{
			return $stock;
		}
	}

	/** 
	 * The function is used to get last stock transaction
	 * @param Integer $stock_id
	 * @return
	 */

	public function last_stock_transaction($stock_id){
		return $last_stock_transaction = DB::table('stock_transaction')
											->select('qty','transaction_date')
											->where('stock_id',$stock_id)
											->where('type',1)
											->orderBy('transaction_date','DESC')
											->limit(1)
											->get();
	}

	/** 
	 * The function is used to add stock
	 * @param Integer $product_id Integer $warehouse_id Integer $qty String $remark Integer $type
	 * @return
	 */

	public function add_stock($product_id,$warehouse_id,$qty,$remark,$type){
		
		try{
			$stock_check = Stock::where('warehouse_id',$warehouse_id)
							->where('product_id',$product_id)
							->where('status',1)
							->first();
			DB::beginTransaction();									
			if(count($stock_check) > 0){
				$stock_update = Stock::find($stock_check->id);
				if($type > 0){
					$total_stock  = $stock_update->qty + $qty;
					$last_stock   = $qty;
				}else{
					$total_stock  = $stock_update->qty - $qty;
					$last_stock   = '-'.$qty;
				}
				$stock_update->qty  = $total_stock;
				$stock_update->save();
				if($stock_update){
					$stock_transaction = StockTransaction::create([
						'stock_id' 	   		=> $stock_update->id,
						'qty'		   		=> $last_stock,
						'remark'	   		=> $remark,
						'type'				=> 1,
						'transaction_date' 	=> date('Y-m-d h:i:s')
					]);
					if(!$stock_transaction){
						DB::rollBack();
						throw new Exception("Error occured while adding stock trnsaction when update stock");
					}
					if($type == 0){ //when reduce stock , reduce stock in allocated to promotion
 						
 						$allocated_stock = PromotionAllocate::where('product_id',$product_id)
 															 ->where('allocate_qty','>',0)
 															 ->where('status',DEFAULT_STATUS)
 															 ->whereNull('deleted_at')
 															 ->first();
 						if(count($allocated_stock) > 0){
 							if($allocated_stock->allocate_qty <= $qty){
 								$remove_stock = $allocated_stock->allocate_qty;
 							}else{
 								$remove_stock = $qty;
 							}
 							$stock_remove_promotion = DB::table('product_promotion')
											->where('product_id',$product_id)
											->where('allocate_qty','>',0)
											->decrement('allocate_qty',$remove_stock);
								if(!$stock_remove_promotion){
									DB::rollBack();
									throw new Exception("Error occured while removing allocated stock to promotion");
								}
 						}
					}
					DB::commit();
					$total_stock = Stock::select(DB::raw('sum(qty) as total_qty'))->where('product_id',$product_id)
						->where('status',1)->first();
					return ['total_stock' => $total_stock->total_qty,'last_stock' => $stock_transaction->qty,'update' => $stock_transaction->transaction_date];
				}else{
					DB::rollBack();
					throw new Exception("Error occured while update stock");
				}
			}else{
				$stock_add = Stock::create([
					'warehouse_id' => $warehouse_id,
					'product_id'   => $product_id,
					'qty'		   => $qty,
					'status'	   => 1
				]);
				if(!$stock_add){
					DB::rollBack();
					throw new Exception("Error occured while adding stock");
				}
				$stock_transaction = StockTransaction::create([
						'stock_id' 	   		=> $stock_add->id,
						'qty'		   		=> $qty,
						'remark'	   		=> $remark,
						'type'				=> 1,
						'transaction_date' 	=> date('Y-m-d h:i:s')
					]);
				if(!$stock_transaction){
					DB::rollBack();
					throw new Exception("Error occured while adding stock trnsaction when update stock");
				}
				$total_stock = Stock::select(DB::raw('sum(qty) as total_qty'))->where('product_id',$product_id)
							->where('status',1)->first();
				DB::commit();
				return ['total_stock' => $total_stock->total_qty,'last_stock' => $stock_transaction->qty,'update' => $stock_transaction->transaction_date];
			}
		}catch(Exception $e){
			return $e-getMessage();
		}
		 
	}

	/** 
	 * This Function is used to available stock
	 * @param Integer $product_id Integer $warehouse_id
	 * @return
	 */

	public function stock_available($product_id,$warehouse_id){
		try{
			$available_stock = Stock::where('product_id',$product_id)
								->where('warehouse_id',$warehouse_id)
								->where('status',1)
								->first();
			if(count($available_stock) > 0){
				return $available_stock;
			}else{
				return [];
			}
		}catch(Exception $e){
			return $e->getMessage();
		}
	}

	/** 
	 * The function is used to get search product and its current stock
	 * @param No
	 * @return
	 */

	public function search_stock($warehosue,$product,$from,$to,$supplier){
		try{
			$stock = DB::table('product')
					->leftJoin('stock','product.id','=','stock.product_id')
					->select('stock.id as sid',
							 'product.id as product_id',
							 'product.code',
							 'product.name',
							 DB::raw('max(stock.id) as stock_id'),
							 DB::raw('sum(stock.qty) as stock_qty'));
					
					
			if($warehosue > 0){
				$stock = $stock->where('stock.warehouse_id',$warehosue);
			}
			if(strlen($product) > 0){
				$stock = $stock->where('product.name','LIKE','%'.$product.'%')
							   ->orWhere('product.code','LIKE','%'.$product.'%')
							   ->orWhere('product.display_name','LIKE','%'.$product.'%');
			}
			if(strlen($from) && strlen($to)){
				$stock = $stock->whereDate('stock.updated_at','>=',$from)->whereDate('stock.updated_at','<=',$to);
			}
			if($supplier > 0){
				$stock = $stock->where('product.supplier_id',$supplier);
			}
			$stock->groupBy('product.id');
			return $stock->paginate(unserialize(SHOW_OPTION)[1]);

		}catch(Exception $e){
			return $e->getMessage();
		}
	}


}
