<?php namespace App\Modules\Stock\Controllers;


/**
* Controller class
* @author Author <lahirumadhusankaha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;

use App\Modules\Stock\BusinessLogics\StockLogic;
use App\Modules\Product\BusinessLogics\ProductLogic;
use App\Modules\WarehouseManage\BusinessLogics\Logic as Warehouse;
use App\Modules\supplier\Controllers\SupplierController as Supplier;

class StockController extends Controller {

	protected $stock;
	protected $product;
	protected $warehouse;
	protected $supplier;

    public function __construct(StockLogic $stock,ProductLogic $product,Warehouse $warehouse,Supplier $supplier){
        $this->stock  	= $stock;
        $this->warehouse= $warehouse;
        $this->product  = $product;
        $this->supplier  = $supplier;
    }

	/**
	 * Display a listing of the stock of each product.
	 *
	 * @return Response
	 */
	public function index()
	{
		$supplier_list  = $this->supplier->getSupplier();
		$all_warehouses = $this->warehouse->get_warehouse();
		$stock_list 	= $this->stock->produt_stock();
		
		foreach($stock_list as $list_obj){
			if(!empty($list_obj->stock_id)){
				$last_stock_transaction = $this->stock->last_stock_transaction($list_obj->stock_id);
				if(count($last_stock_transaction) > 0){
					$list_obj->last_stock_qty = $last_stock_transaction[0]->qty;
					$list_obj->last_stock_update = $last_stock_transaction[0]->transaction_date;
				}else{
					$list_obj->last_stock_qty = null;
					$list_obj->last_stock_update = null;
				}
			}else{
				$list_obj->last_stock_qty = null;
				$list_obj->last_stock_update = null;
			}
		}
		return view("Stock::index")->with([
			'list' => $stock_list,
			'all_warehouses' => $all_warehouses,
			'warehouse_search'=> '',
			'product_search'  => '',
			'start'			  => '',
			'end'			  => '',
			'supplier_list'   => $supplier_list,
			'supplier'		  => ''
		]);
	}

	/**
	 * Get each of the product
	 * @param 
	 * @return Response
	 */
	public function product_details(Request $request){
		if($request->ajax()){
			$product 	   = $this->product->get_product($request->get('product'));
			$section_list  = '';
			$section_list .= '<select id="home_decide" class="form-control input-sm chosen" name="home_decide">';
            $section_list .= '<option value=" ">Select Section</option>';
            if(count(unserialize(HOME_SECTION)) > 0){
                foreach(unserialize(HOME_SECTION) as $key => $value){
                	if(count($product) > 0){
                		if($product[0]->status == 2){
                			if($product[0]->home_decide == $key){
        						$section_list .= '<option value="'.$key.'" selected>'.unserialize(HOME_SECTION)[$key].'</option>';	
                			}else{
                				if($key != 1 && $key != 3){
                					$section_list .= '<option value="'.$key.'">'.$value.'</option>';
                				}
                			}
                		}else{
		                	if($product[0]->home_decide == $key){
		                    	$section_list .= '<option value="'.$key.'" selected>'.$value.'</option>';
		                	}else{
		                		//if product have deals
		                		if($key != 2){
									$section_list .= '<option value="'.$key.'">'.$value.'</option>';
		                		}
		                	}
                		}
                	}
                }
            }
            $section_list .= '</select>';
			return Response::json(['product' => $product,'section' => $section_list]);
		}else{
			return Response::json([]);
		}
	}

	/**
	 * Check the stock available
	 * @param 
	 * @return Response
	 */
	public function get_stock(Request $request){
		if($request->ajax()){
			$stock = $this->stock->stock_available($request->get('product'),$request->get('warehouse'));
			return Response::json(['stock' => $stock]);
		}else{
			return Response::json([]);
		}
	}

	/**
	 * Search stock 
	 * @param 
	 * @return Response
	 */
	public function search(Request $request){
		$supplier_list  = $this->supplier->getSupplier();
		$all_warehouses = $this->warehouse->get_warehouse();
		$stock_list 	= $this->stock->search_stock($request->get('warehouse_search'),$request->get('product_search'),$request->get('start'),$request->get('end'),$request->get('supplier'));
		foreach($stock_list as $list_obj){
			if(!empty($list_obj->stock_id)){
				$last_stock_transaction = $this->stock->last_stock_transaction($list_obj->stock_id);
				$list_obj->last_stock_qty = $last_stock_transaction[0]->qty;
				$list_obj->last_stock_update = $last_stock_transaction[0]->transaction_date;
			}else{
				$list_obj->last_stock_qty = null;
				$list_obj->last_stock_update = null;
			}
		}
		return view("Stock::index")->with([
			'list' 			  => $stock_list,
			'all_warehouses'  => $all_warehouses,
			'warehouse_search'=> $request->get('warehouse_search'),
			'product_search'  => $request->get('product_search'),
			'start'			  => $request->get('start'),
			'end'			  => $request->get('end'),
			'supplier_list'   => $supplier_list,
			'supplier'		  => $request->get('supplier')

		]);
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a new stock Qty
	 * @param
	 * @return Response
	 */
	public function store(Request $request)
	{
		if($request->ajax()){
			$stock_add = $this->stock->add_stock($request->get('product'),$request->get('warehouse'),$request->get('qty'),$request->get('remark'),$request->get('type'));
			return Response::json(['stock_status' => $stock_add]);
		}else{
			return Response::json([]);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
