@extends('layouts.back_master') @section('title','Stock Management')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Stock
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li class="active">Stock Add</li>
	</ol>
</section>
{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Add Stock</h3>
		</div>
        <form role="form" class="form-horizontal form-validation" method="get" autocomplete="off" action="{{url('stock/search')}}">
		    <div class="box-body">
		    	<div class="form-group">
		    		<div class="col-sm-4">
                        <select id="warehouse_search" class="form-control input-sm chosen" name="warehouse_search">
                            <option value="">Select Warehouse</option>
                            @if(count($all_warehouses))
                            	@foreach($all_warehouses as $key => $value)
                                	<option value="{{$key}}" @if($warehouse_search == $key) selected @endif>{{$value}}</option>
                            	@endforeach
                            @endif
                        </select>
                        @if($errors->has('big_store_category'))
                            <label id="label-error" class="error" for="label">The big store category field is required when other category is not selected</label>
                        @endif
                    </div>
                    <div class="col-sm-4">
                    	<input type="text" class="form-control input-sm" id="product_search" name="product_search" placeholder="Search Product Name & Product Code" value="{{$product_search}}">
                    </div>
                    <div class="col-sm-4">
                	    <div class="input-daterange input-group" id="datepicker">
					        <input type="text" class="input-sm form-control" name="start" placeholder="From" value="{{$start}}" />
					        <span class="input-group-addon">to</span>
					        <input type="text" class="input-sm form-control" name="end" placeholder="To" value="{{$end}}"/>
					    </div>
                    </div>	
		    	</div>
		    	<div class="form-group">
		    		<div class="col-sm-4">
                        <select name="supplier" class="form-control chosen">
                            <option value="">Select Supplier</option>
                            @if(count($supplier_list) > 0)
                              @foreach($supplier_list as $key => $value)
                                <option value="{{$key}}" @if($key == $supplier) selected @endif>
                                  {{$value}}
                                </option>
                              @endforeach
                            @endif
                        </select>
                    </div>	
		    	</div>
		    	
		    	<div class="form-group">
                    <div class="col-md-3 col-md-offset-9">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-default btn-sm" id="plan"><i class="fa fa-search" style="padding-right: 16px;width: 28px;"></i>Search</button>
                            <a href="add" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top"><i class="fa fa-refresh" style="padding-right: 16px;width: 20px;"></i>Refresh</a>
                        </div>
                    </div>
                </div>
            </div>    
        </form>
    </div>
    <div class="box">
        <div class="col refresh">
	        <div class="box box-widget box-list box-primary">
	            <div class="box-header with-border">
	                <h3 class="box-title">Product Stock List</h3>
	            </div>
	            <div class="box-body">
	                <table class="table table-bordered bordered table-striped table-condensed" id="orderTable" >
	                    <thead>
	                    <tr>
	                        <th width="5%" class="text-center">ID</th>
	                        <th width="10%">Product Code</th>
	                        <th width="15%">Product Name</th>
	                        <th width="8%">Total Stock</th>
	                        <th width="8%">Last Stock</th>
	                        <th width="12%">Last Transaction Date</th>
	                        <th width="5%" class="text-center">Action</th>
	                    </tr>
	                    </thead>
	                    <tbody>
	                    <?php $i = ($list->currentpage()-1)* $list->perpage() + 1;?>
	                        @if(count($list) > 0)
	                            @foreach($list as $value)
	                                @include('Stock::template.stock')
	                            <?php $i++;?>
	                            @endforeach
	                        @else
	                            <tr><td colspan="7" class="text-center">No data found.</td></tr>
	                        @endif
	                        </tbody>
	                    </table>
	                    Showing {{$list->firstItem()}} to {{$list->lastItem()}} of {{$list->total()}} products
	                    @if($list != null)
	                        <div style="float: right;"> {!! count($list) > 0?$list->appends($_GET)->render():'' !!} </div>
	                    @endif
	                    
	            </div>
	        </div>
    	</div>
	</div>
</section>
<!-- hide section - add stock -->

<!-- hide section - add stock -->
@stop
@section('js')
<script type="text/javascript">
/*Add new stock*/
$('.input-daterange').datepicker({
	format: "yyyy-mm-dd",
	autoclose: true
});

$('.add-stock').on('click',function(){
	var row = $(this);
  	$(".content").addClass('panel-refreshing');
    var product_id = $(this).data('idd');
  	$.ajax({
      url: "{{URL::to('stock/get-product')}}",
      method: 'GET',
      data: {'product':product_id},
      async: false,
      success: function (data) {
      	product = data.product[0].code+' | '+data.product[0].name;
      },error: function () {
        $.alert({
         	theme: 'material',
         	title: 'Error Occured',
         	type: 'red',
         	content: 'Error Occured when adding stock'
        });
      }
  	});

  	var content = '<div class="row">'+
    	'<div class="col-lg-12 form-group">'+
      		'<label class="control-label" id="product"><h4>'+product+'</h4></label>'+
      		'<input type="hidden" id="product_id" value="'+product_id+'">'+
    	'</div>'+
    	'<div class="col-lg-12 form-group">'+
      		'<label class="control-label">Warehouse</label>'+
        	'<select id="warehouse" class="form-control input-sm chosen" name="warehouse">'+
	            '<option value="">Select Warehouse</option>'+
	            @if(count($all_warehouses) > 0)
		            @foreach($all_warehouses as $key => $value)
		            	'<option value="{{$key}}">{{$value}}</option>'+
		            @endforeach
		        @endif
        	'</select>'+
        	'<label id="label-error" class="error warehouse hide" for="label">Please Select Warehouse</label>'+
	    '</div>'+
	    '<div class="col-lg-12 form-group">'+
	      '<label class="control-label">Current Stock</label>'+
	        '<input type="text" class="form-control input-sm" id="current_stock" readonly>'+
	    '</div>'+
	    '<div class="col-lg-12 form-group">'+
	      '<label class="control-label">Transaction Type</label>'+
	      		'<select id="type" class="form-control input-sm type chosen" name="type">'+
	            '<option value="1">Add Stock</option>'+
	            '<option value="0">Remove Stock</option>'+
        	'</select>'+
	    '</div>'+
	    '<div class="col-lg-12 form-group">'+
	      '<label class="control-label">Qunatity</label>'+
	        '<input type="text" class="form-control input-sm new_qty" name="new_qty" id="new_qty" placeholder="Enter Quantity">'+
	        '<label id="label-error" class="error new-qty hide" for="label">Please Enter Quantity</label>'+
	    '</div>'+
	    '<div class="col-lg-12 form-group">'+
	      '<label class="control-label">Note</label>'+
	        '<textarea id="remark" class="form-control input-sm description" rows="3" name="remark" placeholder="Enter Remark"></textarea>'+
	    '</div>'+
	  '</div>';
	$('.chosen').chosen();
  	$.confirm({
        title: '',
        content:content,
        buttons: {
            confirm: {
                btnClass: 'btn-sm bg-purple disable',
                text: 'Save',
                action: function () {
                	if($('#warehouse').val() == ''){
                		$('.warehouse').removeClass('hide');
                		return false;
                	}
                	if($('#new_qty').val() == ''){
                		$('.new-qty').html('Please Enter Quantity');
                		$('.new-qty').removeClass('hide');
                		return false;
                	}
                	if($('#type').val() == 0){
	                	if(parseInt($('#new_qty').val()) < 1){
	                		$('.new-qty').html('Please Enter Correct Quantity');
	                		$('.new-qty').removeClass('hide');
	                		return false;
	                	}
	                	if(parseInt($('#current_stock').val()) < parseInt($('#new_qty').val())){
	                		$('.new-qty').html('Please Enter Correct Quantity');
	                		$('.new-qty').removeClass('hide');
	                		return false;
	                	}
	                }
	                $(".content").addClass('panel-refreshing');
                    $.ajax({
				      url: "{{URL::to('stock/add-stock')}}",
				      method: 'GET',
				      data: {
				      	'product'  :product_id,
				      	'warehouse':$('#warehouse').val(),
				      	'type'	   :$('#type').val(),
				      	'qty' 	   :$('#new_qty').val(),
				      	'remark'   :$('#remark').val()	 
				      },
				      async: false,
				      success: function (data) {
				      	row.closest("tr").find('#total-stock').html(data.stock_status.total_stock);
				      	row.closest("tr").find('#last_stock_qty').html(data.stock_status.last_stock);
				      	row.closest("tr").find('#stock_update').html(data.stock_status.update);
				      	$(".content").removeClass('panel-refreshing');

				      },error: function (data) {
				      	$(".content").removeClass('panel-refreshing');
				         $.alert({
				         	theme: 'material',
				         	title: 'Error Occured',
				         	type: 'red',
				         	content: 'Error Occured when adding stock'
				        });
				      }
				  	});
                }
            },
            cancelAction: {
                text: 'Cancel',
                action: function () {
                    $(".content").removeClass('panel-refreshing');
                }
            }
        }
    });
});
/*Add new stock*/
/*Get Available Stock*/
$(document).on('change','#warehouse',function(){
	$.ajax({
      url: "{{URL::to('stock/get-stock')}}",
      method: 'GET',
      data: {
      	'product':$('#product_id').val(),
      	'warehouse':$(this).val()
      },
      async: false,
      success: function (data) {
      	if(Object.keys(data.stock).length > 0){
	      	if(data.stock.qty > 0){
	      		$('#current_stock').val(data.stock.qty);
	      		$(".type option[value='0']").removeAttr('disabled');
	      	}else{
	      		$('#current_stock').val(data.stock.qty);
	      		$(".type option[value='0']").attr('disabled','true');
	      	}
	    }else{
      		$('#current_stock').val(0);
      		$(".type option[value='0']").attr('disabled','true');
      	}
      	
      },error: function () {
   		$.alert({
         	theme: 'material',
         	title: 'Error Occured',
         	type: 'red',
         	content: 'Error Occured when adding stock'
        });	
      }
  	});
});
/*Get Available Stock*/
</script>
@stop
