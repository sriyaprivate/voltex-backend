<tr id="{{$i}}">
     <td class="text-center">{{$i}}</td>
     <td>{{$value->code}}</td>
     <td>{{$value->name}}</td>
     <td id="total-stock">{{$value->stock_qty?:'0'}}</td>
     <td id="last_stock_qty">{{$value->last_stock_qty?:'0'}}</td>
     <td id="stock_update">{{$value->last_stock_update?:'-'}}</td>
     <td class="text-center">
        <div class="btn-group">
            <!-- <a href="javascript:void(0);" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="View More"><i class="fa fa-eye"></i></a> -->
            <a href="javascript:void(0);" class="btn btn-xs btn-default add-stock" data-toggle="tooltip" data-placement="top" title="Add Stock" data-idd="{{$value->product_id}}"><i class="fa fa-pencil"></i></a>
        </div>
     </td>
</tr>

