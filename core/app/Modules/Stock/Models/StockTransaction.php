<?php namespace App\Modules\Stock\Models;

/**
*
* Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class StockTransaction extends Model {

	protected $table   = 'stock_transaction';

	protected $guarded = ['id']; 

}
