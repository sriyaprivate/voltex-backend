<?php namespace App\Modules\Stock\Models;

/**
*
* Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;


class Stock extends Model {

	protected $table   = 'stock';

	protected $guarded = ['id']; 


	public function return_product(){
        return $this->hasOne('App\Models\ReturnPolicy','product_id','id');
    }

}
