<?php 
namespace App\Modules\EMployeeManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\City;

use Illuminate\Http\Request;

use App\Modules\EmployeeManage\Models\Designation;
use App\Modules\EmployeeManage\BusinessLogics\EmployeeLogic;
use App\Modules\EmployeeManage\Repositories\EmployeeRepository;

class EmployeeManageController extends Controller {

	protected $employee;
	protected $repository;

	private function employeeValidate(Request $request){
		$this->validate($request,[
			'first_name' => 'required|unique:employee,full_name,'.$request->id,
			'code' => 'required',
			'designation' => 'required',
		]);
	}

	public function __construct(EmployeeLogic $employee, EmployeeRepository $repository){
		$this->employee = $employee;
		$this->repository = $repository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view("EmployeeManage::index");
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function addView()
	{
		$designations = Designation::orderBy('name', 'ASC')->lists('name','id');
		return view("EmployeeManage::add")->with(compact('designations'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function add(Request $request)
	{
		$this->employeeValidate($request);
		try{
			$this->employee->saveEmployee($request);

			return redirect('employee/add')->with([
                'success.title'   => 'Done!',
                'success.message' => 'Employee has been successfully added!'
            ]);
		}catch(\Exception $e){
			return redirect('employee/add')->with([
                'error.title'   => 'Error',
                'error.message' => $e->getMessage()
            ])->withInput();
		}
	}

	public function editView($id)
	{
		$designations = Designation::orderBy('name', 'ASC')->lists('name','id');
		$employee = $this->repository->getEmployeeById($id);

		return view("EmployeeManage::edit")->with(compact('designations', 'employee'));
	}

	public function edit($id, Request $request)
	{
		$this->employeeValidate($request);
		try{
			$this->employee->updateEmployee($id, $request);

			return redirect('employee')->with([
                'success.title'   => 'Done!',
                'success.message' => 'Employee has been successfully updated!'
            ]);
		}catch(\Exception $e){
			return redirect('employee/edit/'.$id)->with([
                'error.title'   => 'Error',
                'error.message' => $e->getMessage()
            ])->withInput();
		}
	}

	public function delete($id)
	{
		try{
			$this->employee->deleteEmployee($id);

			return redirect('employee')->with([
                'success.title'   => 'Done!',
                'success.message' => 'Employee has been successfully deleted!'
            ]);
		}catch(\Exception $e){
			return redirect('employee/list')->with([
                'error.title'   => 'Error',
                'error.message' => $e->getMessage()
            ])->withInput();
		}
	}

	public function listView(Request $request)
	{
		try{
			$records = $this->employee->filterRecords($request);
		}catch(\Exception $e){
			$records = [];
		}

		$designations = Designation::orderBy('name', 'ASC')->lists('name','id');

		return view('EmployeeManage::list')->with([
			'old' => $request, 
			'records' => $records, 
			'designations' => $designations
		]);
	}

}
