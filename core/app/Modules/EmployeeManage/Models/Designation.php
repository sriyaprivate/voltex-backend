<?php namespace App\Modules\EmployeeManage\Models;

/**
*
* Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Designation extends Model {

    use SoftDeletes;
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'designation';
     /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $fillable = ['name', 'status', 'created_at', 'updated_at', 'deleted_at'];

    public function employees(){
        return $this->hasMany('App\Models\Eployee','id','employee_id');
    }

}
