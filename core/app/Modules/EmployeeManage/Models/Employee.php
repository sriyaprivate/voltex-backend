<?php namespace App\Modules\EmployeeManage\Models;

/**
*
* Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model {

    use SoftDeletes;
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employee';
     /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $fillable = ['code', 'first_name', 'last_name', 'full_name', 'designation_id', 'contact', 'address', 'status', 'created_at', 'updated_at', 'deleted_at'];

    public function designation(){
        return $this->belongsTo('App\Modules\EmployeeManage\Models\Designation','designation_id','id')->withTrashed();
    }

}
