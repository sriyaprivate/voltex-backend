<?php
namespace App\Modules\EmployeeManage\Repositories;

use App\Modules\EmployeeManage\Models\Employee;

class EmployeeRepository{

    public function save(array $data){
        $employee = Employee::create($data);

        if(!$employee){
            throw new \Exception('LR1-Could not save the employee');
        }

        return 1;
    }

    public function getEmployeeById($id){
        $employee = Employee::with(['designation'])->where('id', $id)->first();

        if(!$employee){
            throw new \Exception('LR2-Could not find employee for Id: '.$id);
        }

        return $employee;
    }

    public function update($id, $data){
        $employee = $this->getEmployeeById($id);
        $employee->designation_id   = $data->designation_id;
        $employee->first_name       = $data->first_name;
        $employee->last_name        = $data->last_name;
        $employee->full_name        = $data->first_name." ".$data->last_name;
        $employee->contact          = $data->contact;
        $employee->address          = $data->address;
        $employee->status           = $data->status;
        $employee->save();
        
        return 1;
    }

    public function delete($id){
        $employee = $this->getEmployeeById($id);
        $employee->delete();
        
        return 1;
    }

    public function getRecords($conditions, $orderBy,$paginate = true, $records = 20){
        $employees = Employee::with(['designation']);

        if(!$orderBy){
            $orderBy = 'id';
        }

        if(count($conditions) > 0){
            foreach($conditions as $column => $condition){
                $employees->where($column, $condition['operator'], $condition['value']);
            }
        }

        if($paginate){
            $employees = $employees->orderBy($orderBy, 'asc')->paginate($records);
        }else{
            $employees = $employees->orderBy($orderBy, 'asc')->get();
        }

        if(count($employees) > 0){
            return $employees;
        }else{
            return [];
        }
    }
}