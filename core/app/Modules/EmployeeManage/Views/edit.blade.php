@extends('layouts.back_master') @section('title','Add Employee')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" href="{{asset('assets/dist/jquery-multiselect/css/multi-select.css')}}">
<style type="text/css">
.ms-container {
    background: transparent url("{{asset('assets/dist/jquery-multiselect/img/switch3.png')}}") no-repeat 50% 50%;
    width: 100%;
}

.has-error .chosen-container{
    border: 1px solid #ff0505;
    border-radius: 3px;
}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Employee
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('employee')}}}">Employee List</a></li>
		<li class="active">Edit</li>
	</ol>
</section>
{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Edit Employee</h3>
			<!--<div class="box-tools pull-right">
				<a href="{{url('employee/list')}}" class="btn btn-warning btn-sm" style="margin-top: 2px;">Employee List</a>
			</div>-->
		</div>
        <form role="form" class="form-horizontal form-validation" method="post" autocomplete="off">
		    <div class="box-body">
                {!!Form::token()!!}
                <div class="form-group @if($errors->has('code')) has-error @endif">
                    <label class="col-sm-2 control-label">Code <span class="require">*</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="code" placeholder="Code" value="{{$employee->code}}">
                        @if($errors->has('code'))
                            <label id="label-error" class="error" for="label">{{$errors->first('code')}}</label>
                        @endif
                    </div>
                </div>

                <div class="form-group @if($errors->has('name')) has-error @endif">
                    <label class="col-sm-2 control-label">Name <span class="require">*</span></label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control input-sm" name="first_name" placeholder="First Name" value="{{$employee->first_name}}">
                        @if($errors->has('first_name'))
                            <label id="label-error" class="error" for="label">{{$errors->first('first_name')}}</label>
                        @endif
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control input-sm" name="last_name" placeholder="Last Name" value="{{$employee->last_name}}">
                        @if($errors->has('last_name'))
                            <label id="label-error" class="error" for="label">{{$employee->last_name}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('designation')) has-error @endif">
                    <label class="col-sm-2 control-label">Designation <span class="require">*</span></label>
                    <div class="col-sm-4">
                        <select name="designation" class="form-control chosen">
                            <option value="">Select a Designation</option>
                            @if(count($designations) > 0)
                                @foreach($designations as $key => $designation)
                                <option value="{{$key}}" @if($employee->designation_id == $key) selected @endif>{{$designation}}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has('designation'))
                            <label id="label-error" class="error" for="label">{{$errors->first('designation')}}</label>
                        @endif
                    </div>

                    <div class="col-sm-4">
                        <input type="text" class="form-control input-sm" name="contact" placeholder="Ex:0710000001" value="{{$employee->contact}}">
                        @if($errors->has('contact'))
                            <label id="label-error" class="error" for="label">{{$errors->first('contact')}}</label>
                        @endif
                    </div>

                </div>

                <div class="form-group @if($errors->has('contact')) has-error @endif">
                    <label class="col-sm-2 control-label">Address <span class="require">*</span></label>
                    <div class="col-sm-8">
                        <textarea class="form-control input-sm" name="address" placeholder="Address">{{$employee->address}}</textarea>
                        @if($errors->has('address'))
                            <label id="label-error" class="error" for="label">{{$errors->first('address')}}</label>
                        @endif
                    </div>
                </div>

                <div class="form-group @if($errors->has('status')) has-error @endif">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-9">
                        <select name="status" class="form-control chosen">
                            <option value="1" @if($employee->status == 1) selected @endif>Active</option>
                            <option value="0" @if($employee->status == 0) selected @endif>Inactive</option>
                        </select>
                        @if($errors->has('status'))
                            <label id="label-error" class="error" for="label">{{$errors->first('status')}}</label>
                        @endif
                    </div>
                </div>
            </div>
            <div class="box-footer with-border">
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-9">
                        <button type="submit" class="btn bg-purple btn-sm pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                </div>
            </div>
        </form>
	</div>
</section>
@stop
@section('js')
<script type="text/javascript">
</script>
@stop
