<?php namespace App\Modules\EmployeeManage\BusinessLogics;

use App\Modules\EmployeeManage\Repositories\EmployeeRepository;

/**
* Business Logics 
* Define all the busines logics in here
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use App\Modules\EmployeeManage\Models\Employee;

class EmployeeLogic{

	protected $repository;

	public function __construct(EmployeeRepository $repository){
		$this->repository = $repository;
	}

	public function allEmployee(){
		return $employee = Employee::all()->lists('name','id');
	}

	public function saveEmployee($data){

		if(!$data->code && !$data->first_name && !$data->last_name && !$data->designation && !$data->contact && !$data->address){
			throw new \Exception("LL1-Invalid data array");
		}

		$data = [
			'code' => $data->code,
			'first_name' => $data->first_name,
			'last_name' => $data->last_name,
			'full_name' => $data->first_name." ".$data->last_name,
			'contact' => $data->contact,
			'address' => $data->address,
			'designation_id' => $data->designation,
			'status' => 1
		];

		return $this->repository->save($data);
	}

	public function updateEmployee($id, $data){

		if(!$id){
			throw new \Exception("LL2-Invalid Id");
		}

		if(!$data->code && !$data->first_name && !$data->last_name && !$data->designation && !$data->contact && !$data->address){
			throw new \Exception("LL3-Invalid data array");
		}

		return $this->repository->update($id, $data);
	}

	public function deleteEmployee($id){
		
		if(!$id){
			throw new \Exception("LL4-Invalid Id");
		}

		return $this->repository->delete($id);
	}

	public function filterRecords($data){

		$conditions = [];
		if($data->name != ''){
			$conditions['full_name'] = ['operator' => 'LIKE', 'value' => '%'.$data->name.'%'];
		}

		if($data->designation != ''){
			$conditions['designation_id'] = ['operator' => '=', 'value' => $data->designation];
		}

		if($data->status != ''){
			$conditions['status'] = ['operator' => '=', 'value' => $data->status];
		}

		return $this->repository->getRecords($conditions, 'code');
	}

}
