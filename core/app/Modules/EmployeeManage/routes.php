<?php

Route::group(['middleware' => ['auth']], function()
{
    Route::group(array('prefix'=>'employee','namespace' => 'App\Modules\EmployeeManage\Controllers'), function()
    {
        //GET Routes
        Route::get('list', ['as' => 'employee.list', 'uses' => 'EmployeeManageController@listView']);

        Route::get('add', ['as' => 'employee.add', 'uses' => 'EmployeeManageController@addView']);

        Route::get('edit/{id}', ['as' => 'employee.edit', 'uses' => 'EmployeeManageController@editView']);

        Route::get('delete/{id}', ['as' => 'employee.delete', 'uses' => 'EmployeeManageController@delete']);

        //POST Routes
        Route::post('add', ['as' => 'employee.add', 'uses' => 'EmployeeManageController@add']);

        Route::post('edit/{id}', ['as' => 'employee.edit', 'uses' => 'EmployeeManageController@edit']);
        
    });
});