@extends('layouts.back_master') @section('title','Repeater Request')

@section('links')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/bootstrap-tokenfield.css')}}">
    <link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        .title{
            text-align: left !important;
        }
        .input-group-addon.error{
            border-color:#dd4b39;
        }
        .control-label.error{
            color:#dd4b39;
            font-weight:700 !important;
        }
    </style>
@stop

@section('content')
    <section class="content-header">
        <h1>
        Repeater Request
        <small> Management</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
            <li><a href="{{{url('product/list')}}}">Repeater Request Management</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-widget box-list box-primary">
            <div class="box-body">
                <table class="table table-sm table-bordered">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th>Request No</th>
                            <th>Repeater Product</th>
                            <th>Initiator</th>
                            <th>Created Date</th>
                            <th width="10%" class="text-center" colspan="3">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($_lists)>0)
                            <?php $i=1; ?>
                            @foreach($_lists as $_list)
                                <tr>
                                    <td>{{$i}}</td>
                                    <td>
                                        <a target="_blank" href="{{url('repeater/show/')}}/{{$_list->id}}" style="margin:0">
                                            {{$_list->request_no}}
                                        </a>
                                    </td>
                                    <td>{{$_list['form']['category']->name}}</td>
                                    <td>
                                        {{$_list['initiator']->username}}
                                        <br>
                                        {{$_list['initiator']->first_name}} {{$_list['initiator']->last_name}}
                                    </td>
                                    <td>{{$_list->created_at}}</td>
                                    <td style="text-align: center;">
                                        <a target="_blank" href="{{url('repeater/show/')}}/{{$_list->id}}" style="margin:0">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5" style="text-align: center">No Forms</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </section>

@stop

@section('js')
    <script>
        $(document).ready(function(){
            
        });
</script>
@stop