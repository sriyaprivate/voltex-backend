@extends('layouts.back_master') @section('title','Repeater Request')

@section('links')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/dist/bootstrap-token/css/bootstrap-tokenfield.css')}}">
    <link href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        .title{
            text-align: left !important;
        }
        .input-group-addon.error{
            border-color:#dd4b39;
        }
        .control-label.error{
            color:#dd4b39;
            font-weight:700 !important;
        }
    </style>
@stop

@section('content')
    
    <section class="content-header">
        <h1>
        Repeater Request
        <small> Management</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{{url('admin')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
            <li><a href="{{{url('product/list')}}}">Repeater Request Management</a></li>
            <li class="active">List</li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-widget box-list box-primary">
            <div class="box-body">
                <h3 class="box-title">
                    Repeater Request - {{ $form->form->category->name }} {{$form?"#(".$form->request_no.")":""}}
                </h3>
            
                <h4 style="margin-top: 0">Request From</h4>
                <table class="table table-bordered" style="margin-bottom: 20px;">
                    <tbody>
                        <tr>
                            <td width="20%">Initiated By</td>
                            <td width="80%">
                                <strong>
                                    {{$form->initiator->first_name}} {{$form->initiator->last_name}}<br>
                                    {{$form->initiator->username}}
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%">Contact</td>
                            <td width="80%">
                                {{$form->initiator->mobile?$form->initiator->mobile:"no contact number found "}}
                            </td>            
                        </tr>
                        <tr>
                            <td width="20%">Email</td>
                            <td width="80%">
                                <a href="mailto: {{$form->initiator->email?$form->initiator->email:''}}"> {{$form->initiator?$form->initiator->email:" "}}</a>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <h4>Request Details</h4>
                <table class="table table-bordered">
                    <tbody>
                        <?php $steps = json_decode($json)->count ?>
                        @for($i=1 ; $i <= $steps ; $i++)
                            <?php $aa = 'step'.$i?>
                            <?php $array = json_decode($json,true)?>
                            {{-- Getting json repeater form details --}}
                            @foreach($array[$aa]['fields'] as $fields)
                                <tr>
                                    @if(isset($fields['hint']) && ($fields['hint']) != null)
                                        <td width="20%">{{$fields['hint']}}</td>
                                    @elseif(isset($fields['label']) && ($fields['label']) != null)
                                        <td width="20%">{{$fields['label']}}</td>

                                        {{-- Getting values if form input type equal Checkbox --}}
                                        @if((($fields['type']) == "check_box") && (isset($fields['options'])))
                                            @foreach($fields['options'] as $option)
                                                @if(($option['value']) == "true")
                                                  <td>{{$option['text']}}</td>
                                                @elseif(($option['value']) == "false")
                                                  <td>{{$option['text'] = "-"}}</td>
                                                @endif
                      
                                            @endforeach
                                        @endif
                                    @endif

                                    {{-- Getting values if form input type not equal Checkbox --}}
                                    <td colspan="4">{{isset($fields['value'])?$fields['value']:'-'}}</td>
                                </tr>
                            @endforeach
                        @endfor
                    </tbody>
                </table>
                  
                <div class="row">
                    @foreach($form->attachments as $key=>$attach)
                    <div class="col-md-3">
                        <div class="card mb-3 box-shadow">
                            <div class="card-img-top" style="height: 155px; width: 100%; display: block;">
                                <img class="img-responsive" src="{{url('core/storage/'.$attach->file)}}" width="100%" style="margin: 10% auto;">
                                <hr style="color: #ddd">
                            </div>
                            <div class="card-body"> 
                                <p class="card-text">Uploaded By
                                <br><small class="text-muted">{{$attach->created_at}}</small></p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <a target="_blank" href="{{url('core/storage/'.$attach->file)}}" class="btn btn-sm btn-outline-secondary">View</a>
                                    </div>                  
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@stop

@section('js')
    <script>
        $(document).ready(function(){
            
        });
</script>
@stop