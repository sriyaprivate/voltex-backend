<?php 
namespace App\Modules\Repeater\Models;

use Illuminate\Database\Eloquent\Model;

class RepeaterFormCategories extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'repeater_form_categories';

    /**
     * The attributes that are not assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

}
