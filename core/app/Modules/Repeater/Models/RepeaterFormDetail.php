<?php 
namespace App\Modules\Repeater\Models;

use Illuminate\Database\Eloquent\Model;

class RepeaterFormDetail extends Model {

	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'repeater_form_details';

    /**
     * The attributes that are not assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function category(){
        return $this->belongsTo('App\Modules\Repeater\Models\RepeaterFormCategories','category_id','id');
    }

}
