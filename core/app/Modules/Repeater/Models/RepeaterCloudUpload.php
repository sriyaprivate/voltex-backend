<?php
namespace App\Modules\Repeater\Models;

use Baum\Extensions\Eloquent\Model;

class RepeaterCloudUpload extends Model {

    protected $table = 'repeater_request_cloud_uploads';

    protected $guarded = ['id'];

}