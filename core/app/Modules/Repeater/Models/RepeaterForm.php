<?php
namespace App\Modules\Repeater\Models;

use Baum\Extensions\Eloquent\Model;

class RepeaterForm extends Model {

    protected $table = 'repeater_form';

    protected $guarded = ['id'];

    public function category(){
        return $this->belongsTo('App\Modules\Repeater\Models\RepeaterFormCategories','repeater_form_category_id','id');
    }


}