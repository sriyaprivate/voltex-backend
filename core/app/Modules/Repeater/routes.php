<?php

Route::group(['middleware' => ['auth']], function()
{ 
	Route::group(array('prefix'=>'repeater','namespace' => 'App\Modules\Repeater\Controllers'), function() {

	    Route::get('list', [
	        'uses'  => 'RepeaterController@viewRepeaterRequestList',
	        'as'    => 'repeater.list.request'
	    ]);

	    Route::get('show/{id}', [
	        'uses'  => 'RepeaterController@showRequestDetails',
	        'as'    => 'repeater.show.request'
	    ]);

	}); 
});