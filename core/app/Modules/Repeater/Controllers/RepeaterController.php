<?php 
namespace App\Modules\Repeater\Controllers;

/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Exception;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

use App\Modules\Repeater\Models\RepeaterFormCategories;
use App\Modules\Repeater\Models\RepeaterForm;
use App\Modules\Repeater\Models\RepeaterFormRequests;
use App\Modules\Repeater\Models\RepeaterFormAttachment;

class RepeaterController extends Controller {

    public function __construct()
    {
    
    }

    public function viewRepeaterRequestList(Request $request)
    {   
        $_lists = RepeaterFormRequests::with(['form.category','attachments','initiator']);

        $_lists = $_lists->orderBy('timestamp','desc')->paginate(10);

        return view("Repeater::list-request",compact('_lists'));
    }

    public function showRequestDetails($id)
    {   
        $form = RepeaterFormRequests::with(['form.category','initiator'])->find($id);
        
        $json = json_decode($form->form_details);
        
        $files =  RepeaterFormAttachment::where('request_id',$id)->get();

        return view("Repeater::show-request",compact('json','form','files'));
    }

}
