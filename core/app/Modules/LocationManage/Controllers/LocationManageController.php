<?php 
namespace App\Modules\LocationManage\Controllers;


/**
* Controller class
* @author Author <author@gmail.com>
* @version x.x.x
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\City;

use Illuminate\Http\Request;

use App\Modules\LocationManage\BusinessLogics\LocationLogic;
use App\Modules\LocationManage\Repositories\LocationRepository;

class LocationManageController extends Controller {

	protected $location;
	protected $repository;

	private function locationValidate(Request $request){
		$this->validate($request,[
			'name' => 'required|unique:location,name,'.$request->id,
			'city' => 'required'
		]);
	}

	public function __construct(LocationLogic $location, LocationRepository $repository){
		$this->location = $location;
		$this->repository = $repository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view("LocationManage::index");
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function addView()
	{
		$cities = City::orderBy('name', 'ASC')->lists('name','id');
		return view("LocationManage::add")->with(compact('cities'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function add(Request $request)
	{
		$this->locationValidate($request);
		try{
			$this->location->saveLocation($request);

			return redirect('location/add')->with([
                'success.title'   => 'Done!',
                'success.message' => 'Location has been successfully added!'
            ]);
		}catch(\Exception $e){
			return redirect('location/add')->with([
                'error.title'   => 'Error',
                'error.message' => $e->getMessage()
            ])->withInput();
		}
	}

	public function editView($id)
	{
		$cities = City::orderBy('name', 'ASC')->lists('name','id');
		$location = $this->repository->getLocationById($id);

		return view("LocationManage::edit")->with(compact('cities', 'location'));
	}

	public function edit($id, Request $request)
	{
		$this->locationValidate($request);
		try{
			$this->location->updateLocation($id, $request);

			return redirect('location')->with([
                'success.title'   => 'Done!',
                'success.message' => 'Location has been successfully updated!'
            ]);
		}catch(\Exception $e){
			return redirect('location/edit/'.$id)->with([
                'error.title'   => 'Error',
                'error.message' => $e->getMessage()
            ])->withInput();
		}
	}

	public function delete($id)
	{
		try{
			$this->location->deleteLocation($id);

			return redirect('location')->with([
                'success.title'   => 'Done!',
                'success.message' => 'Location has been successfully deleted!'
            ]);
		}catch(\Exception $e){
			return redirect('location')->with([
                'error.title'   => 'Error',
                'error.message' => $e->getMessage()
            ])->withInput();
		}
	}

	public function listView(Request $request)
	{
		try{
			$records = $this->location->filterRecords($request);
		}catch(\Exception $e){
			$records = [];
		}

		$cities = City::orderBy('name', 'ASC')->lists('name','id');

		return view('LocationManage::list')->with([
			'old' => $request, 
			'records' => $records, 
			'cities' => $cities
		]);
	}

}
