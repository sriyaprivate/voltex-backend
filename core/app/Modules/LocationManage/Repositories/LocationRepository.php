<?php
namespace App\Modules\LocationManage\Repositories;

use App\Modules\LocationManage\Models\Location;

class LocationRepository{

    public function save(array $data){
        $location = Location::create($data);

        if(!$location){
            throw new \Exception('LR1-Could not save the location');
        }

        return 1;
    }

    public function getLocationById($id){
        $location = Location::with(['city'])->where('id', $id)->first();

        if(!$location){
            throw new \Exception('LR2-Could not find location for Id: '.$id);
        }

        return $location;
    }

    public function update($id, $data){
        $location = $this->getLocationById($id);
        $location->city_id = $data->city;
        $location->name = $data->name;
        $location->status = $data->status;
        $location->save();
        
        return 1;
    }

    public function delete($id){
        $location = $this->getLocationById($id);
        $location->delete();
        
        return 1;
    }

    public function getRecords($conditions, $orderBy,$paginate = true, $records = 20){
        $locations = Location::with(['city']);

        if(!$orderBy){
            $orderBy = 'id';
        }

        if(count($conditions) > 0){
            foreach($conditions as $column => $condition){
                $locations->where($column, $condition['operator'], $condition['value']);
            }
        }

        if($paginate){
            $locations = $locations->orderBy($orderBy, 'asc')->paginate($records);
        }else{
            $locations = $locations->orderBy($orderBy, 'asc')->get();
        }

        if(count($locations) > 0){
            return $locations;
        }else{
            return [];
        }
    }
}