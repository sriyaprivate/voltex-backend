<?php

Route::group(['middleware' => ['auth']], function()
{
    Route::group(array('prefix'=>'location','namespace' => 'App\Modules\LocationManage\Controllers'), function()
    {
        //GET Routes
        Route::get('/', ['as' => 'location.list', 'uses' => 'LocationManageController@listView']);

        Route::get('add', ['as' => 'location.add', 'uses' => 'LocationManageController@addView']);

        Route::get('edit/{id}', ['as' => 'location.edit', 'uses' => 'LocationManageController@editView']);

        Route::get('delete/{id}', ['as' => 'location.delete', 'uses' => 'LocationManageController@delete']);

        //POST Routes
        Route::post('add', ['as' => 'location.add', 'uses' => 'LocationManageController@add']);

        Route::post('edit/{id}', ['as' => 'location.edit', 'uses' => 'LocationManageController@edit']);
        
    });
});