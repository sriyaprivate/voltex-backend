<?php namespace App\Modules\LocationManage\BusinessLogics;

use App\Modules\LocationManage\Repositories\LocationRepository;

/**
* Business Logics 
* Define all the busines logics in here
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/
use App\Modules\LocationManage\Models\Location;

class LocationLogic{

	protected $repository;

	public function __construct(LocationRepository $repository){
		$this->repository = $repository;
	}

	public function allLocation(){
		return $location = Location::all()->lists('name','id');
	}

	public function saveLocation($data){

		if(!$data->name && !$data->city){
			throw new \Exception("LL1-Invalid data array");
		}

		$data = [
			'name' => $data->name,
			'city_id' => $data->city,
			'status' => 1
		];

		return $this->repository->save($data);
	}

	public function updateLocation($id, $data){

		if(!$id){
			throw new \Exception("LL2-Invalid Id");
		}

		if(!$data->name && !$data->city){
			throw new \Exception("LL3-Invalid data array");
		}

		return $this->repository->update($id, $data);
	}

	public function deleteLocation($id){
		
		if(!$id){
			throw new \Exception("LL4-Invalid Id");
		}

		return $this->repository->delete($id);
	}

	public function filterRecords($data){
		$conditions = [];
		if($data->name != ''){
			$conditions['name'] = ['operator' => 'LIKE', 'value' => '%'.$data->name.'%'];
		}

		if($data->city != ''){
			$conditions['city_id'] = ['operator' => '=', 'value' => $data->city];
		}

		if($data->status != ''){
			$conditions['status'] = ['operator' => '=', 'value' => $data->status];
		}

		return $this->repository->getRecords($conditions, 'name');
	}

}
