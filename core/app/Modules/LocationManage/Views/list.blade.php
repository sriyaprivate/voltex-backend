@extends('layouts.back_master') @section('title','List Location')
@section('css')
<style type="text/css">
  
    
</style>
@stop
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    Location 
    <small> Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
        <li class="active">Location List</li>
    </ol>
</section>
<!-- SEARCH -->
<section class="content">
    <!-- Default box -->
    <div class="box">        
        <div class="box-body">
            <form action="{{ route('location.list') }}" method="get">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Search for..." value="{{ $old->name }}">
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">City</label>
                        <select class="form-control chosen" name="city">
                            <option value="">All</option>
                            @if(count($cities) > 0)
                                @foreach($cities as $key => $city)
                                <option value="{{$key}}" @if($old->city == $key) selected @endif>{{$city}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="control-label">Status</label>
                        <select class="form-control chosen" name="status">
                            <option value="">All</option>
                            <option value="1" @if($old->status == 1) selected @endif>Active</option>
                            <option value="0" @if($old->status != '' && $old->status == 0) selected @endif>Inactive</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-default" style="margin-top:19px;">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div> 

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Location</h3>
            <div class="box-title pull-right">
                <a href="{{ route('location.add') }}" class="btn btn-success btn-sm" title="Add Location">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                </a>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered bordered table-striped table-condensed">
                    <thead>
                        <tr>
                            <th style="text-align:center" width="3%">#</th>
                            <th style="text-align:center">Name</th>
                            <th style="text-align:center">City</th>
                            <th style="text-align:center" width="4%">Status</th>
                            <th style="text-align:center" colspan="2" width="4%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($records) > 0)
                        @foreach($records as $key => $item)
                            <tr>
                                <td style="text-align:center;">{{ (($records->currentPage()-1)*$records->perPage())+($key+1) }}</td>
                                <td>{{ ($item->name != '') ? $item->name : '-' }}</td>
                                <td>{{ ($item->city) ? $item->city->name : '-' }}</td>
                                <td style="text-align:center">
                                    @if($item->status == 1)
                                        <span class="fa fa-check-circle lbl-green"></span>
                                    @else
                                        <span class="fa fa-check-circle lbl-gray"></span>
                                    @endif
                                </td>
                                <td style="text-align:center;">
                                    <a href="{{ route('location.edit', $item->id) }}" title="Edit Location">
                                        <button class="btn btn-primary btn-xs">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                        </button>
                                    </a>
                                </td>
                                <td style="text-align:center;">
                                    <a href="{{ route('location.delete', $item->id) }}" title="Delete Location">
                                        <button class="btn btn-danger btn-xs">
                                            <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <td colspan="10">
                            <h2 align="center" class="lbl-gray">Data not found!</h2>
                        </td>
                    @endif
                    </tbody>
                </table>
                @if(count($records) > 0)
                <div class="pagination-wrapper pull-right"> {!! $records->appends($old->except('page'))->render() !!} </div>
                @endif
            </div>            
        </div>
    </div>  
</section>



@stop
@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $('.delete-btn').click(function(){    
        var link    = $(this).data('url');
        var title   = 'Confirm!.';
        var message = 'Are you sure you want to delete this location?.'

        confirm_delete(link, title, message);
    });
});
</script>
@stop


































