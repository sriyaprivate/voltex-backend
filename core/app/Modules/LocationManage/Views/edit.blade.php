@extends('layouts.back_master') @section('title','Add Location')
@section('css')
<link rel="stylesheet" href="{{asset('assets/dist/chosen/bootstrap-chosen.css')}}">
<link rel="stylesheet" href="{{asset('assets/dist/jquery-multiselect/css/multi-select.css')}}">
<style type="text/css">
.ms-container {
    background: transparent url("{{asset('assets/dist/jquery-multiselect/img/switch3.png')}}") no-repeat 50% 50%;
    width: 100%;
}

.has-error .chosen-container{
    border: 1px solid #ff0505;
    border-radius: 3px;
}
</style>
@stop
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
	Location
	<small> Management</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="{{{url('/')}}}"><i class="fa fa-home mr5"></i>Home</a></li>
		<li><a href="{{{url('location')}}}">Location List</a></li>
		<li class="active">Edit</li>
	</ol>
</section>
{{--<!-- Main content -->--}}
<section class="content">
	{{--<!-- Default box -->--}}
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title">Edit Location</h3>
			<!--<div class="box-tools pull-right">
				<a href="{{url('location/list')}}" class="btn btn-warning btn-sm" style="margin-top: 2px;">Location List</a>
			</div>-->
		</div>
        <form role="form" class="form-horizontal form-validation" method="post" autocomplete="off">
		    <div class="box-body">
                {!!Form::token()!!}
                <div class="form-group @if($errors->has('name')) has-error @endif">
                    <label class="col-sm-2 control-label">Name <span class="require">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control input-sm" name="name" placeholder="Name" value="{{$location->name}}">
                        @if($errors->has('name'))
                            <label id="label-error" class="error" for="label">{{$errors->first('name')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('city')) has-error @endif">
                    <label class="col-sm-2 control-label">City <span class="require">*</span></label>
                    <div class="col-sm-9">
                        <select name="city" class="form-control chosen">
                            <option value="">Select a City</option>
                            @if(count($cities) > 0)
                                @foreach($cities as $key => $city)
                                <option value="{{$key}}" @if($location->city_id == $key) selected @endif>{{$city}}</option>
                                @endforeach
                            @endif
                        </select>
                        @if($errors->has('city'))
                            <label id="label-error" class="error" for="label">{{$errors->first('city')}}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group @if($errors->has('status')) has-error @endif">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-9">
                        <select name="status" class="form-control chosen">
                            <option value="1" @if($location->status == 1) selected @endif>Active</option>
                            <option value="0" @if($location->status == 0) selected @endif>Inactive</option>
                        </select>
                        @if($errors->has('status'))
                            <label id="label-error" class="error" for="label">{{$errors->first('status')}}</label>
                        @endif
                    </div>
                </div>
            </div>
            <div class="box-footer with-border">
                <div class="form-group">
                    <div class="col-sm-2 col-sm-offset-9">
                        <button type="submit" class="btn bg-purple btn-sm pull-right"><i class="fa fa-floppy-o"></i> Save</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5">
                        <span><em><span class="require">*</span> Indicates required field</em></span>
                    </div>
                </div>
            </div>
        </form>
	</div>
</section>
@stop
@section('js')
<script type="text/javascript">
</script>
@stop
