<?php namespace App\Modules\LocationManage\Models;

/**
*
* Model
* @author Author <lahirumadhusankha0@gmail.com>
* @version 1.0
* @copyright Copyright (c) 2017, OITS.Dev+
*
*/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model {

    use SoftDeletes;
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'location';
     /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $fillable = ['city_id', 'name', 'status', 'created_at', 'updated_at', 'deleted_at'];

    public function city(){
        return $this->belongsTo('App\Models\City','city_id','id')->withTrashed();
    }

}
