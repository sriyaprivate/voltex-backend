<?php namespace App\Http\Logic;

use App\Classes\Functions;
use App\Events\NotificationEvent;

use App\Modules\OrderManage\Models\Order;
use App\Modules\OrderManage\Models\PerformaInvoice;

use DB;
use Sentinel;
use DateTime;
use DateTimeZone;

class Dashboard {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */

	public static function salesRecap($dates){

		$year 	= date('Y');
		$month 	= [];

		$sales 	= [];
		$pis 	= [];

		$series = [];

		for ($i=1; $i <= date('m'); $i++) {

			$pre = "0";

			if($i<=9){
				$pre = $pre.$i;
			}else{
				$pre = $i;
			}

			$tmp  = $year."-".$pre;

			$tmp_utc  	= intval($dates[$i-1]);

			///////////////////////////////////Sales Details/////////////////////////////////

			$orders 	= Order::where('order_date','like','%'.$tmp.'%')
                            ->selectRaw('SUM(amount) as total')
                            ->first();
            $orders   = $orders->total;

			$dd_sales 	= [];

			$dd_sales[] = $tmp_utc;
			$dd_sales[] = floatval(($orders=="")?0:$orders);

			$sales[] 	= $dd_sales;

			/////////////////////////////////////////////////////////////////////////////////

			///////////////////////////////////PI Details/////////////////////////////////

			$pfis = PerformaInvoice::where('confirmed_date','like','%'.$tmp.'%')
                        ->with(['order'])
                        ->get();

            $_pfi_tot = 0;

            foreach ($pfis as $value) {
            	$_pfi_tot+=$value['order']->amount;
            }

            $pfis   = $_pfi_tot;

			$dd_pfi 	= [];

			$dd_pfi[] = $tmp_utc;
			$dd_pfi[] = floatval(($pfis=="")?0:$pfis);

			$pis[] 	= $dd_pfi;

			/////////////////////////////////////////////////////////////////////////////////

		}

		array_push($series, ['name'=>'Sales','data'=>$sales]);
		array_push($series, ['name'=>'Performa Invoice','data'=>$pis]);

		return $series;
	}

}
