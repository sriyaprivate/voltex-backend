<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Str;

class VerifyCsrfToken extends BaseVerifier {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next){
		try{
            if (parent::isReading($request) || parent::shouldPassThrough($request) || $this->tokensMatch($request)) {
                return $this->addCookieToResponse($request, $next($request));
            }

            throw new TokenMismatchException;
            //return parent::handle($request, $next);
        }catch(TokenMismatchException $e){
            return redirect()->route('user.login');
        }
	}

	 protected function tokensMatch($request)
    {
        $sessionToken = $request->session()->token();

        $token = $request->input('_token') ?: $request->header('X-CSRF-TOKEN');

        if (! $token && $header = $request->header('X-XSRF-TOKEN')) {
            $token = $this->encrypter->decrypt($header);
        }

        if($request->is('v1/api/*') || $request->is('gcmService/*') || $request->is('logUploader/*')){
            return true;
        }


        if (! is_string($sessionToken) || ! is_string($token)) {
            return false;
        }

        return Str::equals($sessionToken, $token);
    }

}
