<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Modules\OrderManage\Models\Order;
use App\Modules\OrderManage\Models\OrderDetail;

use App\Modules\Category\Models\Category;
use App\Modules\Category\Models\CategoryType;

use App\Modules\Product\Models\Product;

use App\Models\ProductCategory;

class ApiController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		
	}

	public function getCustomerOrder($id){
		$res = Order::where('id',$id)->orWhere('order_no',$id)->with(['user.customer','customer','details.product.warrenty','deliveryType','courier'])->first();

		$order=[];
		$order_with_details=[];

		if($res){
			// return $res;
			foreach ($res['details'] as $value) {
				
				$order['customer']=$res['user']['customer']->first_name." ".$res['user']['customer']->last_name;
				$order['order_no']=$res->order_no;
				$order['order_date']=$res->order_date;
				$order['delivery_date']=$res->delivery_date;

				$order['item_code']=$value['product']->code;
				$order['qty']=$value->qty;
				$order['price']=$value->price;
				$order['discount_price']=$value->discount_price;
				$order['delivery_type']=$res['deliveryType']->name;

				if($res['courier']){				
					$order['courier']=$res['deliveryType']->name;
				}else{
					$order['courier']='-';
				}

				$order['billing_address']=$res->billing_address;
				$order['delivery_address']=$res->delivery_address;
				$order['pickup_address']=$res->pickup_address;

				$order_with_details[]=$order;
				
			}

			$response["result"] = $order_with_details;
		}else{
			$response["result"] = NULL;
		}
        return response()->json($response);
	}

	public function getCategoryHierarchy($type_id)
	{
		
		$data = Category::with(['parentCategory'])->where('category_type_id',$type_id)->whereNull('parent')->get();

		$jsonList = array();

		foreach ($data as $key => $value) {

			$jsonList[] = ['id'=>$value->id, 'name'=> $value->name];

			$_tmp = $value->getDescendants();

			foreach ($_tmp as $child) {
				$jsonList[] = ['id'=>$child->id, 'name'=> '--'.$child->name];

				$_tmp_node = $child->getDescendants();

				foreach ($_tmp_node as $grand_child) {
					$jsonList[] = ['id'=>$grand_child->id, 'name'=> '----'.$grand_child->name];
				}

			}

		}

		$response["result"] = $jsonList;
		
        return response()->json($response);
	}

	public function getCategoryChildren($cat_id)
	{
		$data = Category::find($cat_id)->getDescendants();

		$jsonList = array();

		foreach ($data as $key => $value) {

			$jsonList[] = $value;

			$_tmp = $value->getDescendants();

			foreach ($_tmp as $child) {
				$jsonList[] = $child;

				$_tmp_node = $child->getDescendants();

				foreach ($_tmp_node as $grand_child) {
					$jsonList[] = $grand_child;
				}

			}

		}

		$response["result"] = $jsonList;
		
        return response()->json($response);
	}

	public function getAllProducts()
	{
		$product_list = Product::with('product_category.category.type','stock','country','productImage')
				->orderBy('created_at','desc')
				->whereNull('deleted_at')
		  		->get();


		$response["result"] = $product_list;
		
        return response()->json($response);
	}

	public function getProductsByCategoryId($cat_id)
	{
		$product_list = Product::whereIn('id',function($query)use($cat_id){
			$query->select('product_id')->from('product_category')->where('category_id',$cat_id);
		})->with('country','productImage')->get();

		$response["result"] = $product_list;
		
        return response()->json($response);
	}

}
