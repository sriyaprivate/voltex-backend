<?php namespace App\Http\Controllers;

use Sentinel;
use Illuminate\Http\Request;

use App\Modules\OrderManage\Models\Order;
use App\Modules\OrderManage\Models\OrderDetail;
use App\Modules\OrderManage\Models\PerformaInvoice;

use App\Http\Logic\Dashboard;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function admin(Request $request)
	{
		$from 	= $request->get('from');
		$to 	= $request->get('to');

		if(($from=="" && $to=="") && ($from=="" || $to!="") && ($from!="" || $to=="") || ($from==NULL || $to==NULL)){
			$from 	= date('Y-m')."-01 00:00:00";
			$to 	= date('Y-m-d 23:59:59');
		}elseif($from!="" && $to!=""){
			$from 	= $from.' 00:00:00';
			$to 	= $to.' 23:59:59';
		}

		$newly_created = Order::where('status',NEWLY_CREATED)->whereBetween('created_at',[$from,$to])->get()->count();
		$approve_pending_pi = Order::where('status',APPROVE_PENDING)->whereBetween('created_at',[$from,$to])->get()->count();
		$confirm_pi = Order::where('status',CUSTOMER_CONFIRMED)->whereBetween('created_at',[$from,$to])->get()->count();

		$leadtimes = [];

		$convertion_data = [];

		return view('dashboard')->with([
        	'old' =>  ['from'=>$from,'to'=>$to],
        	'leadtimes' 			=>  $leadtimes,
        	'newly_created' 		=>  $newly_created,
        	'approve_pending_pi' 	=>  $approve_pending_pi,
        	'confirm_pi' 			=>  $confirm_pi,
        	'convertiondata' 		=>  $convertion_data
		]);
	}


	public function test()
	{
		return 'Hooray';
	}

	public function getSalesCap(Request $request){
		$dates = $request->get('dates');
		// return [];
		return $sales_recap = Dashboard::salesRecap($dates);
	}


}
