<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * USER AUTHENTICATION MIDDLEWARE
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::get('/', [
        'as' => 'index', 'uses' => 'WelcomeController@admin'
    ]);

    Route::get('/getSalesCap', [
        'as' => 'index', 'uses' => 'WelcomeController@getSalesCap'
    ]);
});

Route::get('user/login', [
    'as' => 'user.login', 'uses' => 'AuthController@loginView'
]);

Route::post('user/login', [
    'as' => 'user.login', 'uses' => 'AuthController@login'
]);

Route::get('user/logout', [
    'as' => 'user.logout', 'uses' => 'AuthController@logout'
]);

Route::get('test/test', 'WelcomeController@test'); //This is test controller to test data...

//add columns to all table
Route::get('get_all_table_in_db', function(){
	$test = DB::select('
		SELECT * FROM information_schema.tables WHERE table_schema = "ecommerce" 
	');

	foreach ($test as $key => $value) {
		DB::select('
			ALTER TABLE '.$value->TABLE_NAME.' ADD COLUMN `status` INT DEFAULT 1 NOT NULL,
			ADD COLUMN `created_at` DATETIME NOT NULL,
			ADD COLUMN `updated_at` DATETIME NULL,
			ADD COLUMN `deleted_at` DATETIME NULL;
		');
	}
});

Route::group(['prefix' => 'api'], function ()
{
    Route::any('getCustomerOrder/{id}', 'ApiController@getCustomerOrder');

    Route::any('getCategoryHierarchy/{type_id}', 'ApiController@getCategoryHierarchy');

    Route::any('getCategoryChildren/{cat_id}', 'ApiController@getCategoryChildren');

    Route::any('getAllProducts', 'ApiController@getAllProducts');

    Route::any('getProductsByCategoryId/{cat_id}', 'ApiController@getProductsByCategoryId');
});