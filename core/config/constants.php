<?php
define('UPLOADS_DIR', 'uploads/');
define('PATH_TO_BANNER', 'uploads/images/banners');
define('PATH_TO_CATEGORY', 'uploads/images/categories');
define('UPLOAD_PATH','core/storage/');
define('SHOW_OPTION',serialize([
	'1' => '25',
	'2' => '50',
	'3' => '100'
]));

//product have 2 status, 1 is in bigstore, 2 is this item in savings center
//this consts for cart.
define('DEFAULT_STATUS', 1);
define('SAVINGS_CENTER_STATUS', 2);
define('PRODUCT_CHECK_STATUS', serialize([DEFAULT_STATUS, SAVINGS_CENTER_STATUS]));

//promotion type
define('PROMOTION_TYPE',serialize([
	'1' => 'BANK PROMOTION',
	'2' => 'DEAL',
	'3' => 'DISCOUNT',
	'4' => 'BANK INSTALLMENT'
]));

//promotions
define('BANK_PROMOTION', 1);
define('DEAL_PROMOTION', 2);
define('DISCOUNT', 3);
define('BANK_INSTALLMENT', 4);

//PO
define('NEWLY_CREATED',0);
define('APPROVE_PENDING',1);
define('CUSTOMER_CONFIRMED',2);
define('CUSTOMER_REJECTED',3);

//home apply product section
define('HOME_SECTION',serialize([
	'0' => 'NO SECTION',
	'1' => 'BUY LATEST',
	'2'	=> 'DEALS',
	'3' => 'HIGHLY MOVING'
]));

//custom dates
define('DATE_FROM',date('Y-m-d',strtotime('2000-01-01')));
define('DATE_TO',date('Y-m-d',strtotime('3000-01-01')));

//warrenty period type
define('WARRENTY_PERIOD',serialize([
	'm' => 'Month',
	'd' => 'Date',
	'y' => 'Year',
	'l' => 'Life Time'
]));

//return  period type
define('RETURN_PERIOD',serialize([
	'm' => 'Month',
	'd' => 'Date',
	'y' => 'Year'
]));

//default image
define('DEFAULT_IMAGE', 'uploads/images/default/default.png');
;
//category type
define('BIG_STORE',1);
define('BULK_MARKET',2);
define('SAVING_CENTER',3);

//tax type
define('TAX_TYPE',serialize([
	'NO_VAT' => '1',
	'VAT'    => '2',
	'NBT_VAT'=> '3'
]));


//prefix for supplier
define('SUPPLIER_PREFIX', 'SUP-');

//default pricebook & channel
define('CHANNEL_ONLINE','online');

// automatically generated discount
define('DEFAULT_DISCOUNT','SYSTEM-DISCOUNT');

//delivery methoids
define('PICKUP', 'pickup');
define('DIRECT', 'direct');
define('EXPRESS', 'express');










