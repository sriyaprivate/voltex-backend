@extends('layouts.back_master') @section('title','Ops! You\'re in a Wrong Place.')
@section('content')
<div class="container  text-center" style="padding-top:100px">
  <div class="row">
    <span style="font-size:25px;font-weight:900">404</span>
  </div>
  <div class="h5">PAGE NOT FOUND</div>
  <p>Sorry, but the page you were trying to view does not exist.</p>
</div>
@stop