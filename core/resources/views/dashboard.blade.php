@extends('layouts.back_master') @section('title','Dashboard')
@section('current_title','Dashboard')

@section('css')
<style type="text/css">
  .stats .info-box-icon{
        background: none;
    }

    .stats .info-box-text{
        margin-top: 8px;
    }

    .stats .info-box-number{
        font-weight: 800;
    }

    .sector-performance a{
        padding: 5px 10px !important;
    }

    .dis-excced{
        color: red;
    }

    .btn{
        background: none;
    }

    .box-green{
      border-color: #00a65a;
    }

    .users-list-img {
        border-radius: 50%;
        max-width: 30%;
        height: auto;
    }

    .info-box-content{
        margin-left: 0px !important;
        color: #73879C;
        padding: 12px 10px;
        text-align: center;
        color: #ffffff;
    }

    .blue{
        background-color: #33B2FF;
    }

    .yellow{
        background-color: #FFC733;
    }

    .green{
        background-color: #239D0D;
    }

    .pink{
        background-color: #96228D;
    }

    .navy{
        background-color: #1C2EE2;
    }

    .icon-box{
        font-size: 17px;        
        font-weight: 600;        
    }

    .icon-padding{
        padding-right: 5px;        
    }

    .info-box-number{
        font-weight: 600;
        font-size: 21px;
    }
</style>  
@stop

@section('content')
<section>
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Welcome to
            <small>Inter Company Order Poratl</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <!-- !!Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <form action="{{url('admin')}}" method="get">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="box box-green">
                        <div class="box-header">
                            <div class="row form-group">
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <label for="" class="control-label">From</label>
                                    <input id="from" name="from" type="text" class="form-control datepick" value="{{$old['from']}}">
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <label for="" class="control-label">To</label>
                                    <input type="text" class="form-control datepick" name="to" id="to" value="{{$old['to']}}">
                                </div>
                                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <label for="" class="control-label">Country</label>
                                    <select class="form-control">
                                        <option>ALL</option>
                                        <option>Sri Lanka</option>
                                        <option>China</option>
                                        <option>India</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                                    <button class="btn btn-default pull-right" type="submit" style="margin-top: 20px"><i class="fa fa-search"></i> Filter</button>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </form>

        @if($user->hasAnyAccess(['dashboard.basic-details', 'admin']))  
        <div class="row stats">
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box blue">
                    
                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-user"></i>New Orders</span>
                        <span class="info-box-number">{{$newly_created}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                  <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box yellow">
                    
                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-file-text"></i>Pending PI</span>
                        <span class="info-box-number">{{$approve_pending_pi}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                  <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box yellow">

                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-book"></i>Confirmed PI</span>
                        <span class="info-box-number">{{$confirm_pi}}</span>
                    </div>
                <!-- /.info-box-content -->
                </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box green">

                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-file-pdf-o"></i>Coming soon</span>
                        <span class="info-box-number"></span>
                    </div>
                <!-- /.info-box-content -->
                </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box pink">

                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-bus"></i>Coming soon</span>
                        <span class="info-box-number"></span>
                    </div>
                <!-- /.info-box-content -->
                </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="info-box navy">

                    <div class="info-box-content">
                        <span class="info-box-text icon-box"><i class="icon-padding fa fa-money"></i>Coming soon</span>
                        <span class="info-box-number"></span>
                    </div>
                <!-- /.info-box-content -->
                </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>  
        @endif

        @if($user->hasAnyAccess(['dashboard.leadtimes', 'admin']))  
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="box box-green">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lead Time Analysis</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Milestone</th>
                                        <th class="text-center">Lead Time(Average Days)</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    <tr>
                                        <td><strong>PO -> PPO</strong></td>
                                        <td class="text-center"><strong>10%</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong>PPI -> CPI</strong></td>
                                        <td class="text-center"><strong>20%</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                      <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        
                    </div>
                <!-- /.box-footer -->
                </div>
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="box box-green">
                    <div class="box-header with-border">
                        <h3 class="box-title">Conversion Analysis</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th class="text-center">Gap Time(Average Days)</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    <tr>
                                        <td><strong>PO -> PI</strong></td>
                                        <td class="text-center"><strong>10%</strong></td>
                                    </tr>
                                    <tr>
                                        <td><strong>PPI -> CPI</strong></td>
                                        <td class="text-center"><strong>20%</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                      <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer clearfix">
                        
                    </div>
                <!-- /.box-footer -->
                </div>
            </div>
        </div>
        @endif

        @if($user->hasAnyAccess(['dashboard.sales.recap', 'admin']))  
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box">
                <!-- /.box-header -->
                    <div class="box-body">
                        <div id="monthly-sales-recap" style="min-width: 280px; height: 280px; margin: 0 auto"></div>
                    </div>
                    <!-- ./box-body -->
                    <div class="box-footer">
                        
                        <!-- /.row -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        @endif

    </section>

</section>
<!-- !!Main content -->

@stop

@section('js')
<script src="{{asset('assets/dist/highcharts/highcharts.js')}}"></script>
<script src="{{asset('assets/dist/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.datepick').datepicker({
              keyboardNavigation: false,
              forceParse: false,
              format: 'yyyy-mm-dd'
          });

        $('[data-toggle="popover"]').popover(); 

        @if($user->hasAnyAccess(['dashboard.sales.recap', 'admin'])) 

            dd=[];
            year = (new Date()).getFullYear();

            for (var i = 0 ; i < 12; i++) {
                dd.push(Date.UTC(year,i));
            }

            $.ajax({
                url: "{{url('getSalesCap')}}",
                type: 'GET',
                data: {'dates': dd},
                success: function(data) {

                    Highcharts.chart('monthly-sales-recap', {
                        title: {
                            text: '12 Month Sales Recap'
                        },

                        subtitle: {
                            text: year+' Jan To '+year+' Dec'
                        },

                        yAxis: {
                            title: {
                                text: 'Value'
                            }
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle'
                        },

                        xAxis: {
                          type: 'datetime',
                          min: Date.UTC((new Date()).getFullYear(), 0),
                          max: Date.UTC((new Date()).getFullYear(), 11),
                          labels: {
                              step: 1,
                              style: {
                                  fontSize: '13px',
                                  fontFamily: 'Arial,sans-serif'
                              }
                          },
                          dateTimeLabelFormats: { // don't display the dummy year
                              month: '%b \'%y',
                              year: '%Y'
                          }
                        },
           
                        series: data,

                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom'
                                    }
                                }
                            }]
                        }
                      });
                },error: function(data){

                }
            });

        @endif
    });
</script>
@stop
