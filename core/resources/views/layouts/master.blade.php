<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>OREL ICOP | @yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- ecommerce styles -->
    <link href="{{asset('assets/css/ecommerce.css')}}" rel="stylesheet">

    <link href="{{asset('assets/css/media.css')}}" rel="stylesheet">

    <!-- font awesome -->
    <link rel="stylesheet" href="{{asset('assets/libraries/font-awesome/css/font-awesome.min.css')}}">

    <!-- Jquery confirm alert -->
    <link rel="stylesheet" href="{{asset('assets/libraries/jquery-confirm/css/jquery-confirm.min.css')}}">

    <!-- animate -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/animate/animate.css') }}">

    <!-- toastr notification -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/libraries/toastr-notification/build/toastr.min.css') }}">
    <!-- <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}"/> -->
    <link href="https://fonts.googleapis.com/css?family=Permanent+Marker|Merriweather:300,400,700|Lato:300,400,700" rel="stylesheet" type="text/css">
    <style>
      .twitter-typeahead{
        display:block !important;
      }
    </style>
    @yield('links')

    @yield('css')
    
  </head>

  <body class="position-relative">    

    <!-- page-loading -->
    <div class="page-loading">
      <div class="loader"></div>
      <span class="text">Loading...</span>
    </div>
    <!-- / page-loading ends -->

    <div name="panel-refresh"></div>

    <!-- main navigation -->
    @include('includes.menu')
    <!-- / main navigation -->
    
    <!-- main content -->
    @yield('content')
    <!-- / main content -->

    <!-- main footer -->
    @include('includes.footer')
    <!-- / main footer -->

    <!-- jQuery 3.2.1 -->    
    <script src="{{asset('assets/js/jquery-3.2.1.min.js')}}"></script>

    <!-- Tether -->    
    <script src="{{asset('assets/js/tether.min.js')}}"></script>  

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('assets/js/popper.js')}}"></script>
    
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{asset('assets/js/ie10-viewport-bug-workaround.js')}}"></script>

    <!-- Jquery confirm alert -->
    <script src="{{asset('assets/libraries/jquery-confirm/js/jquery-confirm.min.js')}}"></script>

    <!-- toastr notification -->
    <script src="{{ asset('assets/libraries/toastr-notification/build/toastr.min.js') }}"></script>

    <!-- main js -->
    <script src="{{asset('assets/js/main.js')}}"></script>

    <!-- typeahead -->
    <script src="{{ asset('assets/libraries/typeahead/dist/typeahead.bundle.min.js') }}"></script>
    @yield('js')
    
    <!-- confirm message -->
    <script>
    <!--Start of Tawk.to Script-->
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
      var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
      s1.async=true;
      s1.src='https://embed.tawk.to/5aa0dba14b401e45400d830e/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();

<!--End of Tawk.to Script-->
      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip(); 

          $('.m-enter').mouseleave(function(){
              $(this).find('.animate-me').removeClass('animated bounceIn');
          });

          $('.m-enter').mouseenter(function(){
              $(this).find('.animate-me').addClass('animated bounceIn');
          });
      });
      
      function confirm_alert(confirmLink, cancelLink, qtyData = null, btn = null, title, message)
      {
        var qtyInfo = null;

        if(qtyData){
          qtyInfo = JSON.parse(qtyData);
        }

        if(btn !== null){ 
          var btn = btn; 
        }else{ 
          var btn = 'btn-red'; 
        };

        $.confirm({
            theme: 'material',
            title: title,
            content: message,
            buttons: {
                confirm: {
                    btnClass: btn,
                    keys: ['enter'],
                    action: function(){
                        if(confirmLink.length > 0){
                          if(qtyInfo && qtyInfo.canAddToCart == false){
                            return confirm_alert(confirmLink, cancelLink, null, 'btn-warning', 'Warning!.', 'Sorry, You request "'+qtyInfo.totalQty+'" quantity can not be proceed, only "'+qtyInfo.availableQty+'" quantity available in stock, Would you like to get '+qtyInfo.availableQty+' quantity?');
                          }
                          
                          window.open(confirmLink, "_self");
                        }
                    }
                },
                cancel: function () {
                  if(cancelLink.length > 0){
                    window.open(cancelLink, "_self");
                  }
                }
            }
        });
      }
    </script>

    @yield('scripts')

    <script>
      //alerts
      @if(session('success'))
        $.alert({theme: 'material',title: '{{session('success.title')}}',type: 'green',content: '{!! session('success.message') !!}'});
      @elseif(session('error'))
        $.alert({theme: 'material',title: '{{session('error.title')}}',type: 'red',content: '{{ str_replace(PHP_EOL, '', session('error.message')) }}'});
      @elseif(session('warning'))
        $.alert({theme: 'material',title: '{{session('warning.title')}}',type: 'orange',content: '{{session('warning.message')}}'});
      @elseif(session('info'))
        $.alert({theme: 'material',title: '{{session('info.title')}}',type: 'blue',content: '{{session('info.message')}}'});
      @endif

      // notification message
      @if(Session::has('notification'))
        notification(
          "{{ Session::get('option')['position']?:'toast-bottom-right' }}", 
          "{{ Session::get('option')['type']?:'warning' }}", 
          "{{ Session::get('notification')?:'-' }}", 
          "{{ Session::get('option')['timeOut']?:'5000' }}"
        );
        console.log('{{ Session::get('option')['position'] }}', '{{ Session::get('option')['type'] }}', '{{ Session::get('option')['timeOut'] }}');
      @endif

      @if(Session::has('stock_not_available'))
        $.confirm({
            theme: 'material',
            title: "{{ Session::get('title') }}",
            content: "{{ Session::get('message') }}",
            buttons: {
                confirm: {
                    btnClass: 'btn-warning',
                    keys: ['enter'],
                    action: function(){
                      $("{{ Session::get('option')['selector'] }}").find('input[name=qty]').val("{{ Session::get('option')['availableQty'] }}");
                      $("{{ Session::get('option')['selector'] }}").find('button[value=cart]').click();
                      addPanelRefresh();
                    }
                },
                cancel: function () {
                  notification("toast-bottom-right", "success", "Canceled!.", "5000");
                }
            }
        });
      @endif

      //live search
      // $(".search-input").typeahead({
      //   hint: true,
      //   highlight: true,
      //   minLength: 1
      // });

      function slugify(text)
      {
        return text.toString().toLowerCase()
          .replace(/\s+/g, '-')           // Replace spaces with -
          .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
          .replace(/\-\-+/g, '-')         // Replace multiple - with single -
          .replace(/^-+/, '')             // Trim - from start of text
          .replace(/-+$/, '');            // Trim - from end of text
      }

      $(document).ready(function($) {

        // Set the Options for "Bloodhound" suggestion engine
        var engine = new Bloodhound({
            remote: {
                url: '{{ route("home.ajax.search") }}'+'?q=%QUERY%',
                wildcard: '%QUERY%'
            },
            name: 'q',
            datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
            queryTokenizer: Bloodhound.tokenizers.whitespace
        });
    
        $(".search-input").typeahead({
            hint: false,
            highlight: true,
            minLength: 1
        }, {
            displayKey: 'display_name',
            display: 'display_name',
            limit:1000,
            source: engine.ttAdapter(),
    
            // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
            name: 'usersList',
    
            // the key from the array we want to display (name,id,email,etc...)
            templates: {
                empty: [
                    '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
                ],
                header: [
                    '<div class="list-group search-results-dropdown">'
                ],
                suggestion: function (data) {
                  var html             = '';
                  var price_html       = '';
                  var price            = 0;
                  var discounted_price = 0;
                  var image            = "{{ DEFAULT_PRODUCT_IMG }}";
                  var basePath         = "{{ url("data/small/images/product/") }}";
                  var product_url      = '';
                  
                  if(data.images.length > 0){
                    image = data.images[0].image;
                  }

                  if(data.pro_product !== null){
                    if(data.price.mrp !== null && data.pro_product.promo !== null && data.pro_product.promo.discount){
                      price            = data.price.mrp;
                      discounted_price = (data.price.mrp * (100 - data.pro_product.promo.discount)) / 100;
                      price            = parseFloat(price).toFixed(2);
                      discounted_price = parseFloat(discounted_price).toFixed(2);
                      price_html       = `<div class = "search-price"><del style="color: #999999;">${price}</del>&nbsp;<span class="badge badge-danger">${parseFloat(data.pro_product.promo.discount).toFixed()}% OFF</span>&nbsp;<strong>${discounted_price}</strong></div>`;
                    }else{
                      if(data.price !== null && data.price.mrp.length > 0){
                        price = parseFloat(data.price.mrp).toFixed(2);
                        price_html = `<div class="search-price"><strong>${price}</strong></div>`;
                      }else{
                        price_html = "";
                      }
                    }
                  }else{
                    if(data.price !== null && data.price.mrp.length > 0){
                      price = parseFloat(data.price.mrp).toFixed(2);
                      price_html = `<div class="search-price"><strong>${price}</strong></div>`;
                    }else{
                      price_html = "";
                    }
                  }
                  
                  if(data){
                    if(data.display_name){
                      if(data.status == "{{ DEAL_PROMOTION }}"){
                        product_url = "{{ url('') }}/savingscenter/item/"+slugify(data.display_name)+"/"+data.id;
                      }else if(data.status == "{{ DEFAULT_STATUS }}"){
                        product_url = "{{ url('') }}/bigstore/item/"+slugify(data.display_name)+"/"+data.id;
                      }else{
                        product_url = '#';
                      }
                    }else if(data.name){
                      if(data.status == "{{ DEAL_PROMOTION }}"){
                        product_url = "{{ url('') }}/savingscenter/item/"+slugify(data.name)+"/"+data.id;
                      }else if(data.status == "{{ DEFAULT_STATUS }}"){
                        product_url = "{{ url('') }}/bigstore/item/"+slugify(data.name)+"/"+data.id;
                      }else{
                        product_url = '#';
                      }
                    }else{
                      if(data.status == "{{ DEAL_PROMOTION }}"){
                        product_url = "{{ url('') }}/savingscenter/item/unkwon-name/"+data.id;
                      }else if(data.status == "{{ DEFAULT_STATUS }}"){
                        product_url = "{{ url('') }}/bigstore/item/unkwon-name/"+data.id;
                      }else{
                        product_url = '#';
                      }
                    }
                    
                    html = `
                      <a href="${product_url}" target="_blank" class="list-group-item list-group-item-action remove-border-radius">
                        <img class="small-img" src="${basePath}/${image}"/>${data.display_name}
                        ${price_html}
                      </a>`;
                  }

                  return html;
              } 
            }
        });

        $('input[name=q]').focusin(function(){
          var elem       = $('.tt-dataset-usersList');
          var elemHeight = $('.tt-dataset-usersList');

          console.log(elemHeight.css('height') , $(window).height());

          // if(elemHeight.css('height') < $(window).height()){
          //   elem.css('height', 'auto'); 
          //   console.log('auto');
          // }else{
          //   elem.css('height', '60vh');
          //   console.log('60vh');
          // }
        });

        var lastValue = '';

        $('input[name=q]').on('keyup change', function(){
          if($(this).val().trim() == ''){
            $('#global-search-btn').find('span').removeClass().addClass('fa fa-search');
          }else{
            $('#global-search-btn').find('span').removeClass().addClass('fa fa-spinner fa-spin');
          }

          lastValue = $(this).val();

          setTimeout(() => {
            if(lastValue == $(this).val()){
              $('#global-search-btn').find('span').removeClass().addClass('fa fa-search');
            }else{
              $('#global-search-btn').find('span').removeClass().addClass('fa fa-spinner fa-spin');
            } 
          }, 500);
        });

        $('.typeahead').bind('typeahead:asyncreceive', function(ev, suggestion,) {
          $('#global-search-btn').find('span').removeClass().addClass('fa fa-search');
        });

        $('.typeahead').bind('typeahead:cursorchange', function(ev, suggestion,) {
          $('#global-search-btn').find('span').removeClass().addClass('fa fa-search');
        });

        $('#newsletter_email').on('keyup', function(e){
          var pettern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

          if($(this).val().trim() !== '' && pettern.test($(this).val())){
            $('#newsletter_btn').attr('disabled', false);
          }else{
            $('#newsletter_btn').attr('disabled', true);
          }
        });
    });
    </script>
  </body>
</html>
