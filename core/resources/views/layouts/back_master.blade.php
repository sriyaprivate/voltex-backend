<!doctype html>
<html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <title>OREL ICOP | Admin Panel</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="shortcut icon" href="/favicon.ico">
        <meta name="csrf-token" content="{{ csrf_token() }}">
      
        <!-- BOOTSTRAP -->  
        <link rel="stylesheet" href="{{asset('assets/dist/bootstrap/css/bootstrap.min.css')}}">  
        <!-- //BOOTSTRAP -->

        <!-- FONTS -->
        <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/font-awesome.css')}}">
        <!-- //FONTS -->

        <!-- Chosen Select -->
        <link rel="stylesheet" href="{{asset('assets/dist/chosen/css/chosen.min.css')}}">

        <!-- STYLE -->
        <link rel="stylesheet" href="{{asset('assets/adminlte/css/AdminLTE.css')}}">
        <link rel="stylesheet" href="{{asset('assets/adminlte/css/skins/_all-skins.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/core/css/style.css')}}">

        <!-- Bootstrap-datetimepicker -->
        <link rel="stylesheet" href="{{asset('assets/dist/bootstrap-datetimepicker/dist/css/bootstrap-datetimepicker.css')}}">
        <!-- Bootstrap-datetimepicker -->

        <!-- File Upload -->
        <link rel="stylesheet" href="{{asset('assets/dist/fileinput/css/fileinput.min.css')}}">

        <!-- Jquery Confirm -->
        <link rel="stylesheet" href="{{asset('assets/dist/jquery-confirm/css/jquery-confirm.min.css')}}">

        <!-- Angular-confirm-master -->
        <link rel="stylesheet" href="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.css')}}">
        <!-- Angular-confirm-master -->

        <!-- Thumbnail Slider Css -->
        <link rel="stylesheet" href="{{asset('assets/dist/jquery-slider/css/jcarousel.ajax.css')}}">
        <!-- Thumbnail Slider Css -->


        <!-- wizard -->
        <link rel="stylesheet" href="{{asset('assets/dist/wizard/css/easyWizard.css')}}">
        <!-- wizard -->

        <!-- summernote -->
        <link rel="stylesheet" href="{{asset('assets/dist/summer-note/css/summernote.css')}}">
        <link rel="stylesheet" href="{{asset('assets/dist/summer-note/css/summernote-bs4.css')}}">  
        <!-- summernote -->

        <!-- bootstrap multi select -->
        <link rel="stylesheet" href="{{asset('assets/dist/bootstrap-multiselect/css/bootstrap-multiselect.css')}}">
        <!-- bootstrap multi select -->

        <!-- datepicker -->
        <link rel="stylesheet" href="{{asset('assets/dist/date-picker/css/bootstrap-datepicker.min.css')}}">
        <!-- datepicker -->
        @yield('links')

        <!-- //STYLE -->
        <style type="text/css">
            textarea{
                resize: none;
            }
            .require{
                color: #dd4b39;
                font-weight: normal;
            }
            label.error{
                display: initial;
                color: #dd4b39;
            }
            .form-group.has-error .error {
                font-weight: normal;
            }
            .chosen-container{
                width: 100%!important;
            }
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
            .main-header .logo .logo-lg {
                margin-top: 1px;
            }
            /* side bar custom style */
            .sidebar .sidebar-background, .off-canvas-sidebar .sidebar-background {
                position: absolute;
                z-index: 1;
                height: 100%;
                width: 100%;
                display: block;
                top: 0;
                left: 0;
                background-size: cover;
                background-position: center center;
            }
            .sidebar .sidebar-background:after, .off-canvas-sidebar .sidebar-background:after {
                position: absolute;
                z-index: 3;
                width: 100%;
                height: 100%;
                content: "";
                display: block;
                background: /*#d50000*/#2a296e;
                opacity: .8;
            }
            .sidebar-menu hr {
                margin: 8px 15px;
                border-color: rgba(255, 255, 255, 0.3);
            }
            .sidebar-menu {
                position: relative;
                z-index: 9999;
            }
            .main-header .sidebar-toggle:before {
                font-size: 14px;
            }
            .main-header>.navbar {
                border-bottom: 1px solid #d2d6de;
            }

            /* custom nav styles */
            .user-panel {
                z-index: 2;
                padding: 23px 15px 0;
                cursor: pointer;
            }
            .user-panel p {
                color: #fff;
            }
            .user-panel>.info {
                padding: 8px 5px 0px 15px;
            }
            .user-panel>.info>p {
                font-weight: normal; 
                margin-bottom: 10px;
            }
            .user-panel>.image>img {
                max-width: 35px;
                margin-top: 5px;
            }

            .skin-black-light .sidebar a {
                color: #000;
            }
            .skin-black-light .sidebar-menu>li:hover>a, .skin-black-light .sidebar-menu>li.active>a {
                color: #fff; 
                background: red;
            }
            .skin-black-light .sidebar-menu>li.header {
                color: rgba(255, 255, 255, 0.46);
                background: transparent;
            }
            .skin-black-light .sidebar-menu>li>.treeview-menu {
                background: rgba(255, 255, 255, 0.14);
            }
            .skin-black-light .sidebar-menu .treeview-menu>li>a {
                color: #000;
            }
            .skin-black-light .sidebar-menu .treeview-menu>li.active>a, .skin-black-light .sidebar-menu .treeview-menu>li>a:hover {
                color: rgba(255, 255, 255, 0.64);
            }
            .skin-black-light .main-sidebar {
                border-right: 0; 
            }
            .skin-black-light .sidebar-menu>li>a {
                border-left: 0;
                font-weight: normal; 
            }
            .skin-black-light .main-header .navbar>.sidebar-toggle {
                color: crimson;
                border: 2px solid crimson;
                border-radius: 0%;
                width: 34px;
                height: 34px;
                padding: 10px;
                margin: 15px;
                position: absolute;
                opacity: 0.9;
            }
            .main-header .sidebar-toggle:before {
                font-size: 14px;
                top: 5px;
                position: absolute;
                left: 9px;
            }
            .skin-black-light .main-header>.logo{
                border-right: 0;
            }
            .skin-black-light .main-header {
                border-bottom: 0; 
            }
            .skin-black-light .main-header .navbar .navbar-custom-menu .navbar-nav>li>a, .skin-black-light .main-header .navbar .navbar-right>li>a {
                border-left: 0;
                color: black !important;
            }
            .skin-black-light .main-header .navbar .sidebar-toggle:hover {
                color: #2a296e;
                opacity: 0.7;
            }
            .skin-black-light .sidebar-menu>li.active>a {
                font-weight: normal;
                color: #fff !important; 
            }
            .sidebar-menu .treeview-menu>li>a {
                font-size: inherit; 
                color: black !important;
            }
            /* custom nav items */
            .nav li a {
                padding: 0;
                color: black !important;
            }
            .nav li a div {
                color: crimson !important;
                border: 2px solid crimson !important;
                border-radius: 0%;
                width: 34px;
                height: 34px;
                padding: 10px;
                margin: 8px;
                float: left;
                opacity: 0.9;
            }
            .nav li a div i {
                font-size: 14px;
                position: absolute;
                top: 18px;
                color: crimson !important;
            }
            .required:after{
                content:"*";
                color:red;
            }
            .table-header{
                background-color: rgba(44, 62, 80, 0.6);
                color: white;
            }
            .lbl-green{
                color: rgba(26, 188, 156,1.0);
            }
            .lbl-gray{
                color: rgba(127, 140, 141,1.0);
            }

            .form-control{
                height: 30px;
                padding: 5px 10px;
                font-size: 12px;
            }
            .box-body{
                padding: 10px 30px 10px;
            }
            .btn{
                font-size: 12px;
            }
            .has-error .chosen-container{
                border: 1px solid #dd4b39;
                border-radius: 3px;
            }
            label.error{
                font-weight: 400 !important;
            }

            .main-header-title{
                text-decoration: none;
                outline: none;
                color: crimson;
                background: #fff !important;
            }
        </style>
        <!-- //STYLE -->

        <!-- INCLUDE css -->
        @yield('css')
        <!-- INCLUDE css -->
      
    </head>

    <body class="fixed sidebar-mini-expand-feature skin-black-light">
        <div class="refresh_application"></div>
        <div class="wrapper">

            <header class="main-header">

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" style="float: left;width: 230px;margin-left: 0px">
                    <div class="navbar-custom-menu" style="float: left !important;margin-left: 30px">
                        <ul class="nav">
                            <li>
                                <a href="#">
                                    <h3 class="main-header-title"><strong>ORDER</strong>PORTAL</h3>
                                </a>                  
                            </li>
                        </ul>
                    </div>
                </nav>
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        
                    </a>

                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav">
                            <li>
                                <a href="{{url('user/logout')}}">
                                    <div>
                                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                                    </div>                    
                                </a>                  
                            </li>
                        </ul>
                    </div>
                    <div class="navbar-custom-menu">
                        <ul class="nav">
                            <li>
                                <a href="#">
                                    <p style="color: crimson !important;padding-top: 17px;padding-right: 10px">Last seen : {{date('Y-m-d', strtotime( isset($user)? $user->last_login : '-'))}}</p>
                                </a>                  
                            </li>
                        </ul>
                    </div>
                    <div class="navbar-custom-menu">
                        <ul class="nav">
                            <li>
                                <a href="#">
                                    <p style="color: crimson !important;padding-top: 17px;padding-right: 10px">{{ isset($user)? $user->username: '-'}}</p>
                                </a>                  
                            </li>
                        </ul>
                    </div>
                    <div class="navbar-custom-menu">
                        <ul class="nav">
                            <li>
                                <a href="#">
                                    <img style="width: 40px;padding-right: 10px;padding-top: 7px" src="{{url('assets/adminlte/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                                </a>                  
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left">
                            <img style="width: 100px" src="{{url('assets/images/logo.png')}}" alt="User Image">
                            <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
                        </div>
                    </div>

                    <!-- sidebar -->
                    @include('includes.menu')
                    <!-- /.sidebar -->
                    
                </section>
                <!-- /.sidebar -->          
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </div><!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="container-fluid">
                    <div class="pull-left hidden-xs">
                        <b>Version</b> 1.0.1
                    </div>
                    <p class="pull-right">Copyright &copy; 2017-2018. All rights reserved.</p>
                </div>
            </footer>

        </div><!-- ./wrapper -->

        <!-- modernizr -->
        <script src="{{asset('assets/dist/core/js/modernizr.js')}}"></script>
        <!-- jquery -->      
        <script src="{{asset('assets/dist/jquery/js/jquery-1.12.3.min.js')}}"></script>
        <!-- bootstrap -->      
        <script src="{{asset('assets/dist/bootstrap/js/bootstrap.min.js')}}"></script>
        <!-- AdminLTE App -->
        <script src="{{asset('assets/adminlte/js/app.min.js')}}"></script>
        <!-- SlimScroll 1.3.0 -->
        <script src="{{asset('assets/dist/slimScroll/jquery.slimscroll.min.js')}}"></script>
        <!-- sweet-alert -->
        <script src="{{asset('assets/dist/sweetalert/js/sweet-alert.min.js')}}"></script>

        <!-- MY-SCRIPTS -->
        <script src="{{asset('assets/core/js/custom_functions.js')}}"></script>

        <!-- File Upload -->
        <script src="{{asset('assets/dist/fileinput/js/fileinput.min.js')}}"></script>

        <!-- Chosen Select -->
        <script src="{{asset('assets/dist/chosen/js/chosen.jquery.min.js')}}"></script>

        <!-- Jquery Confirm -->
        <script src="{{asset('assets/dist/jquery-confirm/js/jquery-confirm.min.js')}}"></script>

        <!-- bootstrap-datetimepicker -->
        <script src="{{asset('assets/dist/moment/min/moment.min.js')}}"></script>  
        <script src="{{asset('assets/dist/bootstrap-datetimepicker/dist/js/bootstrap-datetimepicker.js')}}"></script>
        <!-- bootstrap-datetimepicker -->

        <!-- ANGULAR -->
        <script src="{{asset('assets/dist/angular/angular/angular.js')}}"></script>
        <!-- ANGULAR -->

        <!-- load angular-moment -->
        <script src="{{asset('assets/dist/angular/angular-moment/angular-moment.min.js')}}"></script>
        <!-- load angular-moment -->

        <!-- angular-confirm-master -->
        <script src="{{asset('assets/dist/angular/angular-confirm-master/angular-confirm.min.js')}}"></script>
        <!-- angular-confirm-master -->

        <!-- angular chosen -->
        <script src="{{asset('assets/dist/angular/angular-chosen/angular-chosen.min.js')}}"></script>
        <!-- angular chosen -->

        <!-- thumbnail slider  -->
        <script src="{{asset('assets/dist/jquery-slider/js/jquery.jcarousel.min.js')}}"></script>
        <script src="{{asset('assets/dist/jquery-slider/js/jcarousel.ajax.js')}}"></script>
        <!-- angular chosen -->

        <!-- bootstrap multiselect -->
        <script type="text/javascript" src="{{asset('assets/dist/bootstrap-multiselect/js/bootstrap-multiselect.js')}}"></script>
        <!-- bootstrap multiselect -->

        <!-- summernote -->
        <script type="text/javascript" src="{{asset('assets/dist/summer-note/js/summernote.js')}}"></script>
        <!-- summernote -->
      
        <!-- //CORE JS -->

        <script type="text/javascript">
            /* Input Filed accept decimal only */
            function acceptDecimal(el){
                var ex = /^[0-9]+\.?[0-9]*$/;
                if(ex.test(el.value)==false){
                    el.value = el.value.substring(0,el.value.length - 1);
                }
            }
            
            $(function(){
                $('.refresh_application').fadeOut(2000);
            })

            $(document).ready(function(){
                // bootstrap file input
                $(".input-group").addClass('input-group-sm');
                $(".file-input .btn-file",".lf-ng-md-file-input .md-primary").addClass('bg-purple').removeClass('btn-primary');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('.chosen').chosen();

                $('#summernote').summernote({
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['picture', ['picture']],
                        ['link',['link']],
                        ['video',['video']],
                        ['table',['table']],
                        ['hr',['hr']],
                        ['fullscreen',['fullscreen']],
                        ['codeview',['codeview']],
                        ['undo',['undo']],
                        ['redo',['redo']],
                        ['help',['help']],
                        ['fontname',['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New']]
                    ],
                    height: 200,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    // maxHeight: 250,             // set maximum height of editor
                    focus: false,
                    fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150'],
                    placeholder: 'Add you description here...',
                    popover: {
                        image: [
                            // This is a Custom Button in a new Toolbar Area
                            ['imagesize', ['imageSize100','imageSize75','imageSize50', 'imageSize25']],
                            ['float', ['floatLeft', 'floatRight', 'floatNone']],
                            ['remove', ['removeMedia']]
                        ]
                    }
                });

                @if(session('success'))
                    $.alert({theme: 'material',title: '{{session('success.title')}}',type: 'green',content: '{!! session('success.message') !!}'});
                @elseif(session('error'))
                    $.alert({theme: 'material',title: '{{session('error.title')}}',type: 'red',content: '{{ session('error.message') }}'});
                @elseif(session('warning'))
                    $.alert({theme: 'material',title: '{{session('warning.title')}}',type: 'orange',content: '{{session('warning.message')}}'});
                @elseif(session('info'))
                    $.alert({theme: 'material',title: '{{session('info.title')}}',type: 'blue',content: '{{session('info.message')}}'});
                @endif

                /*============== Submit button disable after click it. =============== */
                $('.submit').on('click',function(){
                    setTimeout(function(){
                        $('.content').addClass('panel-refreshing');
                        $('.submit').attr('disabled',true); 
                    }, 100);
                });
                /*====================================================================*/
            });
        </script>

        @yield('js')
        <script type="text/javascript">
            /*=========================
                Author : Lahiru dilshan
                =========================*/
            //images
           
            //images
            //back button
            function goBack() {
                window.history.back();
            }

            //comfirm
            function confirm_delete(link, title, message){
                $.confirm({
                    theme: 'material',
                    title: title,
                    content: message,
                    buttons: {
                        confirm: {
                            btnClass: 'btn-red',
                            keys: ['enter'],
                            action: function(){
                                window.open(link, "_self");
                            }
                        },
                        cancel: function () {

                        }
                    }
                });
            }

            //comfirm
            function confirm_delete(link, title, message){
                $.confirm({
                    theme: 'material',
                    title: title,
                    content: message,
                    buttons: {
                        confirm: {
                            btnClass: 'btn-red',
                            keys: ['enter'],
                            action: function(){
                                window.open(link, "_self");
                            }
                        },
                        cancel: function () {

                        }
                    }
                });
            }

            //comfirm
            function alert(title, message){
                $.confirm({
                    title: title,
                    content: message,
                    buttons: {
                        confirm: {
                            btnClass: 'btn-red',
                            keys: ['enter'],
                            action: function(){
                            }
                        },
                        cancel: function () {

                        }
                    }
                });
            }
            //alert
            function error_alert(title,message,type){
                var title = title;
                var message_type  = ''; 
                if(type == 1){
                    title = 'Success !'
                    message_type  = 'green';
                }
                if(type == 2){
                    title = 'Error !';
                    message_type  = 'red'; 
                }
                if(type == 3){
                    title = 'Warning !';
                    message_type  = 'orange'; 
                }
                $.confirm({
                    title: title,
                    content: message,
                    type: message_type,
                    typeAnimated: true,
                    buttons: {
                        close: function () {}
                    }
                });
            }
        </script>
    </body>
</html>
