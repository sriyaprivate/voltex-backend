<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>Orel | iCOP</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" href="/favicon.ico">

    <!-- FONTS -->
    <link rel="stylesheet" href="{{asset('assets/fonts/roboto/roboto.css')}}">

    <!-- FONTS -->
    <link rel="stylesheet" href="{{asset('assets/fonts/fontawesome/css/font-awesome.css')}}">
    <!-- //FONTS -->

    <link rel="stylesheet" href="{{asset('assets/core/css/login.css')}}">
    <!-- //FONTS -->
    <style type="text/css">
        .auth .login-half-bg {
            background: url("{{asset('assets/images/bg/bg10.jpg')}}");
            background-size: cover;
        }

        .auth .register-half-bg {
            background: url("{{asset('assets/images/bg/bg10.jpg')}}");
            background-size: cover;
        }

        .auth.lock-full-bg {
            background: url("{{asset('assets/images/bg/bg10.jpg')}}");
            background-size: cover;
        }
    </style>
</head>

<body>

    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
                <div class="row flex-grow">
                    <div class="col-lg-5 d-flex align-items-center justify-content-center" style="background: white">
                        <div class="auth-form-transparent text-left p-3">
                            <div class="brand-logo">
                                <img src="{{asset('assets/images/logo.png')}}" style=" width: 140px;margin-left: -10px;">
                            </div>
                            <h4>Welcome!</h4>
                            <h6 class="font-weight-light">Happy to see you again and happy 2019</h6>
                            <form class="pt-3" role="form" action="{{URL::to('user/login')}}" method="post">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                @if($errors->has('login'))
                                    <div class="alert alert-danger">
                                        Oh snap! {{$errors->first('login')}}
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="exampleInputEmail">Username</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend bg-transparent">
                                            <span class="input-group-text bg-transparent border-right-0">
                                                <i class="fa fa-user text-primary"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control form-control-lg border-left-0" id="exampleInputEmail" placeholder="Username" name="username" value="{{{Input::old('username')}}}" autocomplete="off" @if(empty(Input::old('username'))) autofocus @endif>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword">Password</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend bg-transparent">
                                            <span class="input-group-text bg-transparent border-right-0">
                                                <i class="fa fa-lock text-primary"></i>
                                            </span>
                                        </div>
                                        <input type="password" class="form-control form-control-lg border-left-0" id="exampleInputPassword" placeholder="Password" name="password" @if(!empty(Input::old('username'))) autofocus @endif>                        
                                    </div>
                                </div>
                                <div class="my-2 d-flex justify-content-between align-items-center">
                                    <div class="form-check">
                                        <label class="form-check-label text-muted">
                                        <input type="checkbox" class="form-check-input">Keep me signed in
                                        <i class="input-helper"></i></label>
                                    </div>
                                    <a href="{{url('forgotPassword')}}" class="auth-link text-black">Forgot password?</a>
                                </div>
                                <div class="my-3">
                                    <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" style="background-color: crimson;border-color: crimson">LOGIN</button>
                                </div>
                                <div class="text-center mt-4 font-weight-light">
                                    Need help ? <a href="#" class="text-primary">Contact us</a>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-lg-7 login-half-bg d-flex flex-row hidden-xs hidden-sm" >
                        <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright OITS © 2019  All rights reserved.</p>
                    </div>

                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>

    <!-- jquery -->      
    <script src="{{asset('assets/dist/jquery/js/jquery-1.12.3.min.js')}}"></script>
    <script type="text/javascript">
    
    </script>
    <!-- endbuild -->
</body>

</html>
