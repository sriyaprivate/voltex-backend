
function generateTable(selector,url, orderableFalseTargets, textCenter,textRight, ajaxData,orderby){

    orderableFalseTargets = typeof orderableFalseTargets !== 'undefined' ? orderableFalseTargets : [];
    textCenter = typeof textCenter !== 'undefined' ? textCenter : [];
    textRight = typeof textRight !== 'undefined' ? textRight : [];
    ajaxData = typeof ajaxData !== 'undefined' ? ajaxData : {};

    var table = $(selector).DataTable({
                    "columnDefs": [
                        { "orderable": false, "targets": orderableFalseTargets },
                        {"className":"text-center vertical-align-middle", "targets":textCenter},
                        {"className":"text-right vertical-align-middle", "targets":textRight}
                    ],
                    'ajax': {
                        'url' : url,
                        "data": ajaxData,
                    },
                    "pageLength": 25,
                    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "order":[[orderby[0],orderby[1]]],
                });
    return table;
}

function sweetAlert(title,message,type){
    var message_types = ['success','info','error','warning'];
    swal(title, message, message_types[type]);
}

function sweetAlertConfirm(title,message,type,func){
    var message_types = ['success','info','error','warning'];

    swal({
        title: title,
        text: message,
        type: message_types[type],
        showConfirmButton : true,
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true,
        showCancelButton: true
    }, func);
}

function ajaxRequest(url,data,method,successFunc){

    data = typeof data !== 'undefined' ? data : {};
    method = typeof method !== 'undefined' ? method : 'post';
    successFunc = typeof successFunc !== 'undefined' ? successFunc : function(data){};

    $.ajax({
        url: url,
        data : data,
        method : method,
        success : successFunc
    });
}

function updated_chosen()
{
    $('.chosen').trigger('chosen:updated');
}

function load_filter_validation(){
    $("select[name='filters[]'], input[name='filter_values[]']").on('change', function(){
        var fv = $(this).closest('.form-group').find("input[name='filter_values[]']").val();
        var f  = $(this).closest('.form-group').find("select[name='filters[]']").val();

        if(fv.length !== 0 && f !== '')
        {
            $('#add_row').attr('disabled', false);
        }
    });
}
